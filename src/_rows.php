<?php
namespace desarrollo_em3\reportes;
use desarrollo_em3\error\error;
use stdClass;

class _rows
{
    private error $error;

    public function __construct()
    {
        $this->error = new error();

    }

    private function ajusta_totales_no_visit(array $informacion): array
    {
        $totales = (new _no_visitadas())->totales_no_visit($informacion);
        if(error::$en_error){
            return $this->error->error('Error al integrar $totales',$totales);
        }

        $informacionn = $this->integra_totales_no_visit($totales);
        if(error::$en_error){
            return $this->error->error('Error al integrar $totales',$totales);
        }
        foreach($informacion AS $key => $value){
            $informacion[$key] = array_merge($value,$informacionn['sumatoria']);
        }
        return $informacion;
    }


    /**
     * Genera una estructura de campos base personalizada para EM3.
     *
     * Este método crea y devuelve un objeto con los campos base personalizados utilizados en EM3.
     * Cada campo contiene un nombre y, en algunos casos, un valor predeterminado.
     *
     * @return stdClass Objeto con los campos base personalizados, incluyendo:
     *                  - `campo_estatus`: Campo de estado del contrato.
     *                      - `name`: Nombre del campo (string).
     *                      - `value`: Valor predeterminado (string).
     *                  - `campo_fecha_valida`: Campo para la fecha de validación de la información.
     *                      - `name`: Nombre del campo (string).
     *                  - `campo_serie`: Campo para la serie.
     *                      - `name`: Nombre del campo (string).
     *                  - `campo_monto_pagado`: Campo para el monto pagado.
     *                      - `name`: Nombre del campo (string).
     *                  - `campo_monto_total`: Campo para el monto total.
     *                      - `name`: Nombre del campo (string).
     *                  - `campo_folio`: Campo para el folio.
     *                      - `name`: Nombre del campo (string).
     *
     * @example
     * // Ejemplo de uso:
     * $campos = $this->campos_base_em3();
     * echo $campos->campo_estatus->name; // Salida: 'status_contrato_id'
     * echo $campos->campo_estatus->value; // Salida: '1'
     *
     * @example Salida esperada:
     * ```php
     * stdClass Object
     * (
     *     [campo_estatus] => stdClass Object
     *         (
     *             [name] => status_contrato_id
     *             [value] => 1
     *         )
     *     [campo_fecha_valida] => stdClass Object
     *         (
     *             [name] => fecha_valida_informacion
     *         )
     *     [campo_serie] => stdClass Object
     *         (
     *             [name] => serie
     *         )
     *     [campo_monto_pagado] => stdClass Object
     *         (
     *             [name] => monto_pagado
     *         )
     *     [campo_monto_total] => stdClass Object
     *         (
     *             [name] => monto_total
     *         )
     *     [campo_folio] => stdClass Object
     *         (
     *             [name] => folio
     *         )
     * )
     * ```
     *
     * Proceso:
     * 1. Crea un objeto `stdClass` para almacenar los campos base personalizados.
     * 2. Agrega propiedades al objeto con nombres de campos y valores predeterminados según sea necesario.
     * 3. Retorna el objeto con todos los campos configurados.
     */
    private function campos_base_em3(): stdClass
    {
        // Inicializa el objeto para almacenar los campos personalizados
        $campos_custom = new stdClass();

        // Configura los campos personalizados
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'status_contrato_id';
        $campos_custom->campo_estatus->value = '1';

        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'fecha_valida_informacion';

        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'serie';

        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'monto_pagado';

        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'monto_total';

        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'folio';

        // Retorna el objeto configurado
        return $campos_custom;
    }



    final public function cobrado_sin_meta(array $row): float
    {
        $cobrado_sin_meta = round((float)$row['COBRADO META'],2);
        $cobrado_sin_meta += round((float)$row['COBRADO META DE MAS'],2);
        $cobrado_sin_meta += round((float)$row['COBRADO CONTRATOS NUEVOS'],2);
        $cobrado_sin_meta += round((float)$row['COBRADO CONTRATOS EXTRA'],2);
        return round((float)$row['COBRADO TOTAL']-round($cobrado_sin_meta,2),2);

    }

    final public function datos_no_visit(array $informacion): array
    {
        foreach($informacion AS $key => $value){

            $informacion = (new _no_visitadas())->ajusta_informacion_no_visit($informacion,$key,$value);
            if(error::$en_error){
                return $this->error->error('Error al integrar procentajes',$informacion);
            }
            $informacion[$key]['sin_visitar'] = true;
        }


        $informacion = $this->ajusta_totales_no_visit($informacion);
        if(error::$en_error){
            return $this->error->error('Error al integrar $totales',$informacion);
        }
        return $informacion;

    }

    /**
     * FIN
     * Formatea múltiples valores enteros en una fila específica de un conjunto de filas.
     *
     * Esta función recorre las claves especificadas para enteros, verifica si cada clave no está vacía,
     * inicializa y formatea cada valor correspondiente como entero. Luego, integra los valores formateados
     * en la fila específica del conjunto de filas.
     *
     * @param int $indice El índice de la fila en el conjunto de filas que se va a actualizar.
     * @param bool $init_key_inexistente Indica si se debe inicializar la clave si no existe.
     * @param array $keys_enteros Las claves de los valores que deben ser formateados como enteros.
     * @param array $row La fila de datos donde se verificarán e inicializarán las claves.
     * @param array $rows El conjunto de filas donde se actualizará la fila con los valores formateados.
     * @return array El conjunto de filas con la fila específica actualizada con los valores formateados.
     */
    private function format_enteros(
        int $indice, bool $init_key_inexistente, array $keys_enteros, array $row, array $rows): array
    {
        foreach ($keys_enteros as $key){
            $key = trim($key);
            if($key === ''){
                return $this->error->error('Error key esta vacio',$key);
            }
            $rows = $this->integra_row_entero($indice,$init_key_inexistente,$key,$row,$rows);
            if(error::$en_error){
                return $this->error->error('Error al ajustar enteros',$rows);
            }
        }

        return $rows;

    }

    private function format_monedas(int $indice, bool $init_key_inexistente, array $keys_monedas, array $row, array $rows): array
    {
        foreach ($keys_monedas as $key){
            $rows = $this->integra_row_moneda($indice,$init_key_inexistente,$key,$row,$rows);
            if(error::$en_error){
                return $this->error->error('Error al ajustar monedas',$rows);
            }
        }

        return $rows;

    }

    private function format_porcentajes(int $indice, bool $init_key_inexistente, array $keys_porcentajes, array $row, array $rows): array
    {
        foreach ($keys_porcentajes as $key){
            $rows = $this->integra_row_porcentaje($indice,$init_key_inexistente,$key,$row,$rows);
            if(error::$en_error){
                return $this->error->error('Error al ajustar porcentajes',$rows);
            }
        }

        return $rows;

    }


    final public function format_numbers(int $indice, bool $init_key_inexistente, array $keys_enteros,
                                         array $keys_monedas, array $keys_porcentajes, array $row, array $rows): array
    {

        $rows = $this->format_enteros($indice,$init_key_inexistente,$keys_enteros,$row,$rows);
        if(error::$en_error){
            return $this->error->error('Error al ajustar enteros',$rows);
        }

        $rows = $this->format_monedas($indice,$init_key_inexistente,$keys_monedas,$row,$rows);
        if(error::$en_error){
            return $this->error->error('Error al ajustar monedas',$rows);
        }

        $rows = $this->format_porcentajes($indice,$init_key_inexistente,$keys_porcentajes,$row,$rows);
        if(error::$en_error){
            return $this->error->error('Error al ajustar porcentajes',$rows);
        }


        return $rows;

    }
    final public function formatea_totales(stdClass  $data_totales, array $rows): array
    {
        $enteros = array('total_contratos_congelados','total_cartera_actual','total_nuevos_contratos');

        $monedas = array('total_meta','total_cobrado_meta','total_cobrado_meta_de_mas',
            'total_cobrado_contratos_nuevos','total_cobrado_contratos_extra','total_cobrado_total',
            'total_cobrado_sin_meta');

        $porcentajes = array('total_porcentaje_meta_general','total_porcentaje_meta_real');

        foreach ($rows as $indice => $row){

            foreach ($enteros as $key){
                $value = (new _format_value())->entero($data_totales->$key);
                if(error::$en_error){
                    return $this->error->error('Error al ajustar entero',$value);
                }
                $rows[$indice][$key] = $value;
            }
            foreach ($monedas as $key){
                $value = (new _format_value())->moneda($data_totales->$key);
                if(error::$en_error){
                    return $this->error->error('Error al ajustar moneda',$value);
                }
                $rows[$indice][$key] = $value;
            }
            foreach ($porcentajes as $key){
                $value = (new _format_value())->porcentaje($data_totales->$key);
                if(error::$en_error){
                    return $this->error->error('Error al ajustar porcentaje',$value);
                }
                $rows[$indice][$key] = $value;
            }

        }

        return $rows;

    }

    /**
     * FIN
     * Inicializa un valor en una fila de datos si es necesario.
     *
     * Esta función verifica si la clave proporcionada no está vacía. Luego, llama a `init_row_key`
     * para inicializar la clave en la fila si es necesario. Si la inicialización falla o la clave
     * aún no existe en la fila, retorna un error.
     *
     * @param bool $init_key_inexistente Indica si se debe inicializar la clave si no existe.
     * @param string $key La clave a verificar e inicializar en la fila.
     * @param array $row La fila de datos donde se verificará e inicializará la clave.
     * @return array La fila de datos con la clave inicializada si era necesario, o un error si ocurre alguna falla.
     */
    private function inicializa_row_value(bool $init_key_inexistente, string $key,array $row): array
    {
        $key = trim($key);
        if($key === ''){
            return $this->error->error('Error key esta vacio',$key);
        }

        $row = $this->init_row_key($init_key_inexistente,$key,$row);
        if(error::$en_error){
            return $this->error->error('Error al inicializa row key: '.$key,$row);
        }
        if(!isset($row[$key])){
            return $this->error->error('Error no existe key en row key: '.$key,$row);
        }

        return $row;

    }

    /**
     * FIN
     * Inicializa una clave en una fila de datos si no existe.
     *
     * Esta función verifica si la clave proporcionada no está vacía. Si el parámetro
     * `$init_key_inexistente` es verdadero y la clave no existe en la fila, se inicializa con el valor 0.
     *
     * @param bool $init_key_inexistente Indica si se debe inicializar la clave si no existe.
     * @param string $key La clave a verificar e inicializar en la fila.
     * @param array $row La fila de datos donde se verificará e inicializará la clave.
     * @return array La fila de datos con la clave inicializada si era necesario.
     */
    private function init_row_key(bool $init_key_inexistente, string $key, array $row): array
    {
        $key = trim($key);
        if($key === ''){
            return $this->error->error('Error key esta vacio',$key);
        }
        if($init_key_inexistente){
            if(!isset($row[$key])){
                $row[$key] = 0;
            }
        }
        return $row;

    }

    /**
     * FIN
     * Integra un valor formateado como entero en una fila específica de un conjunto de filas.
     *
     * Esta función verifica si la clave proporcionada no está vacía, inicializa la clave en la fila
     * si es necesario, y luego formatea el valor correspondiente como entero. El valor formateado se
     * integra en la fila específica del conjunto de filas.
     *
     * @param int $indice El índice de la fila en el conjunto de filas que se va a actualizar.
     * @param bool $init_key_inexistente Indica si se debe inicializar la clave si no existe.
     * @param string $key La clave del valor a formatear e integrar.
     * @param array $row La fila de datos donde se verificará e inicializará la clave.
     * @param array $rows El conjunto de filas donde se actualizará la fila con el valor formateado.
     * @return array El conjunto de filas con la fila específica actualizada con el valor formateado.
     */
    private function integra_row_entero(
        int $indice, bool $init_key_inexistente, string $key, array $row, array $rows): array
    {
        $key = trim($key);
        if($key === ''){
            return $this->error->error('Error key esta vacio',$key);
        }
        $row = $this->inicializa_row_value($init_key_inexistente,$key,$row);
        if(error::$en_error){
            return $this->error->error('Error al inicializa row key: '.$key,$row);
        }
        $value = (new _format_value())->entero($row[$key]);
        if(error::$en_error){
            return $this->error->error('Error al ajustar entero',$value);
        }
        $rows[$indice][$key] = $value;

        return $rows;

    }

    private function integra_row_moneda(int $indice, bool $init_key_inexistente, string $key, array $row, array $rows): array
    {
        $key = trim($key);
        if($key === ''){
            return $this->error->error('Error key esta vacio',$key);
        }
        $row = $this->inicializa_row_value($init_key_inexistente,$key,$row);
        if(error::$en_error){
            return $this->error->error('Error al inicializa row key: '.$key,$row);
        }

        $value = (new _format_value())->moneda($row[$key]);
        if(error::$en_error){
            return $this->error->error('Error al ajustar moneda',$value);
        }
        $rows[$indice][$key] = $value;
        return $rows;

    }

    private function integra_row_porcentaje(int $indice, bool $init_key_inexistente, string $key, array $row, array $rows): array
    {
        $key = trim($key);
        if($key === ''){
            return $this->error->error('Error key esta vacio',$key);
        }
        $row = $this->inicializa_row_value($init_key_inexistente,$key,$row);
        if(error::$en_error){
            return $this->error->error('Error al inicializa row key: '.$key,$row);
        }
        $value = (new _format_value())->porcentaje($row[$key]);
        if(error::$en_error){
            return $this->error->error('Error al ajustar porcentaje',$value);
        }
        $rows[$indice][$key] = $value;

        return $rows;

    }

    private function integra_totales_no_visit(stdClass $totales): array
    {
        $informacionn = array();
        $informacionn['sumatoria']['sum_contratos_activos'] = number_format($totales->sumas->contratos_activos);
        $informacionn['sumatoria']['sum_no_visitado'] = number_format($totales->sumas->no_visitado);
        $informacionn['sumatoria']['sum_con_restos'] = number_format($totales->sumas->con_restos);
        $informacionn['sumatoria']['sum_sin_restos'] = number_format($totales->sumas->sin_restos);
        $informacionn['sumatoria']['porcentaje_sum_no_visitado'] = number_format($totales->porcentajes->no_visitado,2).'%';
        $informacionn['sumatoria']['porcentaje_sum_con_restos'] = number_format($totales->porcentajes->con_restos,2).'%';
        $informacionn['sumatoria']['porcentaje_sum_sin_restos'] = number_format($totales->porcentajes->sin_restos,2).'%';

        return $informacionn;

    }

    final public function params_base_em3()
    {
        $campos_custom =$this->campos_base_em3();
        if(error::$en_error){
           return $this->error->error('Error al obtener $campos_custom', $campos_custom);
        }

        $periodicidades = $this->periodicidades_em3();
        if(error::$en_error){
            return $this->error->error->error('Error al obtener $periodicidades', $periodicidades);
        }

        $campo_condicion_pago = 'periodicidad_pago_id';

        $params = new stdClass();
        $params->campos_custom = $campos_custom;
        $params->periodicidades = $periodicidades;
        $params->campo_condicion_pago = $campo_condicion_pago;
        return $params;

    }


    /**
     * TRASLADADO
     * Obtiene los parámetros base necesarios para la integración con SAP.
     *
     * Esta función recopila información base necesaria para la configuración y operación de SAP:
     * - Campos personalizados.
     * - Periodicidades de pago configuradas.
     * - Campo específico para la condición de pago.
     *
     * @return stdClass|array Devuelve un objeto con los siguientes atributos si la operación es exitosa:
     *                        - `campos_custom` (array): Campos base personalizados obtenidos de `_no_visitadas`.
     *                        - `periodicidades` (array): Periodicidades de pago para SAP.
     *                        - `campo_condicion_pago` (string): Clave del campo de condición de pago.
     *                        En caso de error, devuelve un arreglo de error con detalles del problema.
     *
     * @throws error Lanza un error si:
     *         - Falla al obtener los campos personalizados desde `_no_visitadas::campo_base_sap`.
     *         - Falla al obtener las periodicidades desde `_no_visitadas::periodicidades_sap`.
     *
     * @example
     * // Ejemplo de uso:
     * $params_sap = $this->params_base_sap();
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener los parámetros base para SAP.";
     * } else {
     *     print_r($params_sap);
     * }
     *
     * // Salida esperada:
     * stdClass Object
     * (
     *     [campos_custom] => Array
     *         (
     *             [campo1] => valor1
     *             [campo2] => valor2
     *         )
     *     [periodicidades] => Array
     *         (
     *             [s] => Array
     *                 (
     *                     [key] => S
     *                     [val] => 7
     *                 )
     *             [q] => Array
     *                 (
     *                     [key] => Q
     *                     [val] => 16
     *                 )
     *             [m] => Array
     *                 (
     *                     [key] => M
     *                     [val] => 31
     *                 )
     *         )
     *     [campo_condicion_pago] => U_CondPago
     * )
     */
    final public function params_base_sap()
    {
        // Obtiene los campos personalizados
        $campos_custom = (new _no_visitadas())->campo_base_sap();
        if (error::$en_error) {
            return $this->error->error('Error al obtener $campos_custom', $campos_custom);
        }

        // Obtiene las periodicidades configuradas en SAP
        $periodicidades = (new _no_visitadas())->periodicidades_sap();
        if (error::$en_error) {
            return $this->error->error('Error al obtener $periodicidades', $periodicidades);
        }

        // Define el campo específico para la condición de pago
        $campo_condicion_pago = 'U_CondPago';

        // Construye el objeto de parámetros
        $params = new stdClass();
        $params->campos_custom = $campos_custom;
        $params->periodicidades = $periodicidades;
        $params->campo_condicion_pago = $campo_condicion_pago;

        // Devuelve los parámetros
        return $params;
    }


    /**
     * TRASLADADO
     * Genera un array con las periodicidades utilizadas en EM3.
     *
     * Esta función obtiene el número de días del mes anterior y de la quincena actual
     * utilizando métodos de la clase `_no_visitadas`. Define un array con las claves 's',
     * 'q', y 'm' representando las periodicidades semanal, quincenal y mensual, respectivamente,
     * y les asigna valores correspondientes. Si ocurre un error al obtener los días del mes
     * anterior o de la quincena, se devuelve un array con detalles del error.
     *
     * @return array Retorna un array con las periodicidades 's', 'q', y 'm' y sus valores correspondientes.
     * En caso de error, retorna un array con detalles del error.
     */
    private function periodicidades_em3(): array
    {

        $n_dias_mes_anterior = (new _no_visitadas())->n_dias_mes_anterior();
        if(error::$en_error){
            return (new error())->error('Error al obtener los dias del mes anterior',$n_dias_mes_anterior);
        }
        $n_dias_quincena = (new _no_visitadas())->n_dias_quincena();
        if(error::$en_error){
            return (new error())->error('Error al obtener $n_dias_quincena', $n_dias_quincena);
        }

        $periodicidades['s']['key'] = '2';
        $periodicidades['s']['val'] = '7';

        $periodicidades['q']['key'] = '4';
        //$periodicidades['q']['val'] = $n_dias_quincena;
        $periodicidades['q']['val'] = 16;

        $periodicidades['m']['key'] = '5';
        //$periodicidades['m']['val'] = $n_dias_mes_anterior;
        $periodicidades['m']['val'] = 31;

        return $periodicidades;

    }




}
