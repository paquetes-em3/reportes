<?php
namespace desarrollo_em3\reportes;
use desarrollo_em3\error\error;
use stdClass;

class _no_visitadas{
    private function acumula_info_no_visit(array $informacion)
    {
        $sum_contratos_activos = 0;
        $sum_no_visitado = 0;
        $sum_con_restos = 0;
        $sum_sin_restos = 0;
        $keys = array('contratos_activos','no_visitado','con_restos','sin_restos');
        foreach($informacion AS $value){

            $value = (new _format_value())->values_numeric($keys, $value);
            if(error::$en_error){
                return (new error())->error('Error al integrar valor',$value);
            }

            $sum_contratos_activos += $value['contratos_activos'];
            $sum_no_visitado += $value['no_visitado'];
            $sum_con_restos += $value['con_restos'];
            $sum_sin_restos += $value['sin_restos'];
        }
        $sumas = new stdClass();
        $sumas->contratos_activos  =$sum_contratos_activos;
        $sumas->no_visitado  =$sum_no_visitado;
        $sumas->con_restos  =$sum_con_restos;
        $sumas->sin_restos  =$sum_sin_restos;

        $sumas->contratos_activos = round($sumas->contratos_activos,2);
        $sumas->no_visitado = round($sumas->no_visitado,2);
        $sumas->con_restos = round($sumas->con_restos,2);
        $sumas->sin_restos = round($sumas->sin_restos,2);


        return $sumas;
    }

    final public function ajusta_informacion_no_visit(array $informacion, string $key, array $value): array
    {
        $informacion[$key]['contratos_activos'] = number_format($informacion[$key]['contratos_activos']);
        $informacion[$key]['no_visitado'] = number_format($informacion[$key]['no_visitado']);
        $informacion[$key]['con_restos'] = number_format($informacion[$key]['con_restos']);
        $informacion[$key]['sin_restos'] = number_format($informacion[$key]['sin_restos']);

        $informacion = $this->porcentajes_asigna_no_visit($informacion,$key,$value);
        if(error::$en_error){
            return (new error())->error('Error al integrar procentajes',$informacion);
        }
        return $informacion;

    }

    /**
     * TRASLADADO
     * Crea y retorna un objeto stdClass con campos personalizados para SAP.
     *
     * Este método genera un objeto con varios campos personalizados que
     * se pueden usar para integraciones con SAP. Cada campo es un objeto
     * que contiene propiedades de nombre y, en algunos casos, un valor.
     *
     * @return stdClass Objeto con los campos personalizados.
     */
    final public function campo_base_sap(): stdClass
    {
        $campos_custom = new stdClass();
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'U_StatusCob';
        $campos_custom->campo_estatus->value = 'ACTIVO';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'DocDate';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'U_SeCont';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'PaidToDate';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'DocTotal';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'U_FolioCont';

        return $campos_custom;

    }

    /**
     * TRASLADADO
     * Calcula el número de días de la quincena actual.
     *
     * Esta función determina el número de días de la quincena basándose en la fecha actual.
     * Si la fecha es posterior al día 15, calcula los días restantes del mes. Si esta cantidad
     * es mayor o igual a 15, se ajusta el número de días de la quincena. En caso de error al
     * obtener el número de días del mes actual, se devuelve un array con detalles del error.
     *
     * @return int|array Retorna el número de días de la quincena. En caso de error, retorna un array con detalles del error.
     */
    final public function n_dias_quincena()
    {
        $n_dias_quincena = 15;

        $dia_hoy = (int)date('d');
        if($dia_hoy > $n_dias_quincena){

            $n_dias_mes_hoy = (new _no_visitadas())->n_dias_mes_actual();
            if(error::$en_error){
                return (new error())->error('Error al obtener el mes',$n_dias_mes_hoy);
            }
            $n_dias_calc = $n_dias_mes_hoy - 15;
            if($n_dias_calc >= 15){
                $n_dias_quincena = $n_dias_calc;
            }

        }
        return $n_dias_quincena;

    }

    /**
     * TRASLADADO
     * Calcula el número de días del mes actual.
     *
     * Esta función determina el número de días en el mes actual utilizando
     * la función `cal_days_in_month` con el calendario gregoriano.
     *
     * @return int Retorna el número de días del mes actual.
     */
    final public function n_dias_mes_actual(): int
    {
        $mes_actual = (int)date('m');
        return cal_days_in_month(CAL_GREGORIAN,$mes_actual, date('Y'));

    }


    /**
     * TRASLADADO
     * Obtiene el número de días del mes anterior.
     *
     * Esta función calcula el número de días que tuvo el mes anterior con base en la fecha actual.
     * Si el mes anterior es diciembre, ajusta el año correspondiente.
     *
     * @return int Devuelve el número de días del mes anterior.
     *
     * @example
     * // Ejemplo de uso:
     * $n_dias = $this->n_dias_mes_anterior();
     * echo "El mes anterior tuvo $n_dias días.";
     *
     * @notes
     * - Utiliza la función `cal_days_in_month` para obtener la cantidad de días de un mes específico.
     * - Maneja el cambio de año al retroceder desde enero a diciembre.
     */
    final public function n_dias_mes_anterior(): int
    {
        // Obtiene el mes anterior al actual
        $mes_anterior = (int)date('m') - 1;

        // Obtiene el año base, ajustando si el mes anterior es diciembre
        $anio_base = date('Y');
        if ($mes_anterior === 0) {
            $mes_anterior = 12;
            $anio_base = $anio_base - 1;
        }

        // Calcula y retorna el número de días del mes anterior
        return cal_days_in_month(CAL_GREGORIAN, $mes_anterior, $anio_base);
    }


    /**
     * TRASLADADO
     * Obtiene las periodicidades de pago para SAP.
     *
     * Esta función genera un arreglo con las periodicidades de pago configuradas en SAP,
     * junto con sus valores correspondientes. Estas incluyen periodicidades semanales, quincenales y mensuales.
     *
     * @return array Devuelve un arreglo asociativo con las periodicidades configuradas:
     *               - 's': Periódico semanal (cada 7 días).
     *               - 'q': Periódico quincenal (cada 16 días).
     *               - 'm': Periódico mensual (cada 31 días).
     *               En caso de error, devuelve un objeto de error.
     *
     * @throws error Lanza un error si ocurre algún problema al obtener los días de la quincena
     * o del mes anterior utilizando las funciones `n_dias_mes_anterior` y `n_dias_quincena`.
     *
     * @example
     * // Ejemplo de uso:
     * $periodicidades = $this->periodicidades_sap();
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener periodicidades.";
     * } else {
     *     print_r($periodicidades);
     * }
     *
     * // Salida esperada:
     * Array
     * (
     *     [s] => Array
     *         (
     *             [key] => S
     *             [val] => 7
     *         )
     *     [q] => Array
     *         (
     *             [key] => Q
     *             [val] => 16
     *         )
     *     [m] => Array
     *         (
     *             [key] => M
     *             [val] => 31
     *         )
     * )
     *
     * @notes
     * - Esta función utiliza las funciones `n_dias_mes_anterior` y `n_dias_quincena` para calcular
     *   dinámicamente los valores necesarios. Sin embargo, actualmente los valores son estáticos
     *   y no dependen de los cálculos.
     */
    final public function periodicidades_sap(): array
    {
        // Obtiene el número de días del mes anterior
        $n_dias_mes_anterior = $this->n_dias_mes_anterior();
        if (error::$en_error) {
            return (new error())->error('Error al obtener los dias del mes anterior', $n_dias_mes_anterior);
        }

        // Obtiene el número de días de la quincena
        $n_dias_quincena = $this->n_dias_quincena();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $n_dias_quincena', $n_dias_quincena);
        }

        // Define las periodicidades
        $periodicidades['s']['key'] = 'S'; // Semanal
        $periodicidades['s']['val'] = '7';

        $periodicidades['q']['key'] = 'Q'; // Quincenal
        $periodicidades['q']['val'] = 16;

        $periodicidades['m']['key'] = 'M'; // Mensual
        $periodicidades['m']['val'] = 31;

        // Retorna las periodicidades
        return $periodicidades;
    }


    private function porcentajes_asigna_no_visit(array $informacion, string $key, array $value): array
    {
        if((double)$value['contratos_activos'] === 0.0){
            $informacion[$key]['porcentaje_no_visitado'] = '0.0%';
        }
        else{
            $informacion[$key]['porcentaje_no_visitado'] = number_format($value['no_visitado'] * 100 / $value['contratos_activos'],2).'%';
        }

        if((double)$value['con_restos'] === 0.0){
            $informacion[$key]['porcentaje_con_restos'] = '0.0%';
        }
        else{
            $informacion[$key]['porcentaje_con_restos'] = number_format($value['con_restos'] * 100 / $value['no_visitado'],2).'%';
        }

        if((double)$value['sin_restos'] === 0.0){
            $informacion[$key]['porcentaje_sin_restos'] = '0.0%';
        }
        else{
            $informacion[$key]['porcentaje_sin_restos'] = number_format($value['sin_restos'] * 100 / $value['no_visitado'],2).'%';
        }

        return $informacion;

    }

    private function sum_porc_no_visit(stdClass $sumas): stdClass
    {
        $porcentaje_sum_no_visitado = 0;
        if($sumas->contratos_activos > 0.0) {
            $porcentaje_sum_no_visitado = round($sumas->no_visitado * 100, 2);
            $porcentaje_sum_no_visitado = round($porcentaje_sum_no_visitado / $sumas->contratos_activos, 2);
        }

        $porcentaje_sum_con_restos = 0;

        if($sumas->no_visitado > 0.0) {
            $porcentaje_sum_con_restos = round($sumas->con_restos * 100, 2);
            $porcentaje_sum_con_restos = round($porcentaje_sum_con_restos / $sumas->no_visitado, 2);
        }

        $porcentaje_sum_sin_restos = 0;

        if($sumas->no_visitado > 0.0) {
            $porcentaje_sum_sin_restos = round($sumas->sin_restos * 100, 2);
            $porcentaje_sum_sin_restos = round($porcentaje_sum_sin_restos / $sumas->no_visitado, 2);
        }

        $sumas = new stdClass();
        $sumas->no_visitado = $porcentaje_sum_no_visitado;
        $sumas->con_restos = $porcentaje_sum_con_restos;
        $sumas->sin_restos = $porcentaje_sum_sin_restos;

        return $sumas;
    }

    final public function totales_no_visit(array $informacion)
    {
        $sumas = $this->acumula_info_no_visit($informacion);
        if(error::$en_error){
            return (new error())->error('Error al integrar sumatorias',$sumas);
        }


        $porcentajes = $this->sum_porc_no_visit($sumas);
        if(error::$en_error){
            return (new error())->error('Error al integrar porcentajes',$porcentajes);
        }

        $totales = new stdClass();
        $totales->sumas = $sumas;
        $totales->porcentajes = $porcentajes;

        return $totales;

    }
}
