<?php
namespace desarrollo_em3\reportes;

use desarrollo_em3\error\error;
use desarrollo_em3\error\valida;
use stdClass;

class sql
{
    private error $error;

    public function __construct()
    {
        $this->error = new error();

    }

    /**
     * EM3
     * Asigna un valor a un campo basado en una clave específica en un objeto `stdClass`.
     *
     * Esta función verifica si el campo correspondiente a la clave `$key_campo_asignar` existe
     * y no está vacío en el objeto `$campos`. Si existe y tiene un valor, se utiliza ese valor.
     * En caso contrario, se asigna el valor predeterminado proporcionado en `$name_base`.
     *
     * @param stdClass $campos Objeto que contiene las claves y valores a validar.
     * @param string $key_campo_asignar Clave que se busca dentro del objeto `$campos`.
     * @param string $name_base Valor base que se asignará si la clave `$key_campo_asignar` no existe o está vacía.
     *
     * @return string|array Devuelve el valor asignado al campo. Si ocurre algún error en las validaciones,
     *                      devuelve un array con el detalle del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Clave existente y con valor en $campos
     * $campos = new stdClass();
     * $campos->campo_especifico = 'valor_asignado';
     * $resultado = $this->asigna_key_campo($campos, 'campo_especifico', 'valor_por_defecto');
     * // Resultado: 'valor_asignado'
     *
     * // Ejemplo 2: Clave existente pero vacía en $campos
     * $campos = new stdClass();
     * $campos->campo_especifico = '';
     * $resultado = $this->asigna_key_campo($campos, 'campo_especifico', 'valor_por_defecto');
     * // Resultado: 'valor_por_defecto'
     *
     * // Ejemplo 3: Clave no existente en $campos
     * $campos = new stdClass();
     * $resultado = $this->asigna_key_campo($campos, 'campo_inexistente', 'valor_por_defecto');
     * // Resultado: 'valor_por_defecto'
     *
     * // Ejemplo 4: $name_base vacío
     * $campos = new stdClass();
     * $resultado = $this->asigna_key_campo($campos, 'campo_especifico', '');
     * // Resultado: ['error' => 'Error $name_base esta vacio', 'data' => '']
     *
     * // Ejemplo 5: $key_campo_asignar vacío
     * $campos = new stdClass();
     * $resultado = $this->asigna_key_campo($campos, '', 'valor_por_defecto');
     * // Resultado: ['error' => 'Error $key_campo_asignar esta vacio', 'data' => '']
     * ```
     */
    private function asigna_key_campo(stdClass $campos, string $key_campo_asignar, string $name_base)
    {
        $name_base = trim($name_base);
        if ($name_base === '') {
            return $this->error->error('Error $name_base esta vacio', $name_base);
        }
        $key_campo_asignar = trim($key_campo_asignar);
        if ($key_campo_asignar === '') {
            return $this->error->error('Error $key_campo_asignar esta vacio', $key_campo_asignar);
        }

        $campo = $name_base;
        if (isset($campos->$key_campo_asignar)) {
            if (trim($campos->$key_campo_asignar) !== '') {
                $campo = trim($campos->$key_campo_asignar);
            }
        }
        return $campo;
    }


    /**
     * EM3
     * Genera una representación SQL básica para un campo en una entidad específica.
     *
     * Esta función toma el nombre de un campo y la entidad asociada y retorna
     * una representación SQL en el formato `entidad.campo`. Si el campo o la entidad están vacíos,
     * se devuelve un error detallado.
     *
     * @param string $campo El nombre del campo que se quiere representar.
     *                      Este parámetro debe ser no vacío.
     * @param string $entidad El nombre de la entidad asociada al campo.
     *                        Este parámetro también debe ser no vacío.
     *
     * @return string|array Devuelve una cadena con la representación SQL del campo.
     *                      Si alguno de los parámetros está vacío, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campo y entidad válidos
     * $campo_sql = $this->campo_base('id', 'usuario');
     * // Resultado: 'usuario.id'
     *
     * // Ejemplo 2: Campo vacío
     * $campo_sql = $this->campo_base('', 'usuario');
     * // Resultado: ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Entidad vacía
     * $campo_sql = $this->campo_base('id', '');
     * // Resultado: ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 4: Ambos parámetros vacíos
     * $campo_sql = $this->campo_base('', '');
     * // Resultado: ['error' => 'Error $campo esta vacio', 'data' => '']
     * ```
     */
    private function campo_base(string $campo, string $entidad)
    {
        // Valida que el campo no esté vacío
        if ($campo === '') {
            return $this->error->error('Error $campo esta vacio', $campo);
        }

        // Limpia y valida que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return $this->error->error('Error $entidad esta vacia', $entidad);
        }

        // Retorna la representación SQL del campo
        return "$entidad.$campo";
    }


    /**
     * EM3
     * Genera un campo SQL que concatena la serie y el folio de un contrato.
     *
     * Esta función toma los nombres de los campos de serie y folio de un contrato,
     * junto con el nombre de la entidad base, y devuelve una representación SQL
     * que concatena ambos campos utilizando `CONCAT`.
     *
     * @param string $campo_folio El nombre del campo que representa el folio del contrato.
     *                            Este parámetro debe ser no vacío.
     * @param string $campo_serie El nombre del campo que representa la serie del contrato.
     *                            Este parámetro debe ser no vacío.
     * @param string $entidad_base El nombre de la entidad base asociada a los campos.
     *                             Este parámetro debe ser no vacío.
     *
     * @return string|array Devuelve una cadena con la representación SQL que concatena la serie y el folio.
     *                      Si algún parámetro está vacío, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campos válidos
     * $campo_sql = $this->campo_contrato('folio', 'serie', 'contrato');
     * // Resultado: "CONCAT( contrato.serie, contrato.folio )"
     *
     * // Ejemplo 2: Campo de serie vacío
     * $campo_sql = $this->campo_contrato('folio', '', 'contrato');
     * // Resultado: ['error' => 'Error $campo_serie esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Campo de folio vacío
     * $campo_sql = $this->campo_contrato('', 'serie', 'contrato');
     * // Resultado: ['error' => 'Error $campo_folio esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Entidad base vacía
     * $campo_sql = $this->campo_contrato('folio', 'serie', '');
     * // Resultado: ['error' => 'Error $entidad_base esta vacia', 'data' => '']
     * ```
     */
    private function campo_contrato(string $campo_folio, string $campo_serie, string $entidad_base)
    {
        // Valida que el campo de serie no esté vacío
        if ($campo_serie === '') {
            return $this->error->error('Error $campo_serie esta vacio', $campo_serie);
        }

        // Valida que el campo de folio no esté vacío
        if ($campo_folio === '') {
            return $this->error->error('Error $campo_folio esta vacio', $campo_folio);
        }

        // Limpia y valida que la entidad base no esté vacía
        $entidad_base = trim($entidad_base);
        if ($entidad_base === '') {
            return $this->error->error('Error $entidad_base esta vacia', $entidad_base);
        }

        // Obtiene el campo SQL para la serie
        $campo_serie_sql = $this->campo_base($campo_serie, $entidad_base);
        if (error::$en_error) {
            return $this->error->error('Error al obtener campo serie', $campo_serie_sql);
        }

        // Obtiene el campo SQL para el folio
        $campo_folio_sql = $this->campo_base($campo_folio, $entidad_base);
        if (error::$en_error) {
            return $this->error->error('Error al obtener campo folio', $campo_folio_sql);
        }

        // Retorna la representación SQL concatenada
        return "CONCAT( $campo_serie_sql, $campo_folio_sql )";
    }


    /**
     * EM3
     * Genera la expresión SQL para calcular el saldo entre dos campos.
     *
     * Esta función valida que los nombres de los campos `$campo_aportado` y `$campo_precio` no estén vacíos
     * y construye una expresión SQL para calcular el saldo como la diferencia entre el precio y lo aportado.
     *
     * @param string $campo_aportado El nombre del campo que representa el monto aportado.
     * @param string $campo_precio El nombre del campo que representa el precio.
     *
     * @return string|array Devuelve una cadena SQL que representa la operación `$campo_precio - $campo_aportado`.
     *                      Si alguno de los parámetros está vacío, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campos válidos
     * $saldo = $this->campo_saldo('contrato.monto_aportado', 'contrato.precio_total');
     * // Resultado: 'contrato.precio_total - contrato.monto_aportado'
     *
     * // Ejemplo 2: Campo aportado vacío
     * $saldo = $this->campo_saldo('', 'contrato.precio_total');
     * // Resultado: ['error' => 'Error $campo_aportado esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Campo precio vacío
     * $saldo = $this->campo_saldo('contrato.monto_aportado', '');
     * // Resultado: ['error' => 'Error $campo_precio esta vacio', 'data' => '']
     * ```
     */
    private function campo_saldo(string $campo_aportado, string $campo_precio)
    {
        // Valida que el campo aportado no esté vacío
        $campo_aportado = trim($campo_aportado);
        if ($campo_aportado === '') {
            return $this->error->error('Error $campo_aportado esta vacio', $campo_aportado);
        }

        // Valida que el campo precio no esté vacío
        $campo_precio = trim($campo_precio);
        if ($campo_precio === '') {
            return $this->error->error('Error $campo_precio esta vacio', $campo_precio);
        }

        // Retorna la expresión SQL para calcular el saldo
        return "$campo_precio - $campo_aportado";
    }


    /**
     * EM3
     * Configura y devuelve un array con los nombres de los campos de datos relevantes para contratos.
     *
     * Esta función inicializa un array con valores predeterminados para ciertos campos relacionados con
     * la información de contratos, como la fecha válida, el estado del contrato, y un estado de contrato predeterminado.
     * Si se proporciona un array `$campos_data`, se agregan o sobrescriben las claves con los valores predeterminados.
     *
     * @param array $campos_data Array opcional que puede contener claves adicionales o personalizadas.
     *                           Las claves existentes serán sobrescritas con los valores predeterminados.
     *
     * @return array Devuelve el array `$campos_data` con las claves y valores actualizados.
     *
     * @example
     * ```php
     * // Ejemplo 1: Llamada sin parámetros
     * $resultado = $this->campos_data_contrato();
     * // Resultado:
     * // [
     * //     'campo_fecha_valida' => 'DocDate',
     * //     'campo_status_contrato' => 'U_StatusCob',
     * //     'campo_status_contrato_def' => 'Canceled'
     * // ]
     *
     * // Ejemplo 2: Llamada con un array personalizado
     * $campos_data = ['campo_fecha_valida' => 'fecha_inicio', 'campo_extra' => 'valor_extra'];
     * $resultado = $this->campos_data_contrato($campos_data);
     * // Resultado:
     * // [
     * //     'campo_fecha_valida' => 'DocDate', // Sobrescrito con el valor predeterminado
     * //     'campo_status_contrato' => 'U_StatusCob',
     * //     'campo_status_contrato_def' => 'Canceled',
     * //     'campo_extra' => 'valor_extra'    // Conservado porque no es sobrescrito
     * // ]
     * ```
     */
    private function campos_data_contrato(array $campos_data = array()): array
    {
        $campos_data['campo_fecha_valida'] = 'DocDate';
        $campos_data['campo_status_contrato'] = 'U_StatusCob';
        $campos_data['campo_status_contrato_def'] = 'Canceled';
        return $campos_data;
    }


    /**
     * EM3
     * Genera una subconsulta SQL para obtener el conteo de contratos en la cartera actual.
     *
     * Esta función valida los datos de entrada y construye una subconsulta SQL para contar los contratos
     * activos en una plaza específica, filtrados por su estado y estatus de cancelación.
     *
     * @param string $campo_cancelado Nombre del campo que indica si un contrato está cancelado.
     * @param string $campo_status_contrato Nombre del campo que indica el estado del contrato.
     * @param string $name_database Nombre de la base de datos que contiene las tablas relacionadas.
     * @param string $value_cancelado Valor esperado para el campo de cancelación (ejemplo: 'N').
     * @param string $values_status_contrato Valores permitidos para el estado del contrato, separados por comas.
     *
     * @return string|array Retorna una subconsulta SQL como cadena. Si ocurre algún error en las validaciones,
     *                      devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo de uso con datos válidos
     * $subquery = $this->cartera_actual(
     *     'Canceled',
     *     'U_StatusCob',
     *     'mi_base_de_datos',
     *     'N',
     *     "'ACTIVO', 'PROMESA PAGO'"
     * );
     * // Resultado:
     * // (SELECT COUNT(*) FROM mi_base_de_datos.contrato AS contrato
     * //  WHERE contrato.U_StatusCob IN ('ACTIVO', 'PROMESA PAGO')
     * //  AND contrato.Canceled = 'N'
     * //  AND contrato.plaza_id = meta_gestor.plaza_id) AS 'CARTERA ACTUAL',
     *
     * // Ejemplo con error en el campo cancelado vacío
     * $subquery = $this->cartera_actual(
     *     '',
     *     'U_StatusCob',
     *     'mi_base_de_datos',
     *     'N',
     *     "'ACTIVO', 'PROMESA PAGO'"
     * );
     * // Resultado:
     * // ['error' => 'Error $campo_cancelado esta vacio', 'data' => '']
     *
     * // Ejemplo con error en el nombre de la base de datos vacío
     * $subquery = $this->cartera_actual(
     *     'Canceled',
     *     'U_StatusCob',
     *     '',
     *     'N',
     *     "'ACTIVO', 'PROMESA PAGO'"
     * );
     * // Resultado:
     * // ['error' => 'Error $name_database esta vacio', 'data' => '']
     * ```
     */
    private function cartera_actual(
        string $campo_cancelado,
        string $campo_status_contrato,
        string $name_database,
        string $value_cancelado,
        string $values_status_contrato
    ) {
        $name_database = trim($name_database);
        if ($name_database === '') {
            return $this->error->error('Error $name_database esta vacio', $name_database);
        }

        $campo_status_contrato = trim($campo_status_contrato);
        if ($campo_status_contrato === '') {
            return $this->error->error('Error $campo_status_contrato esta vacio', $campo_status_contrato);
        }

        $values_status_contrato = trim($values_status_contrato);
        if ($values_status_contrato === '') {
            return $this->error->error('Error $values_status_contrato esta vacio', $values_status_contrato);
        }

        $value_cancelado = trim($value_cancelado);
        if ($value_cancelado === '') {
            return $this->error->error('Error $value_cancelado esta vacio', $value_cancelado);
        }

        $campo_cancelado = trim($campo_cancelado);
        if ($campo_cancelado === '') {
            return $this->error->error('Error $campo_cancelado esta vacio', $campo_cancelado);
        }

        $tb_contrato = "$name_database.contrato AS contrato";

        $w_status = "contrato.$campo_status_contrato IN ( $values_status_contrato )";
        $w_cancelado = "contrato.$campo_cancelado = '$value_cancelado'";
        $w_cartera_actual = "contrato.plaza_id = meta_gestor.plaza_id";

        $where = "$w_status AND $w_cancelado AND $w_cartera_actual";

        return "(SELECT COUNT(*) FROM $tb_contrato WHERE $where) AS 'CARTERA ACTUAL',";
    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para contar los contratos activos en una base de datos específica.
     *
     * Esta función valida los campos personalizados y el nombre de la base de datos.
     * Luego, inicializa los campos personalizados y genera una consulta SQL para contar los contratos activos
     * basándose en varios criterios.
     * Si se encuentran errores durante la validación o inicialización, se devuelve un array con información del error.
     *
     * @param string $name_database El nombre de la base de datos.
     * @param stdClass $campos_custom Un objeto que contiene los campos personalizados a validar e inicializar.
     * @return string|array Una cadena SQL que cuenta los contratos activos.
     *                      En caso de error, se devuelve un array con información del error.
     */
    private function contratos_activos(string $name_database, stdClass $campos_custom)
    {

        $valida = (new _valida())->valida_base_campos($campos_custom, $name_database);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }

        $campos = $this->init_campos($campos_custom);
        if(error::$en_error){
            return $this->error->error('Error al inicializar campos', $valida);
        }


        $sql = "(SELECT COUNT(*) FROM $name_database.contrato ";
        $sql .=" WHERE $name_database.contrato.$campos->estatus = '$campos->estatus_val' ";
        $sql .=" AND $name_database.contrato.plaza_id = TABLA.PLAZA_ID AND ";
        $sql .=" $name_database.contrato.$campos->fecha IS NOT NULL AND ";
        $sql .=" $name_database.contrato.$campos->serie IS NOT NULL AND ";
        $sql .=" $name_database.contrato.$campos->serie != '' AND ";
        $sql .=" $name_database.contrato.$campos->monto_p < $name_database.contrato.$campos->monto_t AND ";
        $sql .=" $name_database.contrato.$campos->folio IS NOT NULL) AS contratos_activos";

        return $sql;

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para calcular los días desde la última visita al cliente.
     *
     * Esta función valida que el nombre de la base de datos no esté vacío y luego genera una
     * consulta SQL que calcula la diferencia en días entre la fecha actual y la última visita al cliente.
     * Si se encuentra un error en la validación del nombre de la base de datos, se devuelve un array con información del error.
     *
     * @param string $name_database El nombre de la base de datos.
     * @return string|array Una cadena SQL que calcula los días desde la última visita al cliente.
     *                      En caso de error, se devuelve un array con información del error.
     */
    private function dias_ultima_visita(string $name_database)
    {
        $name_database = trim($name_database);
        if($name_database === ''){
            return $this->error->error('Error $name_database esta vacia', $name_database);
        }
        $fecha_ultima_visita_cliente = "$name_database.contrato.fecha_ultima_visita_cliente";
        $fecha_ultima_visita = "$name_database.contrato.fecha_ultima_visita";
        $renombre_campo = "DIAS TRANS ULT MOV CLIENTE";
        $ifnull = "IFNULL($fecha_ultima_visita_cliente,$fecha_ultima_visita)";
        return "@dias := DATEDIFF(CURDATE(),$ifnull) AS '$renombre_campo'";
    }


    /**
     * EM3
     * Genera una consulta SQL para obtener el número de nuevos contratos asignados a cada vendedor dentro de un rango de fechas.
     *
     * Esta función valida los datos de entrada y construye una consulta SQL que incluye uniones y condiciones basadas
     * en los campos y parámetros proporcionados. La consulta genera un reporte con el número total de contratos,
     * así como el número de nuevos contratos dentro del rango especificado.
     *
     * @param stdClass $campos Un objeto que contiene los nombres de los campos personalizados.
     *                         Por ejemplo: `campo_fecha_valida`, `campo_status_contrato`.
     * @param string $entidad_empleado El nombre de la entidad que representa al empleado o gestor.
     *                                 Por ejemplo: 'empleado'.
     * @param string $fecha_fin La fecha final del rango de análisis.
     * @param string $fecha_inicio La fecha inicial del rango de análisis.
     * @param string $key_empleado_gestor_id (Opcional) El nombre del campo clave para unir con la entidad del gestor.
     *                                       Por ejemplo: 'gestor_id'.
     * @param string $name_database El nombre de la base de datos a utilizar.
     * @param int $plaza_id El identificador único de la plaza a filtrar.
     * @param string $status_values Una lista separada por comas de valores de estado válidos para los contratos.
     *                               Por ejemplo: "'ACTIVO', 'PROMESA PAGO'".
     * @param string $value_status_def El valor por defecto para el estado del contrato.
     *                                  Por ejemplo: 'N' para contratos cancelados.
     *
     * @return string|array Retorna una cadena con la consulta SQL generada.
     *                      Si ocurre un error en cualquier paso, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar consulta para un rango de fechas
     * $campos = new stdClass();
     * $campos->campo_fecha_valida = 'DocDate';
     * $campos->campo_status_contrato = 'U_StatusCob';
     * $campos->campo_status_contrato_def = 'Canceled';
     *
     * $query = $this->clientes_nuevos_by_vendedor(
     *     $campos,
     *     'empleado',
     *     '2025-01-31',
     *     '2025-01-01',
     *     'gestor_id',
     *     'mi_base_datos',
     *     1,
     *     "'ACTIVO', 'PROMESA PAGO'",
     *     'N'
     * );
     * // Resultado esperado:
     * // SELECT empleado.id AS empleado_id, COUNT(*) AS 'n_contratos',
     * //        SUM(IF(contrato.DocDate BETWEEN '2025-01-01' AND '2025-01-31',1,0)) AS 'n_contratos_nuevos',
     * //        empleado.nombre_completo AS 'GESTOR'
     * // FROM mi_base_datos.contrato AS contrato
     * // LEFT JOIN mi_base_datos.cliente AS cliente ON contrato.cliente_id = cliente.id
     * // LEFT JOIN mi_base_datos.empleado AS empleado ON contrato.gestor_id = empleado.id
     * // WHERE contrato.U_StatusCob IN ('ACTIVO', 'PROMESA PAGO')
     * //       AND contrato.plaza_id = 1
     * //       AND contrato.Canceled = 'N'
     * // GROUP BY empleado.id;
     *
     * // Ejemplo 2: Error al pasar una plaza no válida
     * $query = $this->clientes_nuevos_by_vendedor(
     *     $campos,
     *     'empleado',
     *     '2025-01-31',
     *     '2025-01-01',
     *     '',
     *     'mi_base_datos',
     *     0,
     *     "'ACTIVO', 'PROMESA PAGO'",
     *     'N'
     * );
     * // Resultado esperado:
     * // ['error' => 'Error $plaza_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 3: Campos personalizados no definidos
     * $campos = new stdClass();
     * $query = $this->clientes_nuevos_by_vendedor(
     *     $campos,
     *     'empleado',
     *     '2025-01-31',
     *     '2025-01-01',
     *     'gestor_id',
     *     'mi_base_datos',
     *     1,
     *     '',
     *     ''
     * );
     * // Resultado esperado:
     * // ['error' => 'Error al asignar $obj_campos', 'data' => ...]
     * ```
     */
    final public function clientes_nuevos_by_vendedor(stdClass $campos, string $entidad_empleado, string $fecha_fin,
                                                      string $fecha_inicio, string $key_empleado_gestor_id,
                                                      string $name_database, int $plaza_id, string $status_values,
                                                      string $value_status_def)
    {
        $name_database = trim($name_database);
        $fecha_fin = trim($fecha_fin);
        $fecha_inicio = trim($fecha_inicio);

        $valida = (new _valida())->valida_input_base($fecha_fin,$fecha_inicio,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return $this->error->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }

        if($plaza_id <= 0){
            return $this->error->error('Error $plaza_id debe ser mayor a 0', $plaza_id);
        }
        
        $obj_campos = $this->obj_campos($campos);
        if(error::$en_error){
            return $this->error->error('Error al asignar $obj_campos', $obj_campos);
        }


        $values_status = $this->values_status($status_values);
        if(error::$en_error){
            return $this->error->error('Error al asignar $obj_campos', $values_status);
        }


        $status_def = $this->status_def($value_status_def);
        if(error::$en_error){
            return $this->error->error('Error al asignar $status_def', $status_def);
        }

        $key_empleado_id = $entidad_empleado.'_id';


        $left_join_empleado = $this->left_join_empleado_base($entidad_empleado,$key_empleado_gestor_id,
            $key_empleado_id,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al asignar $left_join_empleado', $left_join_empleado);
        }


        $empleado_id_sql = "$entidad_empleado.id AS $key_empleado_id";
        $n_contratos_sql = "COUNT(*) AS 'n_contratos'";
        $rango_fecha = "BETWEEN '$fecha_inicio' AND '$fecha_fin'";
        $if_rango_fecha = "contrato.$obj_campos->campo_fecha_valida $rango_fecha";
        $n_contratos_nuevos = "SUM(IF($if_rango_fecha,1,0)) AS 'n_contratos_nuevos'";
        $from_contrato = "$name_database.contrato AS contrato";
        $from_cliente = "$name_database.cliente AS cliente";
        $join_contrato_cliente = "$from_contrato LEFT JOIN $from_cliente ON contrato.cliente_id = cliente.id";
        $sql_gestor = "$entidad_empleado.nombre_completo AS 'GESTOR'";

        $campos_sql = "$empleado_id_sql, $n_contratos_sql, $n_contratos_nuevos, $sql_gestor";

        $w_status_cont = "contrato.$obj_campos->campo_status_contrato IN ($values_status)";
        $w_plaza = "contrato.plaza_id = $plaza_id";
        $w_status_def = "contrato.$obj_campos->campo_status_contrato_def = '$status_def'";

        $where = "WHERE $w_status_cont AND $w_plaza AND $w_status_def";
        $group_by = "GROUP BY $entidad_empleado.id";

        return /** @lang MYSQL */ "SELECT $campos_sql FROM $join_contrato_cliente $left_join_empleado $where $group_by";

    }

    /**
     * EM3
     * Genera una subconsulta SQL para calcular el total cobrado en un rango de fechas.
     *
     * @param string $campo_fecha_pago Nombre del campo que contiene la fecha de pago.
     * @param string $campo_monto_pago Nombre del campo que contiene el monto del pago.
     * @param string $campo_movimiento Nombre del campo que indica el tipo de movimiento.
     * @param string $fecha_fin Fecha final del rango (formato: 'YYYY-MM-DD').
     * @param string $fecha_inicio Fecha inicial del rango (formato: 'YYYY-MM-DD').
     * @param string $name_database Nombre de la base de datos que contiene la tabla de pagos.
     *
     * @return string|array Devuelve una subconsulta SQL como cadena o un array de error.
     */
    private function cobrado_total(
        string $campo_fecha_pago,
        string $campo_monto_pago,
        string $campo_movimiento,
        string $fecha_fin,
        string $fecha_inicio,
        string $name_database
    ) {
        $campo_monto_pago = trim($campo_monto_pago);
        if ($campo_monto_pago === '') {
            return $this->error->error('Error $campo_monto_pago esta vacio', $campo_monto_pago);
        }

        $name_database = trim($name_database);
        if ($name_database === '') {
            return $this->error->error('Error $name_database esta vacio', $name_database);
        }

        $campo_fecha_pago = trim($campo_fecha_pago);
        if ($campo_fecha_pago === '') {
            return $this->error->error('Error $campo_fecha_pago esta vacio', $campo_fecha_pago);
        }

        $fecha_inicio = trim($fecha_inicio);
        if ($fecha_inicio === '') {
            return $this->error->error('Error $fecha_inicio esta vacio', $fecha_inicio);
        }

        $campo_movimiento = trim($campo_movimiento);
        if ($campo_movimiento === '') {
            return $this->error->error('Error $campo_movimiento esta vacio', $campo_movimiento);
        }

        // Agregar prefijo "pago." si no está presente en los campos
        if (strpos($campo_fecha_pago, 'pago') === false) {
            $campo_fecha_pago = "pago.$campo_fecha_pago";
        }
        if (strpos($campo_movimiento, 'pago') === false) {
            $campo_movimiento = "pago.$campo_movimiento";
        }
        if (strpos($campo_monto_pago, 'pago') === false) {
            $campo_monto_pago = "pago.$campo_monto_pago";
        }

        $from = "FROM $name_database.pago AS pago";
        $fechas = "BETWEEN '$fecha_inicio' AND '$fecha_fin'";
        $w_fechas = "$campo_fecha_pago $fechas";
        $w_abono = "$campo_movimiento = 'Abono'";
        $w_pago_s = "pago.`status` = 'activo'";
        $w_plaza = "pago.plaza_id = plaza.id";
        $where = "WHERE $w_fechas AND $w_abono AND $w_pago_s AND $w_plaza";

        return "(SELECT SUM($campo_monto_pago) $from $where) AS 'COBRADO TOTAL',";
    }



    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener contratos con comisiones pendientes de pago.
     *
     * Esta función valida que el nombre de la base de datos no esté vacío y genera una consulta SQL
     * que selecciona los contratos que tienen montos pendientes de pago y comisiones inactivas.
     * Retorna la consulta SQL resultante para obtener dicha información.
     *
     * @param string $name_database El nombre de la base de datos donde se encuentran las tablas contrato y contrato_comision.
     * @return string|array Una cadena SQL que selecciona contratos con montos restantes pendientes de pago y comisiones inactivas.
     *                      En caso de error, se devuelve un array con información del error.
     */
    private function contratos_con_restos(string $name_database)
    {
        $name_database = trim($name_database);
        if($name_database === ''){
            return $this->error->error('Error $name_database esta vacia', $name_database);
        }
        $contrato_id_sql = "$name_database.contrato_comision.contrato_id";
        $contrato_comision_sql = "$name_database.contrato_comision";
        $contrato_sql = "$name_database.contrato";
        $join_cc_c = "$contrato_sql ON $name_database.contrato.id = $name_database.contrato_comision.contrato_id";
        $where_mr = "$name_database.contrato_comision.monto_resto > 0";
        $where_cp = "$name_database.contrato_comision.comision_pagada = 'inactivo'";
        $group_by = "$name_database.contrato_comision.contrato_id";
        $columns = "$contrato_id_sql, 1 AS x";
        $from = "$contrato_comision_sql INNER JOIN $join_cc_c";
        $where = "$where_mr AND $where_cp";
        return "SELECT $columns FROM $from WHERE $where GROUP BY $group_by";

    }

    /**
     * EM3
     * Genera una subconsulta SQL para contar los contratos nuevos.
     *
     * Esta función valida los datos de entrada y construye una subconsulta SQL para contar los contratos
     * en un rango de fechas específico, filtrados por su estado, estatus de cancelación, y asociación con una plaza.
     *
     * @param string $campo_cancelado Nombre del campo que indica si un contrato está cancelado.
     * @param string $campo_fecha_valida Nombre del campo que contiene la fecha a validar.
     * @param string $campo_status_contrato Nombre del campo que indica el estado del contrato.
     * @param string $fecha_fin Fecha final del rango (formato: 'YYYY-MM-DD').
     * @param string $fecha_inicio Fecha inicial del rango (formato: 'YYYY-MM-DD').
     * @param string $name_database Nombre de la base de datos que contiene las tablas relacionadas.
     * @param string $value_cancelado Valor esperado para el campo de cancelación (ejemplo: 'N').
     * @param string $value_status_contrato Valores permitidos para el estado del contrato, separados por comas.
     *
     * @return string|array Devuelve una subconsulta SQL como cadena. Si ocurre algún error en las validaciones,
     *                      devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo de uso con datos válidos
     * $subquery = $this->contratos_nuevos(
     *     'Canceled',
     *     'DocDate',
     *     'U_StatusCob',
     *     '2024-12-31',
     *     '2024-01-01',
     *     'mi_base_de_datos',
     *     'N',
     *     "'ACTIVO', 'PROMESA PAGO'"
     * );
     * // Resultado:
     * // (SELECT COUNT(*) FROM mi_base_de_datos.contrato AS contrato
     * //  WHERE contrato.DocDate BETWEEN '2024-01-01' AND '2024-12-31'
     * //  AND contrato.U_StatusCob IN ('ACTIVO', 'PROMESA PAGO')
     * //  AND contrato.Canceled = 'N'
     * //  AND meta_gestor.plaza_id = contrato.plaza_id) AS 'CONTRATOS NUEVOS',
     *
     * // Ejemplo con error en el campo de cancelación vacío
     * $subquery = $this->contratos_nuevos(
     *     '',
     *     'DocDate',
     *     'U_StatusCob',
     *     '2024-12-31',
     *     '2024-01-01',
     *     'mi_base_de_datos',
     *     'N',
     *     "'ACTIVO', 'PROMESA PAGO'"
     * );
     * // Resultado:
     * // ['error' => 'Error $campo_cancelado esta vacio', 'data' => '']
     *
     * // Ejemplo con error en la fecha de inicio vacía
     * $subquery = $this->contratos_nuevos(
     *     'Canceled',
     *     'DocDate',
     *     'U_StatusCob',
     *     '2024-12-31',
     *     '',
     *     'mi_base_de_datos',
     *     'N',
     *     "'ACTIVO', 'PROMESA PAGO'"
     * );
     * // Resultado:
     * // ['error' => 'Error $fecha_inicio esta vacio', 'data' => '']
     * ```
     */
    private function contratos_nuevos(
        string $campo_cancelado,
        string $campo_fecha_valida,
        string $campo_status_contrato,
        string $fecha_fin,
        string $fecha_inicio,
        string $name_database,
        string $value_cancelado,
        string $value_status_contrato
    ) {
        $campo_cancelado = trim($campo_cancelado);
        if ($campo_cancelado === '') {
            return $this->error->error('Error $campo_cancelado esta vacio', $campo_cancelado);
        }

        $campo_fecha_valida = trim($campo_fecha_valida);
        if ($campo_fecha_valida === '') {
            return $this->error->error('Error $campo_fecha_valida esta vacio', $campo_fecha_valida);
        }

        $campo_status_contrato = trim($campo_status_contrato);
        if ($campo_status_contrato === '') {
            return $this->error->error('Error $campo_status_contrato esta vacio', $campo_status_contrato);
        }

        $fecha_fin = trim($fecha_fin);
        if ($fecha_fin === '') {
            return $this->error->error('Error $fecha_fin esta vacio', $fecha_fin);
        }

        $fecha_inicio = trim($fecha_inicio);
        if ($fecha_inicio === '') {
            return $this->error->error('Error $fecha_inicio esta vacio', $fecha_inicio);
        }

        $name_database = trim($name_database);
        if ($name_database === '') {
            return $this->error->error('Error $name_database esta vacio', $name_database);
        }

        $value_cancelado = trim($value_cancelado);
        if ($value_cancelado === '') {
            return $this->error->error('Error $value_cancelado esta vacio', $value_cancelado);
        }

        $value_status_contrato = trim($value_status_contrato);
        if ($value_status_contrato === '') {
            return $this->error->error('Error $value_status_contrato esta vacio', $value_status_contrato);
        }

        $f_contrato = "$name_database.contrato AS contrato";
        $fechas = "BETWEEN '$fecha_inicio' AND '$fecha_fin'";
        $w_status_contrato = "contrato.$campo_status_contrato IN ( $value_status_contrato )";
        $w_contrato_cancel = "contrato.$campo_cancelado = '$value_cancelado'";
        $w_meta = "meta_gestor.plaza_id = contrato.plaza_id";
        $renombre = "AS 'CONTRATOS NUEVOS'";
        $where = "contrato.$campo_fecha_valida $fechas AND $w_status_contrato AND $w_contrato_cancel AND $w_meta";

        return "(SELECT COUNT(*) FROM $f_contrato WHERE $where ) $renombre,";
    }



    /**
     * EM3
     * Valida y retorna el nombre de la entidad para clientes.
     *
     * Esta función valida que el nombre de la entidad base no esté vacío. Si no se proporciona un valor para la entidad de cliente,
     * asigna automáticamente el valor de la entidad base.
     *
     * @param string $entidad_base El nombre de la entidad base. Este valor es obligatorio y no puede estar vacío.
     * @param string $entidad_cliente El nombre de la entidad cliente. Si está vacío, se usará el nombre de la entidad base.
     *
     * @return string|array Devuelve el nombre de la entidad cliente. Si la entidad base está vacía,
     *                      devuelve un array con el error correspondiente.
     *
     * @example
     * ```php
     * // Ejemplo 1: Entidad cliente proporcionada
     * $entidad_cliente = $this->entidad_cliente('contrato', 'cliente');
     * // Resultado: 'cliente'
     *
     * // Ejemplo 2: Entidad cliente vacía
     * $entidad_cliente = $this->entidad_cliente('contrato', '');
     * // Resultado: 'contrato'
     *
     * // Ejemplo 3: Entidad base vacía
     * $entidad_cliente = $this->entidad_cliente('', 'cliente');
     * // Resultado:
     * // ['error' => 'Error $entidad_base esta vacia', 'data' => '']
     * ```
     */
    private function entidad_cliente(string $entidad_base, string $entidad_cliente)
    {
        $entidad_base = trim($entidad_base);
        if ($entidad_base === '') {
            return $this->error->error('Error $entidad_base esta vacia', $entidad_base);
        }

        $entidad_cliente = trim($entidad_cliente);
        if ($entidad_cliente === '') {
            $entidad_cliente = $entidad_base;
        }

        return $entidad_cliente;
    }



    /**
     * EM3
     * Valida y retorna el nombre de una entidad de estado.
     *
     * Esta función toma el nombre de una entidad base y una entidad de estado, valida que no estén vacías,
     * y devuelve el nombre de la entidad de estado. Si la entidad de estado no se proporciona (está vacía),
     * se asigna automáticamente el valor de la entidad base.
     *
     * @param string $entidad_base El nombre de la entidad base. Este valor es obligatorio y no puede estar vacío.
     * @param string $entidad_status El nombre de la entidad de estado. Si está vacío, se usará el nombre de la entidad base.
     *
     * @return string|array Devuelve el nombre de la entidad de estado. Si alguno de los parámetros está vacío,
     *                      devuelve un array con el error correspondiente.
     *
     * @example
     * ```php
     * // Ejemplo 1: Entidad de estado proporcionada
     * $entidad_status = $this->entidad_status('contrato', 'contrato_status');
     * // Resultado: 'contrato_status'
     *
     * // Ejemplo 2: Entidad de estado vacía
     * $entidad_status = $this->entidad_status('contrato', '');
     * // Resultado: 'contrato'
     *
     * // Ejemplo 3: Entidad base vacía
     * $entidad_status = $this->entidad_status('', 'contrato_status');
     * // Resultado:
     * // ['error' => 'Error $entidad_base esta vacia', 'data' => '']
     * ```
     */
    private function entidad_status(string $entidad_base, string $entidad_status)
    {
        $entidad_base = trim($entidad_base);
        if ($entidad_base === '') {
            return $this->error->error('Error $entidad_base esta vacia', $entidad_base);
        }
        $entidad_status = trim($entidad_status);
        if ($entidad_status === '') {
            $entidad_status = $entidad_base;
        }

        return $entidad_status;
    }


    /**
     * EM3
     * Valida y retorna el nombre de la entidad para el tipo de morosidad.
     *
     * Esta función toma el nombre de una entidad base y el nombre de una entidad de tipo de morosidad.
     * Valida que la entidad base no esté vacía y, si la entidad de tipo de morosidad no se proporciona (está vacía),
     * asigna automáticamente el valor de la entidad base.
     *
     * @param string $entidad_base El nombre de la entidad base. Este valor es obligatorio y no puede estar vacío.
     * @param string $entidad_tipo_morosidad El nombre de la entidad de tipo de morosidad.
     *                                       Si está vacío, se usará el nombre de la entidad base.
     *
     * @return string|array Devuelve el nombre de la entidad de tipo de morosidad. Si la entidad base está vacía,
     *                      devuelve un array con el error correspondiente.
     *
     * @example
     * ```php
     * // Ejemplo 1: Entidad de tipo de morosidad proporcionada
     * $entidad_tipo_morosidad = $this->entidad_tipo_morosidad('contrato', 'tipo_morosidad');
     * // Resultado: 'tipo_morosidad'
     *
     * // Ejemplo 2: Entidad de tipo de morosidad vacía
     * $entidad_tipo_morosidad = $this->entidad_tipo_morosidad('contrato', '');
     * // Resultado: 'contrato'
     *
     * // Ejemplo 3: Entidad base vacía
     * $entidad_tipo_morosidad = $this->entidad_tipo_morosidad('', 'tipo_morosidad');
     * // Resultado:
     * // ['error' => 'Error $entidad_base esta vacia', 'data' => '']
     * ```
     */
    private function entidad_tipo_morosidad(string $entidad_base, string $entidad_tipo_morosidad)
    {
        $entidad_base = trim($entidad_base);
        if ($entidad_base === '') {
            return $this->error->error('Error $entidad_base esta vacia', $entidad_base);
        }

        $entidad_tipo_morosidad = trim($entidad_tipo_morosidad);
        if ($entidad_tipo_morosidad === '') {
            $entidad_tipo_morosidad = $entidad_base;
        }

        return $entidad_tipo_morosidad;
    }


    /**
     * EM3
     * Genera un objeto con las entidades relacionadas a una entidad base.
     *
     * Esta función valida que el nombre de la entidad base no esté vacío. Luego, utiliza métodos específicos
     * para obtener y asignar valores de las entidades relacionadas, como el cliente, el estado, y el tipo de morosidad.
     *
     * @param string $entidad_base El nombre de la entidad base. Este valor es obligatorio y no puede estar vacío.
     * @param string $entidad_cliente El nombre de la entidad cliente. Si está vacío, se asignará el valor de la entidad base.
     * @param string $entidad_status El nombre de la entidad de estado. Si está vacío, se asignará el valor de la entidad base.
     * @param string $entidad_tipo_morosidad El nombre de la entidad de tipo de morosidad. Si está vacío, se asignará el valor de la entidad base.
     *
     * @return stdClass|array Devuelve un objeto con las entidades asignadas (`entidad_status`, `entidad_tipo_morosidad`,
     *                        `entidad_cliente`). Si ocurre un error, devuelve un array con el detalle del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Todos los parámetros válidos
     * $entidades = $this->entidades_base('contrato', 'cliente', 'status', 'tipo_morosidad');
     * // Resultado:
     * // stdClass {
     * //     entidad_status: 'status',
     * //     entidad_tipo_morosidad: 'tipo_morosidad',
     * //     entidad_cliente: 'cliente'
     * // }
     *
     * // Ejemplo 2: Algunos parámetros vacíos
     * $entidades = $this->entidades_base('contrato', '', '', '');
     * // Resultado:
     * // stdClass {
     * //     entidad_status: 'contrato',
     * //     entidad_tipo_morosidad: 'contrato',
     * //     entidad_cliente: 'contrato'
     * // }
     *
     * // Ejemplo 3: Error en entidad base
     * $entidades = $this->entidades_base('', 'cliente', 'status', 'tipo_morosidad');
     * // Resultado:
     * // ['error' => 'Error $entidad_base esta vacia', 'data' => '']
     * ```
     */
    private function entidades_base(
        string $entidad_base,
        string $entidad_cliente,
        string $entidad_status,
        string $entidad_tipo_morosidad
    ) {
        // Validar que la entidad base no esté vacía
        $entidad_base = trim($entidad_base);
        if ($entidad_base === '') {
            return $this->error->error('Error $entidad_base esta vacia', $entidad_base);
        }

        // Obtener y validar la entidad de estado
        $entidad_status = $this->entidad_status($entidad_base, $entidad_status);
        if (error::$en_error) {
            return $this->error->error('Error al obtener $entidad_status', $entidad_status);
        }

        // Obtener y validar la entidad de tipo de morosidad
        $entidad_tipo_morosidad = $this->entidad_tipo_morosidad($entidad_base, $entidad_tipo_morosidad);
        if (error::$en_error) {
            return $this->error->error('Error al obtener $entidad_tipo_morosidad', $entidad_tipo_morosidad);
        }

        // Obtener y validar la entidad cliente
        $entidad_cliente = $this->entidad_cliente($entidad_base, $entidad_cliente);
        if (error::$en_error) {
            return $this->error->error('Error al obtener $entidad_cliente', $entidad_cliente);
        }

        // Crear y devolver un objeto con las entidades asignadas
        $entidades = new stdClass();
        $entidades->entidad_status = $entidad_status;
        $entidades->entidad_tipo_morosidad = $entidad_tipo_morosidad;
        $entidades->entidad_cliente = $entidad_cliente;

        return $entidades;
    }


    /**
     * TRASLADADO
     * Genera una cadena SQL que representa un campo específico de la tabla de contratos en una base de datos.
     *
     * Esta función toma el nombre de un campo y el nombre de una base de datos, valida que no estén vacíos
     * y genera una cadena SQL con el formato adecuado para referenciar dicho campo en la tabla de contratos.
     *
     * @param string $campo_condicion_pago El nombre del campo en la tabla de contratos.
     * @param string $name_database El nombre de la base de datos donde se encuentra la tabla de contratos.
     * @return string|array La cadena SQL con el campo formateado si ambos parámetros son válidos,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function genera_campo_contrato(string $campo_condicion_pago, string $name_database)
    {
        $name_database = trim($name_database);
        if($name_database === ''){
            return $this->error->error('Error $name_database esta vacia', $name_database);
        }
        $campo_condicion_pago = trim($campo_condicion_pago);
        if($campo_condicion_pago === ''){
            return $this->error->error('Error $campo_condicion_pago esta vacia', $campo_condicion_pago);
        }
        return "$name_database.contrato.$campo_condicion_pago";
    }

    /**
     * TRASLADADO
     * Genera la consulta SQL para obtener contratos no visitados.
     *
     * Esta función valida los parámetros de entrada, obtiene los contratos activos,
     * genera la tabla de contratos y finalmente crea la consulta SQL para obtener los contratos no visitados.
     *
     * @param string $campo_condicion_pago El campo que indica la condición de pago.
     * @param stdClass $campos_custom Los campos personalizados que deben validarse.
     * @param string $name_database El nombre de la base de datos.
     * @param array $periodicidades Un array que contiene las periodicidades a validar.
     * @return string|array Devuelve la consulta SQL para obtener contratos no visitados si todos los parámetros son válidos,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function genera_sql_no_visit(string $campo_condicion_pago, stdClass $campos_custom,
                                              string $name_database, array $periodicidades)
    {
        $valida = (new _valida())->valida_no_visit($campo_condicion_pago,$campos_custom,$name_database,$periodicidades);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }

        $contratos_activos_sql = $this->contratos_activos($name_database, $campos_custom);
        if(error::$en_error){
            return $this->error->error('Error al obtener contratos activos', $contratos_activos_sql);
        }

        $tabla_contratos_sql = $this->genera_tabla_contratos(
            $campos_custom,$campo_condicion_pago,$name_database,$periodicidades);
        if(error::$en_error){
            return $this->error->error('Error al obtener $tabla_contratos_sql', $tabla_contratos_sql);
        }

        $sql = $this->sql_no_visit($contratos_activos_sql,$tabla_contratos_sql,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al obtener $sql', $sql);

        }
        return $sql;

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener información de contratos basándose en las condiciones especificadas.
     *
     * Esta función valida los campos personalizados y el nombre de la base de datos, y luego construye una consulta SQL
     * para obtener información sobre los contratos, incluyendo la condición de pago, los días desde la última visita,
     * y si el contrato tiene restos, basada en las periodicidades especificadas.
     *
     * @param stdClass $campos_custom Un objeto que contiene los campos personalizados necesarios para la consulta.
     * @param string $campo_condicion_pago El nombre del campo de condición de pago.
     * @param string $name_database El nombre de la base de datos.
     * @param array $periodicidades Un array de arrays asociativos, cada uno con las claves 'key' y 'val' que representan la condición y el valor a evaluar.
     * @return string|array La cadena SQL completa para la consulta si todo es válido,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function genera_tabla_contratos(stdClass $campos_custom, string $campo_condicion_pago,
                                                 string $name_database, array $periodicidades)
    {
        $valida = (new _valida())->valida_no_visit($campo_condicion_pago,$campos_custom,$name_database,$periodicidades);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }

        $params = $this->params_sql_contratos($campos_custom,$name_database);
        if(error::$en_error){
           return $this->error->error('Error al obtener $params', $params);
        }

        $tabla_contratos = $this->tabla_contratos($campo_condicion_pago,
            $params->dias_ultima_visita,$name_database,$periodicidades,$params->restos_sql,$params->where_sql);
        if(error::$en_error){
            return $this->error->error('Error al obtener $tabla_contratos', $tabla_contratos);
        }

        return $tabla_contratos;

    }

    final public function genera_union_no_visit(array $databases)
    {
        foreach ($databases as $name_database=>$database){
            $sql  = $this->no_visit($name_database,$database['tipo']);
            if(error::$en_error){
                return $this->error->error('Error al obtener $sql', $sql);
            }
            $databases[$name_database]['sql'] = $sql;
        }

        $sql_rs = $this->union_no_visit($databases);
        if(error::$en_error){
            return $this->error->error('Error al integrar $sql', $sql_rs);
        }
        return $sql_rs;

    }

    /**
     * TRASLADADO
     * Genera una cláusula WHEN SQL basada en múltiples condiciones de periodicidad.
     *
     * Esta función toma un array de arrays asociativos, cada uno conteniendo las claves 'key' y 'val', y construye
     * una parte de una declaración CASE SQL para evaluar condiciones específicas de periodicidad. Valida que las
     * claves de cada array sean correctas y que los parámetros no estén vacíos antes de construir la cláusula WHEN.
     *
     * @param array $periodicidades Un array de arrays asociativos, cada uno con las claves 'key' y 'val' que representan la condición y el valor a evaluar.
     * @param string $when_ccp La condición que se va a evaluar en la cláusula WHEN.
     * @return string|array La cadena SQL de la cláusula WHERE actualizada con las cláusulas WHEN generadas si todo es válido,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function genera_when_periodicidad(array $periodicidades, string $when_ccp)
    {
        if(count($periodicidades) === 0){
            return $this->error->error('Error $periodicidades esta vacia', $periodicidades);
        }
        $when_ccp = trim($when_ccp);
        if($when_ccp === ''){
            return $this->error->error('Error $when_ccp esta vacio', $when_ccp);
        }
        $w_per = '';
        foreach ($periodicidades as $periodicidad){
            if(!is_array($periodicidad)){
                return $this->error->error('Error $periodicidad debe ser un array', $periodicidad);
            }
            $keys = array('key','val');
            $valida = (new valida())->valida_keys($keys,$periodicidad);
            if(error::$en_error){
                return $this->error->error('Error al validar periodicidad', $valida);
            }

            $w_per = $this->when_periodicidad($periodicidad,$w_per,$when_ccp);
            if(error::$en_error){
                return $this->error->error('Error al generar when_periodicidad', $w_per);
            }
        }
        return $w_per;

    }

    /**
     * TRASLADADO
     * Inicializa y valida los campos personalizados.
     *
     * Esta función valida que los nombres y valores de ciertos campos personalizados estén presentes y sean válidos.
     * Luego, inicializa un nuevo objeto con los campos personalizados y sus valores.
     * Si se encuentran errores durante la validación, se devuelve un array con información del error.
     *
     * @param stdClass $campos_custom Un objeto que contiene los campos personalizados a validar e inicializar.
     * @return stdClass|array Un objeto con los campos personalizados inicializados.
     *                        En caso de error, se devuelve un array con información del error.
     */
    private function init_campos(stdClass $campos_custom)
    {
        $valida = (new _valida())->valida_campos_name($campos_custom);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }

        $campo_estatus = $campos_custom->campo_estatus->name;
        $value_status = $campos_custom->campo_estatus->value;
        $campo_fecha_valida = $campos_custom->campo_fecha_valida->name;
        $campo_serie = $campos_custom->campo_serie->name;
        $campo_monto_pagado = $campos_custom->campo_monto_pagado->name;
        $campo_monto_total = $campos_custom->campo_monto_total->name;
        $campo_folio = $campos_custom->campo_folio->name;

        $campos = new stdClass();
        $campos->estatus = $campo_estatus;
        $campos->estatus_val = $value_status;
        $campos->fecha = $campo_fecha_valida;
        $campos->serie = $campo_serie;
        $campos->monto_p = $campo_monto_pagado;
        $campos->monto_t = $campo_monto_total;
        $campos->folio = $campo_folio;

        return $campos;

    }

    /**
     * Genera un LEFT JOIN para la entidad `cp` (código postal) en el contexto de cobranza.
     *
     * Este método valida los campos requeridos y construye una instrucción SQL `LEFT JOIN` para
     * enlazar la tabla `cp` con la entidad `localidad` utilizada en el contexto de cobranza.
     *
     * @param stdClass $campos Objeto que contiene los campos necesarios para generar el JOIN.
     *                         - `entidad_cp` (string): Nombre del alias para la tabla `cp`.
     *                         - `entidad_localidad` (string): Nombre del alias para la tabla `localidad`.
     *
     * @return string|array Devuelve la instrucción SQL `LEFT JOIN` como cadena si los datos son válidos.
     *                      En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * @throws error Lanza un error si:
     *         - Los campos `entidad_cp` o `entidad_localidad` no están definidos en `$campos`.
     *         - El valor de `entidad_cp` no es igual a `cp_cobranza`.
     *
     * @example
     * ```php
     * $campos = new stdClass();
     * $campos->entidad_cp = 'cp_cobranza';
     * $campos->entidad_localidad = 'localidad';
     *
     * $sql = $this->left_join_cp_cobranza($campos);
     * echo $sql;
     * // Resultado: "LEFT JOIN cp AS cp_cobranza ON cp_cobranza.id = localidad.cp_id"
     * ```
     *
     * @example
     * ```php
     * $campos = new stdClass();
     * $campos->entidad_cp = 'otra_entidad';
     * $campos->entidad_localidad = 'localidad';
     *
     * $sql = $this->left_join_cp_cobranza($campos);
     * echo $sql;
     * // Resultado: ""
     * ```
     */
    private function left_join_cp_cobranza(stdClass $campos)
    {
        // Valida que los campos necesarios estén definidos y no sean vacíos
        $keys = array('entidad_cp', 'entidad_localidad');
        $valida = (new valida())->valida_keys($keys, $campos);
        if (error::$en_error) {
            return $this->error->error('Error al validar campos', $valida);
        }

        $left_join_cp_cobranza = '';

        // Verifica si la entidad de código postal es "cp_cobranza"
        if ($campos->entidad_cp === 'cp_cobranza') {
            $left_join_cp_cobranza = "LEFT JOIN cp AS $campos->entidad_cp 
        ON $campos->entidad_cp.id = $campos->entidad_localidad.cp_id";
        }

        return $left_join_cp_cobranza;
    }


    /**
     * Genera una instrucción SQL `LEFT JOIN` para la tabla de domicilio de cobranza.
     *
     * Este método construye una cláusula SQL `LEFT JOIN` entre la tabla base y la tabla
     * `domicilio_contrato` para asociar el domicilio de cobranza, siempre y cuando
     * la entidad de localidad sea igual a `domicilio_cobranza`.
     *
     * @param stdClass $campos Objeto que contiene información sobre los campos de las entidades.
     *                         - `$campos->entidad_localidad` (string): Nombre de la entidad de localidad.
     * @param string $entidad_base Nombre de la tabla base (entidad principal).
     *
     * @return string Devuelve una instrucción SQL `LEFT JOIN` si la entidad de localidad es `domicilio_cobranza`.
     *                Si no, devuelve una cadena vacía.
     *
     * @example
     * // Ejemplo 1: La entidad de localidad es `domicilio_cobranza`
     * $campos = new stdClass();
     * $campos->entidad_localidad = 'domicilio_cobranza';
     * $entidad_base = 'contrato';
     * $left_join = $this->left_join_domicilio_cobranza($campos, $entidad_base);
     * echo $left_join;
     * // Resultado:
     * // LEFT JOIN domicilio_contrato AS domicilio_cobranza
     * // ON domicilio_cobranza.contrato_id = contrato.id
     * // AND domicilio_cobranza.tipo_domicilio_id = 1
     *
     * // Ejemplo 2: La entidad de localidad no es `domicilio_cobranza`
     * $campos = new stdClass();
     * $campos->entidad_localidad = 'otra_entidad';
     * $entidad_base = 'contrato';
     * $left_join = $this->left_join_domicilio_cobranza($campos, $entidad_base);
     * echo $left_join;
     * // Resultado:
     * // (cadena vacía)
     *
     * @throws error Lanza un error si alguno de los parámetros está vacío o no está correctamente definido.
     */
    private function left_join_domicilio_cobranza(stdClass $campos, string $entidad_base): string
    {
        // Inicializa la cláusula LEFT JOIN como una cadena vacía
        $left_join_domicilio_cobranza = '';

        // Genera la clave primaria de la tabla base
        $key_id_base = $entidad_base . '_id';

        // Verifica si la entidad de localidad es 'domicilio_cobranza'
        if ($campos->entidad_localidad === 'domicilio_cobranza') {
            // Construye la cláusula LEFT JOIN
            $left_join_domicilio_cobranza = "LEFT JOIN 
            domicilio_contrato AS $campos->entidad_localidad 
            ON $campos->entidad_localidad.$key_id_base = $entidad_base.id 
            AND $campos->entidad_localidad.tipo_domicilio_id = 1";
        }

        // Devuelve la cláusula LEFT JOIN o una cadena vacía
        return $left_join_domicilio_cobranza;
    }


    /**
     * Genera una instrucción SQL `LEFT JOIN` para enlazar una entidad de empleado.
     *
     * Este método construye dinámicamente un `LEFT JOIN` entre una entidad base y una entidad de empleado,
     * utilizando las claves correspondientes para realizar el enlace.
     *
     * @param string $entidad_base Nombre de la tabla base en la consulta SQL.
     * @param string $entidad_empleado Nombre de la entidad o tabla de empleado que se enlazará.
     *
     * @return string Devuelve la instrucción SQL `LEFT JOIN` generada como una cadena.
     *
     * @example
     * ```php
     * // Caso 1: entidad_empleado es 'empleado'
     * $entidad_base = 'contrato';
     * $entidad_empleado = 'empleado';
     * $sql = $this->left_join_empleado($entidad_base, $entidad_empleado);
     * echo $sql;
     * // Resultado: "LEFT JOIN empleado ON empleado.id = contrato.empleado_gestor_id"
     *
     * // Caso 2: entidad_empleado no es 'empleado'
     * $entidad_base = 'cliente';
     * $entidad_empleado = 'supervisor';
     * $sql = $this->left_join_empleado($entidad_base, $entidad_empleado);
     * echo $sql;
     * // Resultado: "LEFT JOIN supervisor ON supervisor.id = cliente.supervisor_id"
     * ```
     *
     * @throws error No lanza excepciones explícitas pero debe ser utilizada con datos validados previamente.
     */
    private function left_join_empleado(string $entidad_base, string $entidad_empleado): string
    {
        // Construye la clave primaria de la entidad de empleado
        $key_empleado_id = $entidad_empleado . '_id';

        // Genera el LEFT JOIN predeterminado
        $left_join_empleado = "LEFT JOIN $entidad_empleado ON $entidad_empleado.id = cliente.$key_empleado_id";

        // Ajusta la clave si la entidad de empleado es 'empleado'
        if ($entidad_empleado === 'empleado') {
            $left_join_empleado =
                "LEFT JOIN $entidad_empleado ON $entidad_empleado.id = $entidad_base.empleado_gestor_id";
        }

        // Retorna la instrucción SQL generada
        return $left_join_empleado;
    }


    /**
     * EM3
     * Genera una cláusula SQL de tipo LEFT JOIN para la tabla de empleados.
     *
     * Esta función construye una cláusula SQL `LEFT JOIN` para unir la tabla de empleados con
     * otra tabla, como `cliente` o `contrato`, dependiendo de los parámetros proporcionados.
     * También valida que los parámetros necesarios no estén vacíos.
     *
     * @param string $entidad_empleado El nombre de la entidad de empleados.
     * @param string $key_empleado_gestor_id El campo que conecta con el gestor del contrato.
     *                                       Si está vacío, se usará la relación con cliente.
     * @param string $key_empleado_id El campo que conecta con la tabla de empleados.
     * @param string $name_database El nombre de la base de datos donde reside la tabla.
     *
     * @return string|array Devuelve la cláusula SQL generada como una cadena.
     *                      En caso de error, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Relación entre cliente y empleado
     * $join = $this->left_join_empleado_base('empleado', '', 'empleado_id', 'mi_base_datos');
     * // Resultado:
     * // LEFT JOIN mi_base_datos.empleado AS empleado ON cliente.empleado_id = empleado.id
     *
     * // Ejemplo 2: Relación entre contrato y gestor
     * $join = $this->left_join_empleado_base('empleado', 'empleado_gestor_id', 'empleado_id', 'mi_base_datos');
     * // Resultado:
     * // LEFT JOIN mi_base_datos.empleado AS empleado ON contrato.empleado_gestor_id = empleado.id
     *
     * // Ejemplo 3: Error por base de datos vacía
     * $join = $this->left_join_empleado_base('empleado', '', 'empleado_id', '');
     * // Resultado:
     * // ['error' => 'Error $name_database esta vacia', 'data' => '']
     *
     * // Ejemplo 4: Error por entidad vacía
     * $join = $this->left_join_empleado_base('', '', 'empleado_id', 'mi_base_datos');
     * // Resultado:
     * // ['error' => 'Error $entidad_empleado esta vacia', 'data' => '']
     * ```
     */

    private function left_join_empleado_base(string $entidad_empleado, string $key_empleado_gestor_id,
                                             string $key_empleado_id, string $name_database)
    {
        $name_database = trim($name_database);
        if($name_database === ''){
            return (new error())->error('Error $name_database esta vacia',$name_database);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacia',$key_empleado_id);
        }
        $key_empleado_gestor_id = trim($key_empleado_gestor_id);

        $tb_empleado = "$name_database.$entidad_empleado AS $entidad_empleado";

        $left_join_empleado = "LEFT JOIN $tb_empleado ON cliente.$key_empleado_id = $entidad_empleado.id";
        if($key_empleado_gestor_id !==''){
            $left_join_empleado = "LEFT JOIN $tb_empleado ON contrato.$key_empleado_gestor_id = $entidad_empleado.id";
        }
        return $left_join_empleado;

    }

    /**
     * Genera una instrucción SQL `LEFT JOIN` para asociar municipios de cobranza.
     *
     * Este método construye una cláusula SQL `LEFT JOIN` entre la tabla de municipios
     * y la tabla base indicada en `$campos->entidad_localidad`, siempre y cuando la
     * entidad de municipio sea igual a `municipio_cobranza`.
     *
     * @param stdClass $campos Objeto que contiene información sobre las entidades involucradas.
     *                         - `$campos->entidad_municipio` (string): Nombre de la entidad de municipio.
     *                         - `$campos->entidad_localidad` (string): Nombre de la entidad de localidad.
     *
     * @return string Devuelve una instrucción SQL `LEFT JOIN` si la entidad de municipio es `municipio_cobranza`.
     *                Si no, devuelve una cadena vacía.
     *
     * @example
     * // Ejemplo 1: La entidad de municipio es `municipio_cobranza`
     * $campos = new stdClass();
     * $campos->entidad_municipio = 'municipio_cobranza';
     * $campos->entidad_localidad = 'domicilio_cobranza';
     * $left_join = $this->left_join_municipio_cobranza($campos);
     * echo $left_join;
     * // Resultado:
     * // LEFT JOIN municipio AS municipio_cobranza
     * // ON municipio_cobranza.id = domicilio_cobranza.municipio_id
     *
     * // Ejemplo 2: La entidad de municipio no es `municipio_cobranza`
     * $campos = new stdClass();
     * $campos->entidad_municipio = 'otra_entidad';
     * $campos->entidad_localidad = 'domicilio_cobranza';
     * $left_join = $this->left_join_municipio_cobranza($campos);
     * echo $left_join;
     * // Resultado:
     * // (cadena vacía)
     *
     * @throws error Lanza un error si los campos requeridos no están definidos o están vacíos.
     */
    private function left_join_municipio_cobranza(stdClass $campos): string
    {
        // Inicializa la cláusula LEFT JOIN como una cadena vacía
        $left_join_municipio_cobranza = '';

        // Verifica si la entidad de municipio es 'municipio_cobranza'
        if ($campos->entidad_municipio === 'municipio_cobranza') {
            // Construye la cláusula LEFT JOIN
            $left_join_municipio_cobranza = "LEFT JOIN municipio AS $campos->entidad_municipio 
            ON $campos->entidad_municipio.id = $campos->entidad_localidad.municipio_id";
        }

        // Devuelve la cláusula LEFT JOIN o una cadena vacía
        return $left_join_municipio_cobranza;
    }



    /**
     * TRASLADADO
     * Genera una cláusula SQL `LEFT JOIN` para unir una tabla base con una tabla de estado de contrato.
     *
     * Esta función valida que la entidad base y la entidad de estado estén definidas y no estén vacías.
     * Si la entidad de estado es diferente de la entidad base, genera la cláusula `LEFT JOIN` correspondiente
     * para unir la tabla base con la tabla de estado de contrato. Si alguna validación falla, se devuelve un
     * error.
     *
     * @param string $entidad_base El nombre de la entidad base en la consulta SQL.
     * @param stdClass $entidades Un objeto que contiene la entidad de estado (`entidad_status`).
     *
     * @return string|array Retorna una cadena con la cláusula `LEFT JOIN` generada. En caso de error,
     * retorna un array con los detalles del error.
     */
    private function left_join_status_contrato(string $entidad_base, stdClass $entidades)
    {
        $entidad_base = trim($entidad_base);
        if($entidad_base === ''){
            return (new error())->error('Error $entidad_base esta vacia',$entidad_base);
        }
        if(!isset($entidades->entidad_status)){
            return (new error())->error('Error $entidades->entidad_status no existe',$entidades);
        }
        if(trim($entidades->entidad_status) === ''){
            return (new error())->error('Error $entidades->entidad_status esta vacia',$entidades);
        }

        $left_join_status_contrato = '';
        if($entidades->entidad_status !== $entidad_base){
            $key_status_contrato = $entidades->entidad_status.'_id';
            $lf_status = "$entidades->entidad_status AS $entidades->entidad_status";
            $on = "$entidades->entidad_status.id = $entidad_base.$key_status_contrato";
            $left_join_status_contrato = "LEFT JOIN $lf_status ON $on";
        }

        return $left_join_status_contrato;

    }

    /**
     * Genera una instrucción SQL `LEFT JOIN` para la tabla de tipo de morosidad.
     *
     * Este método construye una cláusula SQL `LEFT JOIN` entre la entidad base y la entidad
     * de tipo de morosidad, siempre que estas sean diferentes.
     *
     * @param string $entidad_base Nombre de la tabla base (entidad principal).
     * @param stdClass $entidades Objeto que contiene información sobre las entidades involucradas.
     *                            - `$entidades->entidad_tipo_morosidad` (string): Nombre de la entidad de tipo de morosidad.
     *
     * @return string Devuelve una instrucción SQL `LEFT JOIN` si las entidades son diferentes.
     *                Si las entidades son iguales, devuelve una cadena vacía.
     *
     * @example
     * // Ejemplo 1: Las entidades son diferentes
     * $entidad_base = 'contratos';
     * $entidades = new stdClass();
     * $entidades->entidad_tipo_morosidad = 'tipo_morosidad';
     * $left_join = $this->left_join_tipo_morosidad($entidad_base, $entidades);
     * echo $left_join;
     * // Resultado:
     * // LEFT JOIN tipo_morosidad AS tipo_morosidad
     * // ON tipo_morosidad.id = contratos.tipo_morosidad_id
     *
     * // Ejemplo 2: Las entidades son iguales
     * $entidad_base = 'tipo_morosidad';
     * $entidades = new stdClass();
     * $entidades->entidad_tipo_morosidad = 'tipo_morosidad';
     * $left_join = $this->left_join_tipo_morosidad($entidad_base, $entidades);
     * echo $left_join;
     * // Resultado:
     * // (cadena vacía)
     *
     * @throws error Lanza un error si alguno de los parámetros está vacío o si no están correctamente definidos.
     */
    private function left_join_tipo_morosidad(string $entidad_base, stdClass $entidades): string
    {
        // Inicializa la cláusula LEFT JOIN como una cadena vacía
        $left_join_tipo_morosidad = '';

        // Verifica si las entidades son diferentes
        if ($entidades->entidad_tipo_morosidad !== $entidad_base) {
            // Genera la clave para la relación entre las entidades
            $key_tipo_morosidad = $entidades->entidad_tipo_morosidad . '_id';

            // Construye la cláusula LEFT JOIN
            $left_join_tipo_morosidad = "LEFT JOIN $entidades->entidad_tipo_morosidad AS $entidades->entidad_tipo_morosidad 
            ON $entidades->entidad_tipo_morosidad.id = $entidad_base.$key_tipo_morosidad";
        }

        // Devuelve la cláusula LEFT JOIN o una cadena vacía
        return $left_join_tipo_morosidad;
    }


    /**
     * EM3
     * Genera una consulta SQL para obtener datos relacionados con las metas de un gestor.
     *
     * Esta función valida los datos de entrada y construye una consulta SQL para recuperar
     * información de las metas asignadas a un gestor, incluyendo datos como plaza, fechas,
     * gestor, contratos congelados, y montos relacionados con las metas.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado (ejemplo: 'empleado').
     * @param string $fecha_fin Fecha de fin del rango para filtrar los datos de metas (formato 'YYYY-MM-DD').
     * @param string $fecha_inicio Fecha de inicio del rango para filtrar los datos de metas (formato 'YYYY-MM-DD').
     * @param int $meta_gestor_id Identificador único de la meta del gestor.
     *
     * @return string|array Retorna una consulta SQL como cadena. Si ocurre algún error en la validación,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo de uso con datos válidos
     * $sql = $this->metas('empleado', '2025-01-31', '2025-01-01', 123);
     * // Resultado:
     * // SELECT plaza.descripcion AS 'PLAZA', '2025-01-01' AS 'FECHA INICIO',
     * //        '2025-01-31' AS 'FECHA FIN', empleado.id AS 'GESTOR ID',
     * //        empleado.nombre_completo as 'GESTOR',
     * //        IFNULL(meta_gestor_concentrado.n_contratos,'NO TIENE') AS 'CONTRATOS CONGELADOS',
     * //        IFNULL(ROUND(meta_gestor_concentrado.monto_total,2),'NO TIENE') AS 'META',
     * //        ...
     * // FROM meta_gestor_concentrado
     * // LEFT JOIN empleado ON empleado.id = meta_gestor_concentrado.empleado_id
     * // LEFT JOIN plaza ON empleado.plaza_id = plaza.id
     * // LEFT JOIN meta_gestor ON meta_gestor_concentrado.meta_gestor_id = meta_gestor.id
     * // WHERE meta_gestor.id = 123
     *
     * // Ejemplo con error en los datos de entrada
     * $sql = $this->metas('', '2025-01-31', '2025-01-01', 123);
     * // Resultado:
     * // ['error' => 'Error $entidad_empleado esta vacia', 'data' => '']
     *
     * $sql = $this->metas('empleado', '', '2025-01-01', 123);
     * // Resultado:
     * // ['error' => 'Error $fecha_fin esta vacia', 'data' => '']
     *
     * $sql = $this->metas('empleado', '2025-01-31', '', 123);
     * // Resultado:
     * // ['error' => 'Error $fecha_inicio esta vacia', 'data' => '']
     *
     * $sql = $this->metas('empleado', '2025-01-31', '2025-01-01', -1);
     * // Resultado:
     * // ['error' => 'Error $meta_gestor_id debe ser mayor a 0', 'data' => -1]
     * ```
     */
    final public function metas(string $entidad_empleado, string $fecha_fin, string $fecha_inicio, int $meta_gestor_id)
    {
        $entidad_empleado = trim($entidad_empleado);
        if ($entidad_empleado === '') {
            return $this->error->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }

        $fecha_fin = trim($fecha_fin);
        if ($fecha_fin === '') {
            return $this->error->error('Error $fecha_fin esta vacia', $fecha_fin);
        }

        $fecha_inicio = trim($fecha_inicio);
        if ($fecha_inicio === '') {
            return $this->error->error('Error $fecha_inicio esta vacia', $fecha_inicio);
        }

        if ($meta_gestor_id <= 0) {
            return $this->error->error('Error $meta_gestor_id debe ser mayor a 0', $meta_gestor_id);
        }

        $plaza = "plaza.descripcion AS 'PLAZA'";
        $fecha_i = "'$fecha_inicio' AS 'FECHA INICIO'";
        $fecha_f = "'$fecha_fin' AS 'FECHA FIN'";
        $gestor_id = "$entidad_empleado.id AS  'GESTOR ID'";
        $gestor = "$entidad_empleado.nombre_completo as 'GESTOR'";

        $congelados = "IFNULL(meta_gestor_concentrado.n_contratos,'NO TIENE') AS 'CONTRATOS CONGELADOS'";
        $meta = "IFNULL(ROUND(meta_gestor_concentrado.monto_total,2),'NO TIENE') AS 'META'";
        $cob_meta = "IFNULL(ROUND(meta_gestor_concentrado.meta_proceso,2),0) AS 'COBRADO META'";
        $cob_meta_mas = "IFNULL(ROUND(meta_gestor_concentrado.monto_de_mas,2),0) AS 'COBRADO META DE MAS'";
        $cob_con_nuevos = "IFNULL(ROUND(meta_gestor_concentrado.monto_nuevo,2),0) AS 'COBRADO CONTRATOS NUEVOS'";
        $cob_con_extra = "IFNULL(ROUND(meta_gestor_concentrado.monto_contratos_extra,2),0) AS 'COBRADO CONTRATOS EXTRA'";

        $key_empleado_id = $entidad_empleado . '_id';
        $join_mgc = "$entidad_empleado ON $entidad_empleado.id = meta_gestor_concentrado.$key_empleado_id";
        $join_plaza = "plaza ON $entidad_empleado.plaza_id = plaza.id";
        $join_mgc_emp = "meta_gestor ON meta_gestor_concentrado.meta_gestor_id = meta_gestor.id";

        $campos_gen = "$plaza, $fecha_i, $fecha_f, $gestor_id,  $gestor";
        $campos_dat = "$congelados, $meta, $cob_meta, $cob_meta_mas, $cob_con_nuevos, $cob_con_extra";
        $campos = "$campos_gen, $campos_dat";

        $joins = "LEFT JOIN $join_mgc LEFT JOIN $join_plaza LEFT JOIN $join_mgc_emp";
        $where = "meta_gestor.id = $meta_gestor_id";

        return /** @lang MYSQL */ "SELECT $campos FROM meta_gestor_concentrado $joins WHERE $where";
    }




    private function no_visit(string $name_database, string $tipo)
    {
        $sql = '';
        if($tipo === 'sap'){
            $sql  = $this->sql_no_visit_sap($name_database);
            if(error::$en_error){
                return $this->error->error('Error al obtener $sql', $sql);
            }
        }
        if($tipo === 'em3'){
            $sql  = $this->sql_no_visit_em3($name_database);
            if(error::$en_error){
                return $this->error->error('Error al obtener $sql', $sql);
            }
        }
        return $sql;

    }

    /**
     * EM3
     * Genera un objeto con los campos asignados a partir de los datos proporcionados.
     *
     * Esta función toma un objeto `$campos` como entrada y utiliza un conjunto predefinido
     * de datos de campos (`campos_data`) para mapear y asignar valores. Si un campo ya está
     * definido en `$campos` y no está vacío, se utiliza ese valor; de lo contrario, se asigna
     * un valor base desde `campos_data`.
     *
     * @param stdClass $campos Un objeto que contiene valores de campos personalizados o vacíos.
     *
     * @return stdClass|array Devuelve un objeto con los campos asignados. Si ocurre un error
     *                        durante el proceso, retorna un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campos personalizados proporcionados
     * $campos = new stdClass();
     * $campos->campo_fecha_valida = 'FechaCreacion';
     * $result = $this->obj_campos($campos);
     * // Resultado:
     * // stdClass {
     * //     campo_fecha_valida: 'FechaCreacion',
     * //     campo_status_contrato: 'U_StatusCob',
     * //     campo_status_contrato_def: 'Canceled'
     * // }
     *
     * // Ejemplo 2: Campos predeterminados utilizados
     * $campos = new stdClass();
     * $result = $this->obj_campos($campos);
     * // Resultado:
     * // stdClass {
     * //     campo_fecha_valida: 'DocDate',
     * //     campo_status_contrato: 'U_StatusCob',
     * //     campo_status_contrato_def: 'Canceled'
     * // }
     *
     * // Ejemplo 3: Error durante la asignación
     * $campos = new stdClass();
     * unset($this->campos_data_contrato); // Simulando un error
     * $result = $this->obj_campos($campos);
     * // Resultado:
     * // ['error' => 'Error al asignar $campos_data', 'data' => null]
     * ```
     */
    private function obj_campos(stdClass $campos)
    {
        $campos_data = $this->campos_data_contrato();
        if (error::$en_error) {
            return $this->error->error('Error al asignar $campos_data', $campos_data);
        }

        $obj_campos = new stdClass();
        foreach ($campos_data as $key_campo_asignar => $name_base) {
            $campo = $this->asigna_key_campo($campos, $key_campo_asignar, $name_base);
            if (error::$en_error) {
                return $this->error->error('Error al asignar campo', $campo);
            }
            $obj_campos->$key_campo_asignar = $campo;
        }

        return $obj_campos;
    }


    /**
     * EM3
     * Genera una consulta SQL para obtener los pagos determinados dentro de un período especificado.
     *
     * Esta función valida los datos de entrada, genera una consulta SQL que incluye cálculos
     * como el total cobrado (`COBRADO_GENERAL`) y agrupa los resultados por empleado. La consulta
     * incluye validaciones de fechas, movimiento, estatus y plaza.
     *
     * @param string $campo_fecha_valida Campo de la fecha a validar en la tabla de pagos.
     * @param string $campo_pago_total Campo que representa el total del pago.
     * @param string $campo_movimiento Campo que representa el tipo de movimiento del pago.
     * @param string $entidad_empleado Nombre de la entidad del empleado.
     * @param string $fecha_fin Fecha final del período a consultar (formato: YYYY-MM-DD).
     * @param string $fecha_inicio Fecha inicial del período a consultar (formato: YYYY-MM-DD).
     * @param string $name_database Nombre de la base de datos donde se encuentran las tablas.
     * @param string $plaza_id ID de la plaza a filtrar.
     *
     * @return string|array Retorna una cadena SQL generada para ejecutar la consulta.
     *                      Si ocurre un error en las validaciones de entrada, devuelve un array con el error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $query = $this->pagos_determinados_por_periodo(
     *     'DocDate',
     *     'DocTotal',
     *     'U_Movto',
     *     'ohem',
     *     '2024-12-31',
     *     '2024-01-01',
     *     'corporativo',
     *     '1001'
     * );
     * // Resultado:
     * // SELECT pago.ohem_id, SUM(pago.DocTotal) as COBRADO_GENERAL, ohem.nombre_completo AS 'GESTOR'
     * // FROM corporativo.pago AS pago
     * // LEFT JOIN corporativo.ohem AS ohem ON pago.ohem_id = ohem.id
     * // WHERE pago.DocDate BETWEEN '2024-01-01' AND '2024-12-31'
     * //   AND pago.U_Movto = 'Abono'
     * //   AND pago.`status` = 'activo'
     * //   AND pago.plaza_id = 1001
     * // GROUP BY pago.ohem_id;
     *
     * // Ejemplo 2: Campo fecha inválido
     * $query = $this->pagos_determinados_por_periodo(
     *     '',
     *     'DocTotal',
     *     'U_Movto',
     *     'ohem',
     *     '2024-12-31',
     *     '2024-01-01',
     *     'corporativo',
     *     '1001'
     * );
     * // Resultado: ['error' => 'Error $campo_fecha_valida esta vacio', 'data' => '']
     * ```
     */
    final public function pagos_determinados_por_periodo(
        string $campo_fecha_valida, string $campo_pago_total, string $campo_movimiento, string $entidad_empleado,
        string $fecha_fin, string $fecha_inicio,string $name_database, string $plaza_id)
    {

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return $this->error->error('Error entidad empleado esta vacio', $entidad_empleado);
        }

        $campo_pago_total = trim($campo_pago_total);
        if($campo_pago_total === ''){
            return $this->error->error('Error $campo_pago_total esta vacio', $campo_pago_total);
        }
        $name_database = trim($name_database);
        if($name_database === ''){
            return $this->error->error('Error $name_database esta vacio', $name_database);
        }
        $campo_fecha_valida = trim($campo_fecha_valida);
        if($campo_fecha_valida === ''){
            return $this->error->error('Error $campo_fecha_valida esta vacio', $campo_fecha_valida);
        }
        $fecha_inicio = trim($fecha_inicio);
        if($fecha_inicio === ''){
            return $this->error->error('Error $fecha_inicio esta vacio', $fecha_inicio);
        }
        $fecha_fin = trim($fecha_fin);
        if($fecha_fin === ''){
            return $this->error->error('Error $fecha_fin esta vacio', $fecha_fin);
        }
        $campo_movimiento = trim($campo_movimiento);
        if($campo_movimiento === ''){
            return $this->error->error('Error $campo_movimiento esta vacio', $campo_movimiento);
        }
        $plaza_id = trim($plaza_id);
        if($plaza_id === ''){
            return $this->error->error('Error $plaza_id esta vacio', $plaza_id);
        }

        $key_id_empleado = $entidad_empleado.'_id';

        $empleado_id = "pago.$key_id_empleado";
        $total = "SUM(pago.$campo_pago_total) as COBRADO_GENERAL";
        $gestor = "$entidad_empleado.nombre_completo AS 'GESTOR'";

        $tb_empleado = "$name_database.$entidad_empleado AS $entidad_empleado";
        $tb_pago = "$name_database.pago AS pago";
        $on = "pago.$key_id_empleado = $entidad_empleado.id";
        $from = "$tb_pago LEFT JOIN $tb_empleado ON $on";

        $fechas = "'$fecha_inicio' AND '$fecha_fin'";
        $w_fechas = "pago.$campo_fecha_valida BETWEEN $fechas";
        $w_movto = "pago.$campo_movimiento = 'Abono'";
        $w_status = "pago.`status` = 'activo'";
        $w_plaza = "pago.plaza_id = $plaza_id";

        $campos = "$empleado_id, $total, $gestor";
        $where = "$w_fechas AND $w_movto AND $w_status AND $w_plaza";

        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $where GROUP BY $empleado_id";

    }

    /**
     * TRASLADADO
     * Genera los parámetros SQL necesarios para consultas relacionadas con contratos.
     *
     * Esta función valida los campos personalizados y el nombre de la base de datos,
     * luego construye las cláusulas SQL necesarias para las consultas de contratos,
     * incluyendo la cláusula WHERE, los días desde la última visita y los contratos con restos.
     *
     * @param stdClass $campos_custom Un objeto con los campos personalizados necesarios para la consulta.
     * @param string $name_database El nombre de la base de datos donde se encuentran las tablas de contratos.
     * @return stdClass|array Un objeto que contiene las partes de la consulta SQL:
     *                  - where_sql: La cláusula WHERE de la consulta.
     *                  - dias_ultima_visita: La cláusula SQL para los días desde la última visita.
     *                  - restos_sql: La cláusula SQL para los contratos con restos.
     *                  En caso de error, se devuelve un mensaje de error utilizando el objeto de error asociado.
     */
    private function params_sql_contratos(stdClass $campos_custom, string $name_database)
    {
        $valida = (new _valida())->valida_base_campos($campos_custom, $name_database);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }

        $where_sql = $this->where_contrato($campos_custom,$name_database);
        if(error::$en_error){
           return $this->error->error('Error al obtener $where_sql', $where_sql);
        }

        $dias_ultima_visita = $this->dias_ultima_visita($name_database);
        if(error::$en_error){
            return $this->error->error('Error al obtener $dias_ultima_visita', $dias_ultima_visita);
        }

        $restos_sql = $this->contratos_con_restos($name_database);
        if(error::$en_error){
            return $this->error->error('Error al obtener $restos_sql', $restos_sql);
        }

        $data = new stdClass();
        $data->where_sql = $where_sql;
        $data->dias_ultima_visita = $dias_ultima_visita;
        $data->restos_sql = $restos_sql;

        return $data;

    }


    /**
     * EM3
     * Genera un reporte SQL para obtener la meta del gestor concentrado.
     *
     * Esta función construye una consulta SQL basada en los parámetros proporcionados,
     * calculando metas, contratos, y cobros relacionados con los gestores de una organización.
     *
     * @param string $campo_fecha_pago Nombre del campo que contiene la fecha de pago.
     * @param string $fecha_fin Fecha final del rango en formato 'YYYY-MM-DD'.
     * @param string $fecha_inicio Fecha inicial del rango en formato 'YYYY-MM-DD'.
     * @param string $mete_gestor_id_in Cadena de IDs de gestores separados por comas para incluir en el reporte.
     * @param string $name_database Nombre de la base de datos donde se encuentran las tablas relacionadas.
     *
     * @return string|array Devuelve una consulta SQL como cadena. Si ocurre un error, devuelve un array de error.
     *
     * @example Uso exitoso
     * ```php
     * $campo_fecha_pago = 'DocDate';
     * $fecha_inicio = '2024-01-01';
     * $fecha_fin = '2024-12-31';
     * $mete_gestor_id_in = '1,2,3';
     * $name_database = 'database_name';
     *
     * $reporte = $obj->rpt_meta_gestor_concentrado(
     *     $campo_fecha_pago, $fecha_fin, $fecha_inicio, $mete_gestor_id_in, $name_database
     * );
     *
     * echo $reporte;
     * // Resultado: Una consulta SQL para obtener los datos del reporte de meta gestor concentrado.
     * ```
     *
     * @example Error en parámetros
     * ```php
     * $campo_fecha_pago = '';
     * $fecha_inicio = '';
     * $fecha_fin = '';
     * $mete_gestor_id_in = '';
     * $name_database = '';
     *
     * $reporte = $obj->rpt_meta_gestor_concentrado(
     *     $campo_fecha_pago, $fecha_fin, $fecha_inicio, $mete_gestor_id_in, $name_database
     * );
     *
     * print_r($reporte);
     * // Resultado: [
     * //     'error' => 'Error $mete_gestor_id_in esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Posibles salidas
     * ```sql
     * SELECT plaza.descripcion AS PLAZA,
     *        meta_gestor.fecha_inicio AS 'FECHA INICIO',
     *        meta_gestor.fecha_fin AS 'FECHA FIN',
     *        SUM(IFNULL(meta_gestor_concentrado.monto_total,0)) AS META,
     *        SUM(IFNULL(meta_gestor_concentrado.n_contratos,0)) AS 'CONTRATOS CONGELADOS',
     *        COUNT(*) AS 'CARTERA ACTUAL',
     *        COUNT(*) AS 'CONTRATOS NUEVOS',
     *        SUM(IFNULL(meta_gestor_concentrado.meta_proceso,0)) AS 'COBRADO META',
     *        (SELECT SUM(DocTotal)
     *         FROM database_name.pago AS pago
     *         WHERE pago.DocDate BETWEEN '2024-01-01' AND '2024-12-31'
     *           AND pago.U_Movto = 'Abono'
     *           AND pago.`status` = 'activo'
     *           AND pago.plaza_id = plaza.id) AS 'COBRADO TOTAL',
     *        meta_gestor.id AS meta_gestor_id,
     *        plaza.id AS 'PLAZA ID',
     *        SUM(IFNULL(meta_gestor_concentrado.monto_de_mas,0)) AS 'COBRADO META DE MAS',
     *        SUM(IFNULL(meta_gestor_concentrado.monto_nuevo,0)) AS 'COBRADO CONTRATOS NUEVOS',
     *        SUM(IFNULL(meta_gestor_concentrado.monto_contratos_extra,0)) AS 'COBRADO CONTRATOS EXTRA'
     * FROM database_name.meta_gestor_concentrado AS meta_gestor_concentrado
     * LEFT JOIN database_name.meta_gestor AS meta_gestor
     *    ON meta_gestor.id = meta_gestor_concentrado.meta_gestor_id
     * LEFT JOIN database_name.plaza AS plaza
     *    ON plaza.id = meta_gestor.plaza_id
     * WHERE meta_gestor.id IN (1,2,3)
     * GROUP BY meta_gestor.plaza_id
     * ```
     */

    final public function rpt_meta_gestor_concentrado(
        string $campo_fecha_pago, string $fecha_fin, string $fecha_inicio, string $mete_gestor_id_in,
        string $name_database)
    {
        $name_database = trim($name_database);
        $fecha_fin = trim($fecha_fin);
        $fecha_inicio = trim($fecha_inicio);

        $valida = (new _valida())->valida_input_base($fecha_fin,$fecha_inicio,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }


        $mete_gestor_id_in = trim($mete_gestor_id_in);
        if($mete_gestor_id_in === ''){
            return $this->error->error(
                'Error $mete_gestor_id_in esta vacia en database '.$name_database, $mete_gestor_id_in);
        }

        $campo_cancelado = 'Canceled';
        $campo_status_contrato = 'U_StatusCob';
        $value_cancelado = 'N';
        $value_status_contrato = "'ACTIVO', 'PROMESA PAGO'";
        $campo_fecha_valida = "DocDate";

        $campo_monto_pago = "DocTotal";
        $campo_movimiento = "U_Movto";

        $cartera_actual = $this->cartera_actual($campo_cancelado,$campo_status_contrato,$name_database,
            $value_cancelado,$value_status_contrato);
        if(error::$en_error){
            return $this->error->error('Error al obtener $cartera_actual', $cartera_actual);
        }

        $contratos_nuevos = $this->contratos_nuevos($campo_cancelado,$campo_fecha_valida, $campo_status_contrato,
            $fecha_fin,$fecha_inicio,$name_database,$value_cancelado,$value_status_contrato);
        if(error::$en_error){
            return $this->error->error('Error al obtener $contratos_nuevos', $cartera_actual);
        }

        $cobrado_total = $this->cobrado_total($campo_fecha_pago,$campo_monto_pago, $campo_movimiento,$fecha_fin,
            $fecha_inicio,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al obtener $cobrado_total', $cobrado_total);
        }

        $c_plaza = "plaza.descripcion AS PLAZA";
        $c_fecha_i = "meta_gestor.fecha_inicio AS 'FECHA INICIO'";
        $c_fecha_f = "meta_gestor.fecha_fin AS 'FECHA FIN'";
        $c_meta = "SUM( IFNULL(meta_gestor_concentrado.monto_total,0) ) AS META";
        $c_congelados = "SUM( IFNULL(meta_gestor_concentrado.n_contratos,0) ) AS 'CONTRATOS CONGELADOS'";
        $c_cob_meta = "SUM( IFNULL(meta_gestor_concentrado.meta_proceso,0) ) AS 'COBRADO META'";

        $c_meta_gestor_id = "meta_gestor.id AS meta_gestor_id";
        $c_plaza_id = "plaza.id AS 'PLAZA ID'";
        $c_cob_mas = "SUM( IFNULL(meta_gestor_concentrado.monto_de_mas,0) ) AS 'COBRADO META DE MAS'";
        $c_cob_nuevos = "SUM( IFNULL(meta_gestor_concentrado.monto_nuevo,0)) AS 'COBRADO CONTRATOS NUEVOS'";
        $c_cob_extra = "SUM( IFNULL(meta_gestor_concentrado.monto_contratos_extra,0) ) AS 'COBRADO CONTRATOS EXTRA'";

        $c_generales = "$c_plaza, $c_fecha_i, $c_fecha_f,  $c_congelados,  $cartera_actual $contratos_nuevos";
        $c_montos = "$c_meta, $c_cob_meta, $cobrado_total";
        $c_generales_id = "$c_meta_gestor_id, $c_plaza_id";
        $c_cobrados = "$c_cob_mas, $c_cob_nuevos, $c_cob_extra";

        $tb_mgc = "$name_database.meta_gestor_concentrado AS meta_gestor_concentrado";
        $tb_mg = "$name_database.meta_gestor AS meta_gestor";
        $tb_plaza = "$name_database.plaza AS plaza";

        $lf_mgc = "$tb_mg ON meta_gestor.id = meta_gestor_concentrado.meta_gestor_id";
        $lf_plaza = "$tb_plaza ON plaza.id = meta_gestor.plaza_id ";
        $froms = "$tb_mgc LEFT JOIN $lf_mgc LEFT JOIN $lf_plaza";

        $w_mgc_id = "meta_gestor.id IN ( $mete_gestor_id_in )";
        $gb = "meta_gestor.plaza_id";

        $campos = "$c_generales $c_montos $c_generales_id, $c_cobrados";

        return /** @lang MYSQL */ "SELECT $campos FROM $froms WHERE $w_mgc_id GROUP BY $gb";
    }

    final public function rpt_meta_gestor_concentrado_ss(
        string $fecha_fin, string $fecha_inicio, string $mete_gestor_id_in, string $name_database)
    {
        $name_database = trim($name_database);
        $fecha_fin = trim($fecha_fin);
        $fecha_inicio = trim($fecha_inicio);

        $valida = (new _valida())->valida_input_base($fecha_fin,$fecha_inicio,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }
        $mete_gestor_id_in = trim($mete_gestor_id_in);
        if($mete_gestor_id_in === ''){
            return $this->error->error('Error $mete_gestor_id_in esta vacia', $mete_gestor_id_in);
        }

        $campo_cancelado = 'status';
        $campo_status_contrato = 'status_contrato_id';
        $value_cancelado = 'activo';
        $value_status_contrato = "1, 12";
        $campo_fecha_valida = "fecha_valida_informacion";
        $campo_fecha_pago = "fecha_cliente";
        $campo_monto_pago = "monto";
        $campo_movimiento = "movimiento";

        $cartera_actual = $this->cartera_actual($campo_cancelado,$campo_status_contrato,$name_database,
            $value_cancelado,$value_status_contrato);
        if(error::$en_error){
            return $this->error->error('Error al obtener $cartera_actual', $cartera_actual);
        }

        $contratos_nuevos = $this->contratos_nuevos($campo_cancelado,$campo_fecha_valida, $campo_status_contrato,
            $fecha_fin,$fecha_inicio,$name_database,$value_cancelado,$value_status_contrato);
        if(error::$en_error){
            return $this->error->error('Error al obtener $contratos_nuevos', $cartera_actual);
        }

        $cobrado_total = $this->cobrado_total($campo_fecha_pago,$campo_monto_pago, $campo_movimiento,$fecha_fin,$fecha_inicio,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al obtener $cobrado_total', $cobrado_total);
        }

        return "SELECT 
                    plaza.descripcion AS PLAZA,
                    meta_gestor.fecha_inicio AS 'FECHA INICIO',
	                meta_gestor.fecha_fin AS 'FECHA FIN',
	                SUM( IFNULL(meta_gestor_concentrado.n_contratos,0) ) AS 'CONTRATOS CONGELADOS',
	                
	                $cartera_actual
	                $contratos_nuevos
	                        
	                SUM( IFNULL(meta_gestor_concentrado.monto_total,0) ) AS META,
                    SUM( IFNULL(meta_gestor_concentrado.meta_proceso,0) ) AS 'COBRADO META',
                    
                    $cobrado_total
	                      
	                meta_gestor.id AS meta_gestor_id,
                    plaza.id AS 'PLAZA ID',
	                SUM( IFNULL(meta_gestor_concentrado.monto_de_mas,0) ) AS 'COBRADO META DE MAS',
	                SUM( IFNULL(meta_gestor_concentrado.monto_nuevo,0)) AS 'COBRADO CONTRATOS NUEVOS',
	                SUM( IFNULL(meta_gestor_concentrado.monto_contratos_extra,0) ) AS 'COBRADO CONTRATOS EXTRA'
        FROM
	        $name_database.meta_gestor_concentrado AS meta_gestor_concentrado
	        LEFT JOIN $name_database.meta_gestor AS meta_gestor ON meta_gestor.id = meta_gestor_concentrado.meta_gestor_id
	        LEFT JOIN $name_database.plaza AS plaza ON plaza.id = meta_gestor.plaza_id 
        WHERE
	        meta_gestor.id IN ( $mete_gestor_id_in ) 
        GROUP BY
	        meta_gestor.plaza_id";
    }

    /**
     * Genera un reporte SQL de contratos no visitados basado en diversos parámetros y condiciones.
     *
     * Esta función construye una consulta SQL compleja para obtener información sobre contratos no visitados,
     * integrando múltiples condiciones, relaciones entre tablas, y campos personalizados.
     *
     * @param stdClass $campos Objeto con los campos necesarios para la consulta. Debe incluir:
     *                         - `campo_afiliado`, `campo_aportado`, `campo_codigo_gestor`, `campo_cp`,
     *                           `campo_domicilio_cobranza`, `campo_fecha_contrato`, `campo_folio`,
     *                           `campo_gestor`, `campo_importe_pago`, `campo_localidad`, `campo_municipio`,
     *                           `campo_periodicidad_pago`, `campo_precio`, `campo_serie`, `campo_status_contrato`,
     *                           `campo_tipo_morosidad`, `entidad_cp`, `entidad_localidad`, `entidad_municipio`,
     *                           `value_pp_m`, `value_pp_m_rs`, `value_pp_q`, `value_pp_q_rs`, `value_pp_s`, `value_pp_s_rs`.
     * @param string $entidad_base Nombre de la entidad base en la consulta.
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado asociado.
     * @param string $filtro_plaza Condición adicional para filtrar por plaza.
     * @param string $plaza_id ID de la plaza para filtrar resultados.
     * @param string $entidad_cliente (Opcional) Nombre de la entidad que representa al cliente.
     * @param string $entidad_status (Opcional) Nombre de la entidad que representa el estado del contrato.
     * @param string $entidad_tipo_morosidad (Opcional) Nombre de la entidad que representa el tipo de morosidad.
     *
     * @return string|array Retorna la consulta SQL generada como un string. En caso de error, retorna un objeto de error.
     *
     * @example
     * ```php
     * $campos = (object)[
     *     'campo_afiliado' => 'afiliado_id',
     *     'campo_aportado' => 'aportado',
     *     'campo_codigo_gestor' => 'codigo_gestor',
     *     'campo_cp' => 'codigo_postal',
     *     'campo_domicilio_cobranza' => 'domicilio',
     *     'campo_fecha_contrato' => 'fecha_contrato',
     *     'campo_folio' => 'folio',
     *     'campo_gestor' => 'gestor',
     *     'campo_importe_pago' => 'importe_pago',
     *     'campo_localidad' => 'localidad',
     *     'campo_municipio' => 'municipio',
     *     'campo_periodicidad_pago' => 'periodicidad_pago',
     *     'campo_precio' => 'precio',
     *     'campo_serie' => 'serie',
     *     'campo_status_contrato' => 'status_contrato',
     *     'campo_tipo_morosidad' => 'tipo_morosidad',
     *     'entidad_cp' => 'cp',
     *     'entidad_localidad' => 'localidad',
     *     'entidad_municipio' => 'municipio',
     *     'value_pp_m' => 'M',
     *     'value_pp_m_rs' => '31',
     *     'value_pp_q' => 'Q',
     *     'value_pp_q_rs' => '16',
     *     'value_pp_s' => 'S',
     *     'value_pp_s_rs' => '7'
     * ];
     *
     * $sql = $this->rpt_no_visitadas($campos, 'contrato', 'empleado', 'AND plaza_id = 1', '1');
     * echo $sql;
     * ```
     *
     * @throws error Si:
     *         - Faltan campos requeridos en `$campos`.
     *         - `$entidad_base` o `$entidad_empleado` están vacíos.
     *         - Alguna validación de llaves o campos personalizados falla.
     */
    final public function rpt_no_visitadas(stdClass $campos, string $entidad_base, string $entidad_empleado,
                                           string $filtro_plaza, string $plaza_id, string $entidad_cliente = '',
                                           string $entidad_status='', string $entidad_tipo_morosidad = '')
    {

        $entidad_base = trim($entidad_base);
        if($entidad_base === ''){
            return $this->error->error('Error $entidad_base esta vacia', $entidad_base);
        }

        $entidades = $this->entidades_base($entidad_base,$entidad_cliente,$entidad_status,$entidad_tipo_morosidad);
        if(error::$en_error){
            return $this->error->error('Error al obtener $entidades', $entidades);
        }

        $keys = array('campo_afiliado','campo_aportado','campo_codigo_gestor','campo_cp','campo_domicilio_cobranza',
            'campo_fecha_contrato', 'campo_folio','campo_gestor', 'campo_importe_pago', 'campo_localidad',
            'campo_municipio','campo_periodicidad_pago', 'campo_precio', 'campo_serie', 'campo_status_contrato',
            'campo_tipo_morosidad','entidad_cp', 'entidad_localidad', 'entidad_municipio','value_pp_m','value_pp_m_rs',
            'value_pp_q','value_pp_q_rs','value_pp_s','value_pp_s_rs');

        $valida = (new valida())->valida_keys($keys,$campos);
        if(error::$en_error){
            return $this->error->error('Error al validar datos de entrada', $valida);
        }

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return $this->error->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }

        $campo_contrato = $this->campo_contrato($campos->campo_folio,$campos->campo_serie,$entidad_base);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_contrato', $campo_contrato);
        }

        $campo_status_contrato = $this->campo_base($campos->campo_status_contrato,
            $entidades->entidad_status);

        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_status_contrato', $campo_status_contrato);
        }

        $campo_tipo_morosidad = $this->campo_base($campos->campo_tipo_morosidad, $entidades->entidad_tipo_morosidad);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_tipo_morosidad', $campo_tipo_morosidad);
        }

        $campo_afiliado = $this->campo_base($campos->campo_afiliado, $entidades->entidad_cliente);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_afiliado', $campo_afiliado);
        }

        $campo_domicilio_cobranza = $this->campo_base($campos->campo_domicilio_cobranza, $entidad_base);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_domicilio_cobranza', $campo_domicilio_cobranza);
        }

        $campo_localidad = $this->campo_base($campos->campo_localidad,
            $campos->entidad_localidad);

        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_localidad', $campo_localidad);
        }

        $campo_municipio = $this->campo_base($campos->campo_municipio,
            $campos->entidad_municipio);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_municipio', $campo_municipio);
        }

        $campo_cp = $this->campo_base($campos->campo_cp,
            $campos->entidad_cp);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_cp', $campo_cp);
        }

        $campo_precio = $this->campo_base($campos->campo_precio, $entidad_base);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_precio', $campo_precio);
        }

        $campo_aportado = $this->campo_base($campos->campo_aportado, $entidad_base);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_aportado', $campo_aportado);
        }

        $campo_saldo = $this->campo_saldo($campo_aportado, $campo_precio);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_saldo', $campo_saldo);
        }

        $campo_importe_pago = $this->campo_base($campos->campo_importe_pago,
            $entidad_base);

        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_importe_pago', $campo_importe_pago);
        }

        $campo_periodicidad_pago = $this->campo_base($campos->campo_periodicidad_pago, $entidad_base);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_periodicidad_pago', $campo_periodicidad_pago);
        }

        $campo_codigo_gestor = $this->campo_base($campos->campo_codigo_gestor, $entidad_empleado);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_codigo_gestor', $campo_codigo_gestor);
        }

        $campo_gestor = $this->campo_base($campos->campo_gestor, $entidad_empleado);
        if(error::$en_error){
            return $this->error->error('Error al obtener $campo_gestor', $campo_gestor);
        }


        $when_pp_s = $this->when($campo_periodicidad_pago,'=',$campos->value_pp_s,$campos->value_pp_s_rs);
        if(error::$en_error){
            return $this->error->error('Error al obtener $when_pp_s', $when_pp_s);
        }
        $when_pp_q = $this->when($campo_periodicidad_pago,'=',$campos->value_pp_q,$campos->value_pp_q_rs);
        if(error::$en_error){
            return $this->error->error('Error al obtener $when_pp_q', $when_pp_q);
        }
        $when_pp_m = $this->when($campo_periodicidad_pago,'=',$campos->value_pp_m,$campos->value_pp_m_rs);
        if(error::$en_error){
            return $this->error->error('Error al obtener $when_pp_m', $when_pp_m);
        }

        $where_case_s = $this->when_case($campo_periodicidad_pago,'=',$campos->value_pp_s);
        if(error::$en_error){
            return $this->error->error('Error al obtener $where_case_s', $where_case_s);
        }
        $where_case_q = $this->when_case($campo_periodicidad_pago,'=',$campos->value_pp_q);
        if(error::$en_error){
            return $this->error->error('Error al obtener $where_case_q', $where_case_q);
        }
        $where_case_m = $this->when_case($campo_periodicidad_pago,'=',$campos->value_pp_m);
        if(error::$en_error){
            return $this->error->error('Error al obtener $where_case_m', $where_case_m);
        }

        $left_join_status_contrato = $this->left_join_status_contrato($entidad_base,$entidades);
        if(error::$en_error){
            return $this->error->error('Error al obtener $left_join_status_contrato', $left_join_status_contrato);
        }

        $left_join_tipo_morosidad = $this->left_join_tipo_morosidad($entidad_base,$entidades);
        if(error::$en_error){
            return $this->error->error('Error al obtener $left_join_tipo_morosidad', $left_join_tipo_morosidad);
        }

        $left_join_domicilio_cobranza = $this->left_join_domicilio_cobranza($campos,$entidad_base);
        if(error::$en_error){
            return $this->error->error('Error al obtener $left_join_domicilio_cobranza', $left_join_domicilio_cobranza);
        }


        $left_join_municipio_cobranza = $this->left_join_municipio_cobranza($campos);
        if(error::$en_error){
            return $this->error->error('Error al obtener $left_join_municipio_cobranza', $left_join_municipio_cobranza);
        }


        $left_join_cp_cobranza = $this->left_join_cp_cobranza($campos);
        if(error::$en_error){
            return $this->error->error('Error al obtener $left_join_cp_cobranza', $left_join_cp_cobranza);
        }


        $left_join_empleado = $this->left_join_empleado($entidad_base,$entidad_empleado);
        if(error::$en_error){
            return $this->error->error('Error al obtener $left_join_empleado', $left_join_empleado);
        }


        $n_dias_mes_anterior = (new _no_visitadas())->n_dias_mes_anterior();
        if(error::$en_error){
            return $this->error->error('Error al obtener $n_dias_mes_anterior', $n_dias_mes_anterior);
        }

        $n_dias_quincena = (new _no_visitadas())->n_dias_quincena();
        if(error::$en_error){
            return $this->error->error('Error al obtener $n_dias_quincena', $n_dias_quincena);
        }


        return "SELECT * FROM 
        (
        SELECT
        $entidad_base.id AS 'CONTRATO_ID',
        $campo_contrato AS 'CONTRATO',
        plaza.id AS 'plaza_id',
        plaza.descripcion AS 'PLAZA',
        $entidad_base.$campos->campo_fecha_contrato AS 'FECHA CONTRATO',
        $campo_status_contrato AS 'STATUS DEL CONTRATO',
        $campo_tipo_morosidad AS 'MOROSIDAD',
        $campo_afiliado AS 'AFILIADO',
        $campo_domicilio_cobranza AS 'DOMICILIO COBRANZA',
        $campo_localidad AS 'LOCALIDAD',
		$campo_municipio AS 'MUNICIPIO',
		$campo_cp AS 'CP',
        $entidad_base.telefono AS 'TELEFONO',
        $campo_precio AS 'PRECIO',
        $campo_aportado AS 'APORTADO',
        $campo_saldo AS 'SALDO',
        $campo_importe_pago AS 'IMPORTE PAGO',
        CASE
        $when_pp_s
        $when_pp_q
        $when_pp_m
        END AS 'PERIODICIDAD',
        $entidad_base.fecha_ultima_visita AS 'FECHA ULT MOV CORTADO',
        IFNULL($entidad_base.fecha_ultima_visita_cliente,$entidad_base.fecha_ultima_visita) AS 'FECHA ULT MOV CLIENTE',
        @dias := DATEDIFF(CURDATE(),IFNULL($entidad_base.fecha_ultima_visita_cliente,$entidad_base.fecha_ultima_visita)) AS 'DIAS TRANS ULT MOV CLIENTE',
        $campo_codigo_gestor AS 'CODIGO GESTOR',
        $campo_gestor AS 'GESTOR', 
        CASE
        WHEN $where_case_s AND @dias BETWEEN 0 AND 7 THEN
        IF(@dias < 5, 'SI', 'POR VENCER')
        WHEN $where_case_s AND @dias > 7 THEN 'NO'
        WHEN $where_case_q AND @dias BETWEEN 0 AND 16 THEN
        IF(@dias < 13, 'SI', 'POR VENCER')
        WHEN $where_case_q AND @dias > 16 THEN 'NO'
        WHEN $where_case_m AND @dias BETWEEN 0 AND 31 THEN 
        IF(@dias < 28, 'SI', 'POR VENCER')
        ELSE 'NO'
        END AS 'VISITADA',
        restos.x AS 'RESTOS'
        FROM $entidad_base
        LEFT JOIN cliente ON cliente.id = $entidad_base.cliente_id
        $left_join_empleado
        LEFT JOIN plaza ON plaza.id = contrato.plaza_id
        LEFT JOIN (SELECT contrato_id, 'CON RESTOS' AS x FROM contrato_comision 
            INNER JOIN $entidad_base ON $entidad_base.id = contrato_comision.contrato_id 
            WHERE contrato_comision.monto_resto > 0 AND comision_pagada = 'inactivo' AND 
            $entidad_base.plaza_id = $plaza_id GROUP BY contrato_id) AS restos ON restos.contrato_id = $entidad_base.id
                          
        $left_join_domicilio_cobranza
        $left_join_municipio_cobranza
        $left_join_cp_cobranza    
        $left_join_status_contrato
        $left_join_tipo_morosidad
                          
        WHERE
        $entidad_base.$campos->campo_fecha_contrato IS NOT NULL AND
        $entidad_base.$campos->campo_serie IS NOT NULL AND
        $entidad_base.$campos->campo_serie != '' AND
        $campo_aportado < $campo_precio AND
        $entidad_base.$campos->campo_folio IS NOT NULL AND
        $campo_status_contrato IN ('ACTIVO','SUSPENSION TEMPORAL','OBSERVACION','POR FIRMAR','PRE-CANCELADO','PROMESA PAGO')
        $filtro_plaza
        ) AS TABLA
        WHERE TABLA.VISITADA != 'SI'";

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener estadísticas de contratos no visitados.
     *
     * Esta función toma las consultas SQL de contratos activos y contratos no visitados, y genera una consulta
     * para obtener la cantidad de contratos no visitados, así como aquellos con y sin restos, agrupados por plaza.
     *
     * @param string $contratos_activos_sql La consulta SQL que obtiene los contratos activos.
     * @param string $contratos_no_visit La consulta SQL que obtiene los contratos no visitados.
     * @param string $name_database El nombre de la base de datos.
     * @return string|array La cadena SQL completa para la consulta si todo es válido,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function sql_no_visit(string $contratos_activos_sql, string $contratos_no_visit, string $name_database)
    {
        $contratos_activos_sql = trim($contratos_activos_sql);
        if($contratos_activos_sql === ''){
            return $this->error->error('Error $contratos_activos_sql esta vacio', $contratos_activos_sql);
        }
        $contratos_no_visit = trim($contratos_no_visit);
        if($contratos_no_visit === ''){
            return $this->error->error('Error $contratos_no_visit esta vacio', $contratos_no_visit);
        }
        $name_database = trim($name_database);
        if($name_database === ''){
            return $this->error->error('Error $name_database esta vacio', $name_database);
        }
        $plaza = "$name_database.plaza.descripcion AS plaza";
        $plaza_id = "TABLA.PLAZA_ID";
        $no_visitado = "COUNT(*) AS no_visitado";
        $con_restos = "SUM(CASE WHEN TABLA.RESTOS > 0 THEN 1 ELSE 0 END) AS con_restos";
        $sin_restos = "SUM(CASE WHEN TABLA.RESTOS = 0 THEN 1 ELSE 0 END) AS sin_restos";
        $join = "$name_database.plaza ON $name_database.plaza.id = TABLA.PLAZA_ID";
        $where = "WHERE TABLA.VISITADA != 'SI'";
        $group_by = "GROUP BY TABLA.PLAZA_ID";

        $campos = "SELECT $plaza_id, $plaza, $contratos_activos_sql, $no_visitado, $con_restos, $sin_restos";
        $from = "FROM ($contratos_no_visit) AS TABLA INNER JOIN $join";

        return "$campos $from $where $group_by";

    }

    private function sql_no_visit_em3(string $name_database)
    {
        $params = (new _rows())->params_base_em3();
        if(error::$en_error){
            return $this->error->error('Error al obtener $params', $params);
        }

        $sql = $this->genera_sql_no_visit($params->campo_condicion_pago,$params->campos_custom,$name_database,
            $params->periodicidades);
        if(error::$en_error){
            return $this->error->error('Error al obtener $sql', $sql);
        }
        return $sql;

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener datos relacionados con registros no visitados en SAP.
     *
     * Este método construye una consulta SQL utilizando parámetros específicos para SAP,
     * como campos personalizados, periodicidades, y el nombre de la base de datos proporcionado.
     *
     * @param string $name_database Nombre de la base de datos. No debe estar vacío.
     *
     * @return string|array Devuelve la consulta SQL generada como cadena. En caso de error,
     *                      retorna un arreglo con información detallada del fallo.
     *
     * @throws error Lanza un error si:
     *         - `$name_database` está vacío.
     *         - Falla al obtener los parámetros base para SAP desde `_rows::params_base_sap`.
     *         - Falla al generar la consulta SQL.
     *
     * @example
     * // Ejemplo de uso:
     * $name_database = 'SAP_DATABASE';
     * $sql_query = $this->sql_no_visit_sap($name_database);
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al generar la consulta SQL.";
     * } else {
     *     echo $sql_query; // Muestra la consulta SQL generada.
     * }
     *
     * @example Salida esperada:
     * ```sql
     * SELECT campo1, campo2, ...
     * FROM SAP_DATABASE.tabla
     * WHERE condiciones = valor;
     * ```
     */
    private function sql_no_visit_sap(string $name_database)
    {
        // Elimina espacios en blanco y valida que el nombre de la base de datos no esté vacío
        $name_database = trim($name_database);
        if ($name_database === '') {
            return $this->error->error('Error $name_database esta vacio', $name_database);
        }

        // Obtiene los parámetros base necesarios para SAP
        $params = (new _rows())->params_base_sap();
        if (error::$en_error) {
            return $this->error->error('Error al obtener $params', $params);
        }

        // Genera la consulta SQL basada en los parámetros obtenidos
        $sql = $this->genera_sql_no_visit(
            $params->campo_condicion_pago,
            $params->campos_custom,
            $name_database,
            $params->periodicidades
        );

        if (error::$en_error) {
            return $this->error->error('Error al obtener $sql', $sql);
        }

        // Devuelve la consulta SQL generada
        return $sql;
    }


    /**
     * TRASLADADO
     * Genera una cláusula WHEN SQL basada en condiciones de periodicidad y un campo de condición de pago.
     *
     * Esta función toma el nombre de la base de datos, un campo de condición de pago y un array de periodicidades,
     * y construye una cláusula WHEN SQL utilizando estas entradas. Valida que los parámetros no estén vacíos
     * y genera las partes correspondientes de la declaración SQL.
     *
     * @param string $campo_condicion_pago El nombre del campo de condición de pago.
     * @param string $name_database El nombre de la base de datos.
     * @param array $periodicidades Un array de arrays asociativos, cada uno con las claves 'key' y 'val' que representan la condición y el valor a evaluar.
     * @return string|array La cadena SQL de la cláusula WHEN generada si todo es válido,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function sql_when_periodicidad(string $campo_condicion_pago, string $name_database, array $periodicidades)
    {
        $name_database = trim($name_database);
        if($name_database === ''){
            return $this->error->error('Error $name_database esta vacia', $name_database);
        }
        $campo_condicion_pago = trim($campo_condicion_pago);
        if($campo_condicion_pago === ''){
            return $this->error->error('Error $campo_condicion_pago esta vacia', $campo_condicion_pago);
        }
        if(count($periodicidades) === 0){
            return $this->error->error('Error $periodicidades esta vacia', $periodicidades);
        }

        $when_ccp = $this->genera_campo_contrato($campo_condicion_pago,$name_database);
        if(error::$en_error){
            return $this->error->error('Error al generar campo contrato', $when_ccp);
        }

        $w_per = $this->genera_when_periodicidad($periodicidades,$when_ccp);
        if(error::$en_error){
            return $this->error->error('Error al generar when_periodicidad', $w_per);
        }

        return $w_per;


    }

    /**
     * EM3
     * Genera un valor de estado predeterminado o personalizado.
     *
     * Esta función establece un valor de estado predeterminado (`'N'`) si no se proporciona
     * un valor personalizado. Si se pasa un valor, este sobrescribe el predeterminado.
     *
     * @param string $value_status_def Un valor de estado personalizado. Si está vacío, se utiliza `'N'`.
     *
     * @return string Devuelve el valor de estado a utilizar.
     *
     * @example
     * ```php
     * // Ejemplo 1: Usar valor predeterminado
     * $status = $this->status_def('');
     * // Resultado: 'N'
     *
     * // Ejemplo 2: Usar valor personalizado
     * $status = $this->status_def('ACTIVO');
     * // Resultado: 'ACTIVO'
     *
     * // Ejemplo 3: Usar un valor vacío explícitamente
     * $status = $this->status_def('');
     * // Resultado: 'N'
     * ```
     */
    private function status_def(string $value_status_def): string
    {
        $status_def = 'N';
        if ($value_status_def !== '') {
            $status_def = $value_status_def;
        }

        return $status_def;
    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener información de contratos, incluyendo condición de pago, última visita,
     * y si tiene restos, basada en las periodicidades especificadas.
     *
     * Esta función construye una consulta SQL que selecciona varios campos de la tabla de contratos en una base de datos,
     * incluyendo información sobre la condición de pago, la última visita y si tiene restos. Valida que los parámetros
     * no estén vacíos y genera las partes correspondientes de la declaración SQL.
     *
     * @param string $campo_condicion_pago El nombre del campo de condición de pago.
     * @param string $dias_ultima_visita La expresión SQL que calcula los días desde la última visita.
     * @param string $name_database El nombre de la base de datos.
     * @param array $periodicidades Un array de arrays asociativos, cada uno con las claves 'key' y 'val' que representan la condición y el valor a evaluar.
     * @param string $restos_sql La subconsulta SQL para determinar si el contrato tiene restos.
     * @param string $where_sql La cláusula WHERE SQL para filtrar los contratos.
     * @return string|array La cadena SQL completa para la consulta si todo es válido,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function tabla_contratos(string $campo_condicion_pago, string $dias_ultima_visita,
                                          string $name_database, array $periodicidades, string $restos_sql,
                                          string $where_sql)
    {
        $campo_condicion_pago = trim($campo_condicion_pago);
        if($campo_condicion_pago === ''){
            return $this->error->error('Error al $campo_condicion_pago esta vacio', $campo_condicion_pago);
        }

        $dias_ultima_visita = trim($dias_ultima_visita);
        if($dias_ultima_visita === ''){
            return $this->error->error('Error al $dias_ultima_visita esta vacio', $dias_ultima_visita);
        }

        $name_database = trim($name_database);
        if($name_database === ''){
            return $this->error->error('Error al $name_database esta vacio', $name_database);
        }

        $restos_sql = trim($restos_sql);
        if($restos_sql === ''){
            return $this->error->error('Error al $restos_sql esta vacio', $restos_sql);
        }

        $where_sql = trim($where_sql);
        if($where_sql === ''){
            return $this->error->error('Error al $where_sql esta vacio', $where_sql);
        }

        if(count($periodicidades) === 0){
            return $this->error->error('Error $periodicidades esta vacia', $periodicidades);
        }

        $w_per = $this->sql_when_periodicidad($campo_condicion_pago,$name_database,$periodicidades);
        if(error::$en_error){
            return $this->error->error('Error al generar when_periodicidad', $w_per);
        }


        $plaza_id = "$name_database.contrato.plaza_id AS 'PLAZA_ID',";
        $case = "CASE $w_per END AS 'VISITADA',";
        $restos = "IFNULL(restos.x,0) AS 'RESTOS'";
        $campos = "$plaza_id $dias_ultima_visita, $case $restos";
        $from = "FROM $name_database.contrato";
        $join = "LEFT JOIN ($restos_sql) AS restos ON restos.contrato_id = $name_database.contrato.id";

        return "SELECT $campos $from $join WHERE $where_sql";

    }

    private function union_no_visit(array $databases)
    {
        $sql_rs = '';
        foreach ($databases as $database){
            $union = '';
            if($sql_rs !== ''){
                $union = ' UNION ALL ';
            }

            $sql_rs = "$sql_rs $union ".$database['sql'];

        }

        return $sql_rs;

    }

    /**
     * EM3
     * Genera una lista de valores de estado predeterminados o personalizados.
     *
     * Esta función permite establecer una lista de valores de estado. Si no se proporciona
     * una lista personalizada, se utiliza un valor predeterminado: `'ACTIVO', 'PROMESA PAGO'`.
     *
     * @param string $status_values Una cadena con los valores de estado personalizados, separados por comas.
     *                              Si está vacía, se utiliza el valor predeterminado.
     *
     * @return string Devuelve la lista de valores de estado a utilizar.
     *
     * @example
     * ```php
     * // Ejemplo 1: Usar valores predeterminados
     * $valores = $this->values_status('');
     * // Resultado: "'ACTIVO', 'PROMESA PAGO'"
     *
     * // Ejemplo 2: Usar valores personalizados
     * $valores = $this->values_status("'PENDIENTE', 'EN PROCESO'");
     * // Resultado: "'PENDIENTE', 'EN PROCESO'"
     *
     * // Ejemplo 3: Usar valores parcialmente personalizados
     * $valores = $this->values_status("'ACTIVO', 'CANCELADO'");
     * // Resultado: "'ACTIVO', 'CANCELADO'"
     * ```
     */
    private function values_status(string $status_values): string
    {
        $values_status = "'ACTIVO', 'PROMESA PAGO'";
        if ($status_values !== '') {
            $values_status = $status_values;
        }

        return $values_status;
    }


    /**
     * EM3
     * Genera una condición SQL para una sentencia `CASE` o `WHEN`.
     *
     * Esta función valida los parámetros de entrada y construye una expresión SQL que compara un campo
     * con un valor dado utilizando un operador específico. Es útil para generar dinámicamente
     * condiciones en sentencias SQL, como en `CASE` o `WHEN`.
     *
     * @param string $campo El nombre del campo a comparar.
     * @param string $operador El operador lógico o de comparación (por ejemplo, '=', '<', '>').
     * @param string $value El valor contra el cual se realizará la comparación.
     *
     * @return string|array Devuelve una cadena SQL que representa la condición
     *                      `$campo $operador '$value'`.
     *                      Si alguno de los parámetros está vacío, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Condición válida
     * $condicion = $this->when_case('contrato.status', '=', 'activo');
     * // Resultado: "contrato.status = 'activo'"
     *
     * // Ejemplo 2: Campo vacío
     * $condicion = $this->when_case('', '=', 'activo');
     * // Resultado: ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Operador vacío
     * $condicion = $this->when_case('contrato.status', '', 'activo');
     * // Resultado: ['error' => 'Error $operador esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Valor vacío
     * $condicion = $this->when_case('contrato.status', '=', '');
     * // Resultado: ['error' => 'Error $value esta vacio', 'data' => '']
     * ```
     */
    private function when_case(string $campo, string $operador, string $value)
    {
        // Valida que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return $this->error->error('Error $campo esta vacio', $campo);
        }

        // Valida que el operador no esté vacío
        $operador = trim($operador);
        if ($operador === '') {
            return $this->error->error('Error $operador esta vacio', $operador);
        }

        // Valida que el valor no esté vacío
        $value = trim($value);
        if ($value === '') {
            return $this->error->error('Error $value esta vacio', $value);
        }

        // Retorna la expresión SQL generada
        return "$campo $operador '$value'";
    }


    /**
     * TRASLADADO
     * Genera una cláusula SQL `WHEN` para una instrucción `CASE`.
     *
     * Este método crea una cláusula SQL `WHEN` para un `CASE`, validando los parámetros proporcionados
     * y generando una instrucción que compara un campo con un valor específico y devuelve un resultado.
     *
     * @param string $campo Nombre del campo que será evaluado.
     * @param string $operador Operador lógico para la comparación (por ejemplo, `=`, `!=`, `>`, `<`).
     * @param string $value_compare Valor con el que se comparará el campo.
     * @param string $value_result Valor que se devolverá si la comparación es verdadera.
     *
     * @return string|array Devuelve una instrucción `WHEN` en formato SQL.
     *                      En caso de error, devuelve un arreglo con detalles del fallo.
     *
     * @throws error Lanza un error si:
     *         - `$campo`, `$operador`, `$value_compare`, o `$value_result` están vacíos.
     *         - Ocurre un problema al generar el caso (`when_case`).
     *
     * @example
     * // Ejemplo de uso:
     * $when_clause = $this->when('status', '=', 'active', 'Activo');
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al generar cláusula WHEN.";
     * } else {
     *     echo $when_clause; // Devuelve: WHEN status = 'active' THEN 'Activo'
     * }
     *
     * @example Salida esperada:
     * ```sql
     * WHEN status = 'active' THEN 'Activo'
     * ```
     */
    private function when(string $campo, string $operador, string $value_compare, string $value_result)
    {
        // Validación: El campo no puede estar vacío
        $campo = trim($campo);
        if ($campo === '') {
            return $this->error->error('Error $campo está vacío', $campo);
        }

        // Validación: El operador no puede estar vacío
        $operador = trim($operador);
        if ($operador === '') {
            return $this->error->error('Error $operador está vacío', $operador);
        }

        // Validación: El valor a comparar no puede estar vacío
        $value_compare = trim($value_compare);
        if ($value_compare === '') {
            return $this->error->error('Error $value_compare está vacío', $value_compare);
        }

        // Validación: El valor de resultado no puede estar vacío
        $value_result = trim($value_result);
        if ($value_result === '') {
            return $this->error->error('Error $value_result está vacío', $value_result);
        }

        // Genera el caso de comparación usando el método auxiliar `when_case`
        $campo_case = $this->when_case($campo, $operador, $value_compare);
        if (error::$en_error) {
            return $this->error->error('Error al generar CASE', $campo_case);
        }

        // Devuelve la cláusula `WHEN` generada
        return "WHEN $campo_case THEN '$value_result' ";
    }


    /**
     * TRASLADADO
     * Genera una cadena SQL para la cláusula WHEN de una declaración CASE basada en la periodicidad proporcionada.
     *
     * Esta función toma un array asociativo que contiene las claves 'key' y 'val', y construye una parte de una
     * declaración CASE SQL para evaluar condiciones específicas. Valida que las claves del array sean correctas
     * y que los parámetros no estén vacíos antes de construir la cláusula WHEN.
     *
     * @param array $periodicidad Un array asociativo con las claves 'key' y 'val' que representan la condición y el valor a evaluar.
     * @param string $w_per La cláusula WHERE parcial que se va a concatenar con la cláusula WHEN generada.
     * @param string $when_ccp La condición que se va a evaluar en la cláusula WHEN.
     * @return string|array La cadena SQL de la cláusula WHERE actualizada con la cláusula WHEN generada si todo es válido,
     *                         de lo contrario, un objeto de error con el mensaje correspondiente.
     */
    private function when_periodicidad(array $periodicidad, string $w_per, string $when_ccp)
    {
        $keys = array('key','val');
        $valida = (new valida())->valida_keys($keys,$periodicidad);
        if(error::$en_error){
            return $this->error->error('Error al validar periodicidad', $valida);
        }
        $when_ccp = trim($when_ccp);
        if($when_ccp === ''){
            return $this->error->error('Error $when_ccp esta vacio', $when_ccp);
        }
        $w_per = trim($w_per);

        $key = $periodicidad['key'];
        $val = $periodicidad['val'];
        $w_per .= " WHEN $when_ccp = '$key' AND @dias > $val THEN 'NO' ";

        return $w_per;
    }

    /**
     * TRASLADADO
     * Genera una cláusula WHERE para la tabla contrato en una base de datos específica.
     *
     * Esta función valida los campos personalizados y el nombre de la base de datos.
     * Luego, inicializa los campos personalizados y genera una cláusula WHERE basada en varios criterios
     * para filtrar los contratos activos.
     * Si se encuentran errores durante la validación o inicialización, se devuelve un array con información del error.
     *
     * @param stdClass $campos_custom Un objeto que contiene los campos personalizados a validar e inicializar.
     * @param string $name_database El nombre de la base de datos.
     * @return string|array Una cadena SQL que representa la cláusula WHERE para filtrar contratos activos.
     *                      En caso de error, se devuelve un array con información del error.
     */
    private function where_contrato(stdClass $campos_custom, string $name_database)
    {
        $valida = (new _valida())->valida_base_campos($campos_custom, $name_database);
        if(error::$en_error){
            return $this->error->error('Error al validar entrada de datos', $valida);
        }

        $campos = $this->init_campos($campos_custom);
        if(error::$en_error){
            return $this->error->error('Error al inicializar campos', $valida);
        }

        $sql = "$name_database.contrato.$campos->fecha IS NOT NULL AND ";
        $sql .= " $name_database.contrato.$campos->serie IS NOT NULL AND ";
        $sql .= " $name_database.contrato.$campos->serie != '' AND ";
        $sql .= " $name_database.contrato.$campos->monto_p < $name_database.contrato.$campos->monto_t AND ";
        $sql .= " $name_database.contrato.$campos->folio IS NOT NULL AND ";
        $sql .= " $name_database.contrato.$campos->estatus = '$campos->estatus_val' ";

        return $sql;

    }


}
