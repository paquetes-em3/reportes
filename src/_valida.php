<?php
namespace desarrollo_em3\reportes;

use desarrollo_em3\error\error;

use desarrollo_em3\error\valida;
use stdClass;

/**
 * Clase `_valida` para la validación de parámetros y estructuras personalizadas en operaciones relacionadas con reportes.
 *
 * Esta clase proporciona métodos para validar:
 * - Estructuras personalizadas de campos (`stdClass`).
 * - Parámetros como nombres de base de datos, fechas, y claves necesarias en operaciones.
 * - Entradas específicas como listas de periodicidades y condiciones de pago.
 *
 * ### Propósito
 * La clase centraliza y estandariza la validación de datos, asegurando la integridad y consistencia de los parámetros
 * antes de ejecutar operaciones relacionadas con reportes y consultas en el sistema EM3.
 *
 * ### Uso
 * Utilice esta clase para realizar validaciones específicas que impliquen estructuras personalizadas,
 * nombres de base de datos y valores de entrada en operaciones complejas.
 * Los métodos están diseñados para devolver detalles claros de errores en caso de fallas.
 *
 * ### Ejemplo de uso básico
 * ```php
 * use desarrollo_em3\reportes\_valida;
 * use stdClass;
 *
 * $validador = new _valida();
 *
 * // Validar campos personalizados
 * $campos_custom = new stdClass();
 * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => 'ACTIVO'];
 * $campos_custom->campo_fecha_valida = (object) ['name' => 'fecha_valida'];
 *
 * $resultado = $validador->valida_campos_name($campos_custom);
 * if ($resultado !== true) {
 *     echo "Error en la validación: " . print_r($resultado, true);
 * }
 * ```
 *
 * @package desarrollo_em3\reportes
 * @version 1.0.0
 * @since 2025-01-01
 */
class _valida
{
    /**
     * Propiedad de clase para manejar errores.
     *
     * Esta propiedad almacena una instancia de la clase `error`, utilizada para manejar y devolver
     * información detallada sobre errores que ocurren durante las validaciones.
     *
     * @var error
     */
    private error $error;

    /**
     * Constructor de la clase `_valida`.
     *
     * Inicializa la propiedad `$error` como una nueva instancia de la clase `error`.
     *
     * @example Uso del constructor
     * ```php
     * $validador = new _valida();
     * ```
     */
    public function __construct()
    {
        $this->error = new error();

    }

    /**
     * EM3
     * Valida la existencia y formato del nombre de un campo en un objeto `stdClass`.
     *
     * Esta función verifica si un campo dentro de un objeto `$campos_custom` cumple con las siguientes condiciones:
     * - El `$key_name` no debe estar vacío.
     * - `$campos_custom->$key_name` debe ser un objeto `stdClass`.
     * - `$campos_custom->$key_name->name` debe existir y no estar vacío.
     *
     * Si alguna de estas condiciones falla, devuelve un error. De lo contrario, devuelve `true`.
     *
     * @param stdClass $campos_custom Objeto que contiene la estructura personalizada de campos.
     * @param string $key_name Llave dentro del objeto `$campos_custom` que debe ser validada.
     *
     * @return bool|array Devuelve `true` si la validación es exitosa. Si ocurre un error, devuelve un array con el detalle.
     *
     * @example Uso exitoso
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_prueba = new stdClass();
     * $campos_custom->campo_prueba->name = 'nombre_campo';
     *
     * $resultado = $obj->campo_name($campos_custom, 'campo_prueba');
     * // Resultado: true
     * ```
     *
     * @example Error: $key_name vacío
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_prueba = new stdClass();
     * $campos_custom->campo_prueba->name = 'nombre_campo';
     *
     * $resultado = $obj->campo_name($campos_custom, '');
     * // Resultado:
     * // [
     * //     'error' => 'Error $key_name esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: El campo no es un objeto `stdClass`
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_prueba = 'valor_incorrecto';
     *
     * $resultado = $obj->campo_name($campos_custom, 'campo_prueba');
     * // Resultado:
     * // [
     * //     'error' => 'Error en campos_custom->campo_prueba debe ser un stdclass',
     * //     'data' => $campos_custom
     * // ]
     * ```
     *
     * @example Error: El campo `name` no existe o está vacío
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_prueba = new stdClass();
     *
     * $resultado = $obj->campo_name($campos_custom, 'campo_prueba');
     * // Resultado:
     * // [
     * //     'error' => 'Error en campos_custom->campo_prueba.name no existe',
     * //     'data' => $campos_custom
     * // ]
     *
     * $campos_custom->campo_prueba->name = '';
     * $resultado = $obj->campo_name($campos_custom, 'campo_prueba');
     * // Resultado:
     * // [
     * //     'error' => 'Error campos_custom->campo_prueba.name esta vacio',
     * //     'data' => $campos_custom
     * // ]
     * ```
     */
    private function campo_name(stdClass $campos_custom, string $key_name)
    {
        $key_name = trim($key_name);
        if ($key_name === '') {
            return $this->error->error('Error $key_name esta vacio', $key_name);
        }
        if (!is_object($campos_custom->$key_name)) {
            return $this->error->error("Error en campos_custom->$key_name debe ser un stdclass", $campos_custom);
        }
        if (!isset($campos_custom->$key_name->name)) {
            return $this->error->error("Error en campos_custom->$key_name.name no existe", $campos_custom);
        }
        if (trim($campos_custom->$key_name->name) === '') {
            return $this->error->error("Error campos_custom->$key_name.name esta vacio", $campos_custom);
        }

        return true;
    }


    /**
     * EM3
     * Valida un conjunto de claves en un objeto `stdClass` y verifica los nombres de los campos correspondientes.
     *
     * Esta función realiza dos validaciones:
     * 1. Verifica que el objeto `$campos_custom` contenga todas las claves necesarias especificadas en `$keys`.
     * 2. Para cada clave, valida que el valor asociado sea un objeto `stdClass` que tenga un atributo `name` no vacío.
     *
     * @param stdClass $campos_custom Objeto que contiene las estructuras de los campos personalizados.
     *
     * @return bool|array Devuelve `true` si todas las validaciones son exitosas. Si ocurre un error, devuelve un array con los detalles.
     *
     * @example Validación exitosa
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus'];
     * $campos_custom->campo_fecha_valida = (object) ['name' => 'fecha_valida'];
     * $campos_custom->campo_serie = (object) ['name' => 'serie'];
     * $campos_custom->campo_monto_pagado = (object) ['name' => 'monto_pagado'];
     * $campos_custom->campo_monto_total = (object) ['name' => 'monto_total'];
     * $campos_custom->campo_folio = (object) ['name' => 'folio'];
     *
     * $resultado = $obj->campos_name($campos_custom);
     * // Resultado: true
     * ```
     *
     * @example Error: Falta una clave en `$campos_custom`
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus'];
     *
     * $resultado = $obj->campos_name($campos_custom);
     * // Resultado:
     * // [
     * //     'error' => 'Error al validar campos_custom',
     * //     'data' => [...] // Detalle del error con claves faltantes
     * // ]
     * ```
     *
     * @example Error: Falta el atributo `name` o está vacío
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus'];
     * $campos_custom->campo_fecha_valida = (object) [];
     *
     * $resultado = $obj->campos_name($campos_custom);
     * // Resultado:
     * // [
     * //     'error' => 'Error al validar $campos_custom->campo_fecha_valida',
     * //     'data' => [...] // Detalle del error
     * // ]
     * ```
     */
    private function campos_name(stdClass $campos_custom)
    {
        // Claves esperadas en el objeto
        $keys = array('campo_estatus', 'campo_fecha_valida', 'campo_serie', 'campo_monto_pagado', 'campo_monto_total', 'campo_folio');

        // Validar que todas las claves existan en $campos_custom
        $valida = (new valida())->valida_keys($keys, $campos_custom);
        if (error::$en_error) {
            return $this->error->error('Error al validar campos_custom', $valida);
        }

        // Validar que cada clave tenga un atributo `name` válido
        foreach ($keys as $key) {
            $valida = $this->campo_name($campos_custom, $key);
            if (error::$en_error) {
                return $this->error->error('Error al validar $campos_custom->' . $key, $valida);
            }
        }

        // Validación exitosa
        return true;
    }


    /**
     * EM3
     * Valida los parámetros de entrada base para operaciones con fechas y base de datos.
     *
     * Esta función asegura que los parámetros proporcionados no estén vacíos y realiza
     * una validación básica de los valores de `$fecha_fin`, `$fecha_inicio` y `$name_database`.
     *
     * @param string $fecha_fin La fecha de fin a validar.
     * @param string $fecha_inicio La fecha de inicio a validar.
     * @param string $name_database El nombre de la base de datos a validar.
     *
     * @return bool|array Devuelve `true` si todos los parámetros son válidos. Si alguno de los
     *                    parámetros está vacío, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $resultado = $this->valida_input_base('2025-01-01', '2024-12-01', 'mi_base_datos');
     * // Resultado: true
     *
     * // Ejemplo 2: `$name_database` vacío
     * $resultado = $this->valida_input_base('2025-01-01', '2024-12-01', '');
     * // Resultado:
     * // ['error' => 'Error $name_database esta vacia', 'data' => '']
     *
     * // Ejemplo 3: `$fecha_fin` vacío
     * $resultado = $this->valida_input_base('', '2024-12-01', 'mi_base_datos');
     * // Resultado:
     * // ['error' => 'Error $fecha_fin esta vacia', 'data' => '']
     *
     * // Ejemplo 4: `$fecha_inicio` vacío
     * $resultado = $this->valida_input_base('2025-01-01', '', 'mi_base_datos');
     * // Resultado:
     * // ['error' => 'Error $fecha_inicio esta vacia', 'data' => '']
     * ```
     */
    final public function valida_input_base(string $fecha_fin, string $fecha_inicio, string $name_database)
    {
        $name_database = trim($name_database);
        if ($name_database === '') {
            return $this->error->error('Error $name_database esta vacia', $name_database);
        }
        $fecha_fin = trim($fecha_fin);
        if ($fecha_fin === '') {
            return $this->error->error('Error $fecha_fin esta vacia', $fecha_fin);
        }
        $fecha_inicio = trim($fecha_inicio);
        if ($fecha_inicio === '') {
            return $this->error->error('Error $fecha_inicio esta vacia', $fecha_inicio);
        }

        return true;
    }


    /**
     * EM3
     * Valida los campos base personalizados y la base de datos.
     *
     * Esta función realiza las siguientes validaciones:
     * 1. Verifica que el nombre de la base de datos no esté vacío.
     * 2. Valida los nombres y valores de los campos personalizados en `$campos_custom` mediante la función `valida_campos_name`.
     *
     * @param stdClass $campos_custom Objeto que contiene los campos personalizados a validar.
     * @param string $name_database El nombre de la base de datos a validar.
     *
     * @return bool|array Devuelve `true` si todas las validaciones son exitosas.
     *                    Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Validación exitosa
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => 'ACTIVO'];
     * $campos_custom->campo_fecha_valida = (object) ['name' => 'fecha_valida'];
     * $campos_custom->campo_serie = (object) ['name' => 'serie'];
     * $campos_custom->campo_monto_pagado = (object) ['name' => 'monto_pagado'];
     * $campos_custom->campo_monto_total = (object) ['name' => 'monto_total'];
     * $campos_custom->campo_folio = (object) ['name' => 'folio'];
     *
     * $name_database = 'mi_base_datos';
     * $resultado = $obj->valida_base_campos($campos_custom, $name_database);
     * // Resultado: true
     * ```
     *
     * @example Error: Nombre de base de datos vacío
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => 'ACTIVO'];
     *
     * $name_database = '';
     * $resultado = $obj->valida_base_campos($campos_custom, $name_database);
     * // Resultado:
     * // [
     * //     'error' => 'Error $name_database esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Validación de campos personalizados falla
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => ''];
     *
     * $name_database = 'mi_base_datos';
     * $resultado = $obj->valida_base_campos($campos_custom, $name_database);
     * // Resultado:
     * // [
     * //     'error' => 'Error al validar $campos_custom',
     * //     'data' => [...]
     * // ]
     * ```
     */
    final public function valida_base_campos(stdClass $campos_custom, string $name_database)
    {
        // Validar que el nombre de la base de datos no esté vacío
        $name_database = trim($name_database);
        if ($name_database === '') {
            return $this->error->error('Error $name_database esta vacio', $name_database);
        }

        // Validar los nombres y valores de los campos personalizados
        $valida = $this->valida_campos_name($campos_custom);
        if (error::$en_error) {
            return $this->error->error('Error al validar $campos_custom', $valida);
        }

        // Validación exitosa
        return true;
    }


    /**
     * EM3
     * Valida los nombres y valores de los campos personalizados.
     *
     * Esta función verifica que:
     * 1. El objeto `$campos_custom` cumpla con las validaciones de estructura definidas en la función `campos_name`.
     * 2. El campo `campo_estatus->value` exista dentro de `$campos_custom` y tenga un valor no vacío.
     *
     * @param stdClass $campos_custom Objeto que contiene los campos personalizados a validar.
     *
     * @return bool|array Devuelve `true` si todas las validaciones son exitosas.
     *                    Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Validación exitosa
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => 'ACTIVO'];
     * $campos_custom->campo_fecha_valida = (object) ['name' => 'fecha_valida'];
     * $campos_custom->campo_serie = (object) ['name' => 'serie'];
     * $campos_custom->campo_monto_pagado = (object) ['name' => 'monto_pagado'];
     * $campos_custom->campo_monto_total = (object) ['name' => 'monto_total'];
     * $campos_custom->campo_folio = (object) ['name' => 'folio'];
     *
     * $resultado = $obj->valida_campos_name($campos_custom);
     * // Resultado: true
     * ```
     *
     * @example Error: Falta `campo_estatus->value`
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus'];
     * $campos_custom->campo_fecha_valida = (object) ['name' => 'fecha_valida'];
     *
     * $resultado = $obj->valida_campos_name($campos_custom);
     * // Resultado:
     * // [
     * //     'error' => 'Error $campos_custom->campo_estatus->value no existe',
     * //     'data' => $campos_custom
     * // ]
     * ```
     *
     * @example Error: `campo_estatus->value` está vacío
     * ```php
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => ''];
     *
     * $resultado = $obj->valida_campos_name($campos_custom);
     * // Resultado:
     * // [
     * //     'error' => 'Error $campos_custom->campo_estatus->value esta vacio',
     * //     'data' => $campos_custom
     * // ]
     * ```
     */
    final public function valida_campos_name(stdClass $campos_custom)
    {
        // Validar la estructura de campos personalizados
        $valida = $this->campos_name($campos_custom);
        if (error::$en_error) {
            return $this->error->error('Error al validar $campos_custom', $valida);
        }

        // Validar la existencia de `campo_estatus->value`
        if (!isset($campos_custom->campo_estatus->value)) {
            return $this->error->error('Error $campos_custom->campo_estatus->value no existe', $campos_custom);
        }

        // Validar que `campo_estatus->value` no esté vacío
        if (trim($campos_custom->campo_estatus->value) === '') {
            return $this->error->error('Error $campos_custom->campo_estatus->value esta vacio', $campos_custom);
        }

        // Validación exitosa
        return true;
    }


    /**
     * EM3
     * Valida las entradas necesarias para operaciones de datos relacionadas con campos personalizados y periodicidades.
     *
     * Esta función valida:
     * 1. Que los campos personalizados y la base de datos sean correctos utilizando la función `valida_base_campos`.
     * 2. Que el campo de condición de pago no esté vacío.
     * 3. Que el array de periodicidades no esté vacío.
     *
     * @param string $campo_condicion_pago Nombre del campo que representa la condición de pago.
     * @param stdClass $campos_custom Objeto que contiene los campos personalizados.
     * @param string $name_database Nombre de la base de datos.
     * @param array $periodicidades Lista de periodicidades a validar.
     *
     * @return bool|array Devuelve `true` si todas las validaciones son exitosas.
     *                    Si alguna validación falla, devuelve un array con los detalles del error.
     *
     * @example Validación exitosa
     * ```php
     * $campo_condicion_pago = 'condicion_pago';
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => 'ACTIVO'];
     * $campos_custom->campo_fecha_valida = (object) ['name' => 'fecha_valida'];
     * $name_database = 'mi_base_datos';
     * $periodicidades = ['SEMANAL', 'MENSUAL'];
     *
     * $resultado = $obj->valida_no_visit($campo_condicion_pago, $campos_custom, $name_database, $periodicidades);
     * // Resultado: true
     * ```
     *
     * @example Error: Campo de condición de pago vacío
     * ```php
     * $campo_condicion_pago = '';
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => 'ACTIVO'];
     * $name_database = 'mi_base_datos';
     * $periodicidades = ['SEMANAL', 'MENSUAL'];
     *
     * $resultado = $obj->valida_no_visit($campo_condicion_pago, $campos_custom, $name_database, $periodicidades);
     * // Resultado:
     * // [
     * //     'error' => 'Error al $campo_condicion_pago esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Lista de periodicidades vacía
     * ```php
     * $campo_condicion_pago = 'condicion_pago';
     * $campos_custom = new stdClass();
     * $campos_custom->campo_estatus = (object) ['name' => 'estatus', 'value' => 'ACTIVO'];
     * $name_database = 'mi_base_datos';
     * $periodicidades = [];
     *
     * $resultado = $obj->valida_no_visit($campo_condicion_pago, $campos_custom, $name_database, $periodicidades);
     * // Resultado:
     * // [
     * //     'error' => 'Error $periodicidades esta vacia',
     * //     'data' => []
     * // ]
     * ```
     */
    final public function valida_no_visit(
        string $campo_condicion_pago,
        stdClass $campos_custom,
        string $name_database,
        array $periodicidades
    ) {
        // Validar campos base
        $valida = (new _valida())->valida_base_campos($campos_custom, $name_database);
        if (error::$en_error) {
            return $this->error->error('Error al validar entrada de datos', $valida);
        }

        // Validar que el campo de condición de pago no esté vacío
        $campo_condicion_pago = trim($campo_condicion_pago);
        if ($campo_condicion_pago === '') {
            return $this->error->error('Error al $campo_condicion_pago esta vacio', $campo_condicion_pago);
        }

        // Validar que la lista de periodicidades no esté vacía
        if (count($periodicidades) === 0) {
            return $this->error->error('Error $periodicidades esta vacia', $periodicidades);
        }

        // Validaciones exitosas
        return true;
    }



}
