<?php
namespace desarrollo_em3\reportes;
use desarrollo_em3\error\error;
use NumberFormatter;

class _format_value
{
    private error $error;

    public function __construct()
    {
        $this->error = new error();

    }

    /**
     * FIN
     * Convierte un valor a entero formateado.
     *
     * Esta función utiliza `init_value` para inicializar y limpiar el valor proporcionado.
     * Si ocurre un error durante la inicialización, retorna un error. De lo contrario,
     * retorna el valor formateado como un número entero.
     *
     * @param mixed $value El valor a convertir a entero.
     * @return string|array El valor formateado como número entero.
     */
    final public function entero($value)
    {
        $value = $this->init_value($value);
        if(error::$en_error){
            return $this->error->error('Error al inicializa value',$value);
        }

        return number_format($value);

    }

    /**
     * FIN
     * Inicializa y limpia un valor proporcionado.
     *
     * Esta función verifica si el valor proporcionado es nulo y lo establece en 0 si es necesario.
     * Luego elimina los caracteres no deseados como comas, símbolos de dólar, espacios y porcentajes.
     * Si el valor resultante está vacío, lo establece en 0.
     *
     * @param string $value El valor a inicializar y limpiar.
     * @return string|array El valor inicializado y limpiado como cadena.
     */
    private function init_value($value)
    {
        if (is_null($value)) {
            $value = 0;
        }
        $value = trim($value);
        $value = str_replace(',', '', $value);
        $value = str_replace('$', '', $value);
        $value = str_replace(' ', '', $value);
        $value = str_replace('%', '', $value);
        $value = trim($value);
        if($value === ''){
            $value = 0;
        }

        if(!is_numeric($value)){
            return $this->error->error('Error al inicializa value numero es texto',$value);
        }

        return trim($value);

    }

    /**
     * FIN
     * Formatea un valor como moneda.
     *
     * Esta función utiliza `init_value` para inicializar y limpiar el valor proporcionado.
     * Si ocurre un error durante la inicialización, retorna un error. De lo contrario,
     * retorna el valor formateado como moneda utilizando la configuración regional "es_MX".
     *
     * @param string $value El valor a formatear como moneda.
     * @return string|array El valor formateado como moneda.
     */
    final public function moneda($value)
    {
        $value = $this->init_value($value);
        if(error::$en_error){
            return $this->error->error('Error al inicializa value',$value);
        }

        $format = new NumberFormatter("es_MX", NumberFormatter::CURRENCY);
        return $format->format($value);

    }

    /**
     * FIN
     * Formatea un valor como porcentaje.
     *
     * Esta función utiliza `init_value` para inicializar y limpiar el valor proporcionado.
     * Si ocurre un error durante la inicialización, retorna un error. De lo contrario,
     * divide el valor entre 100 y lo formatea como porcentaje utilizando la configuración regional "es_MX".
     *
     * @param mixed $value El valor a formatear como porcentaje.
     * @return string|array El valor formateado como porcentaje.
     */
    final public function porcentaje($value)
    {
        $value = $this->init_value($value);
        if(error::$en_error){
            return $this->error->error('Error al inicializa value',$value);
        }

        $value = $value / 100;
        $format = new NumberFormatter("es_MX", NumberFormatter::IGNORE, "#,##0.00%");
        return $format->format($value);

    }

    /**
     * FIN
     * Convierte los valores especificados en el array $row a enteros si es posible.
     *
     * Itera sobre los keys proporcionados en $keys y convierte los valores correspondientes
     * en $row a enteros utilizando el método `entero`. Si algún key está vacío o el valor
     * no está definido en $row, se asigna 0. Retorna un array con los valores convertidos
     * o un objeto de error si ocurre algún problema durante el proceso.
     *
     * @param array $keys Un array de keys que deben existir en $row.
     * @param array $row El array de datos donde se buscarán los valores correspondientes a los keys.
     * @return array Retorna un array con los valores convertidos a enteros si todos los keys son válidos,
     *                        o un objeto de error con un mensaje descriptivo si ocurre algún error durante el proceso.
     */
    final public function values_enteros(array $keys, array $row): array
    {
        foreach ($keys as $key){
            $key = trim($key);
            if($key === ''){
                return $this->error->error('Error $key esta vacio',$keys);
            }
            if(!isset($row[$key])){
                $row[$key] = 0;
            }
            $row[$key] = $this->entero($row[$key]);
            if(error::$en_error){
                return $this->error->error('Error al integrar valor',$row);
            }
        }
        return $row;


    }

    final public function values_numeric(array $keys, array $row): array
    {
        foreach ($keys as $key){
            $key = trim($key);
            if($key === ''){
                return $this->error->error('Error $key esta vacio',$keys);
            }
            if(!isset($row[$key])){
                $row[$key] = 0;
            }
            $row[$key] = $this->init_value($row[$key]);
            if(error::$en_error){
                return $this->error->error('Error al integrar valor',$row);
            }
            $row[$key] = trim($row[$key]);
        }
        return $row;


    }


}