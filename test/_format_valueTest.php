<?php
namespace desarrollo_em3\test;
use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\reportes\_format_value;
use desarrollo_em3\reportes\_valida;
use desarrollo_em3\reportes\sql;
use PHPUnit\Framework\TestCase;
use stdClass;

class _format_valueTest extends TestCase
{
    final public function test_entero()
    {
        error::$en_error = false;
        $format = new _format_value();

        $value = '';
        $result = $format->entero($value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('0',$result);


        error::$en_error = false;

        $value = '$$100.999%%%';
        $result = $format->entero($value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('101',$result);


    }

    final public function test_init_value()
    {
        error::$en_error = false;
        $format = new _format_value();
        $format = new liberator($format);

        $value = '';
        $result = $format->init_value($value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('0',$result);


        error::$en_error = false;

        $value = '$$100.999%%%';
        $result = $format->init_value($value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('100.999',$result);
        error::$en_error = false;

    }

    final public function test_moneda()
    {
        error::$en_error = false;
        $format = new _format_value();

        $value = '';
        $result = $format->moneda($value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('$0.00',$result);


        error::$en_error = false;

        $value = '$$100.999%%%';
        $result = $format->moneda($value);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('$101.00',$result);
        error::$en_error = false;

    }

    final public function test_porcentaje()
    {
        error::$en_error = false;
        $format = new _format_value();

        $value = null;
        $result = $format->porcentaje($value);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('0.00%',$result);


        error::$en_error = false;

        $value = '$$10,,0.999%%%';
        $result = $format->porcentaje($value);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('101.00%',$result);
        error::$en_error = false;

    }

    final public function test_values_enteros()
    {
        error::$en_error = false;
        $format = new _format_value();

        $keys = array('a');
        $row = array();
        $result = $format->values_enteros($keys,$row);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('0',$result['a']);

        error::$en_error = false;


    }

    final public function test_values_numeric()
    {
        error::$en_error = false;
        $format = new _format_value();

        $keys = array('a');
        $row = array('a'=>'111,111,111');
        $result = $format->values_numeric($keys,$row);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('111111111',$result['a']);

        error::$en_error = false;


    }



}
