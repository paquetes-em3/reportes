<?php
namespace desarrollo_em3\test;
use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\reportes\_format_value;
use desarrollo_em3\reportes\_rows;
use desarrollo_em3\reportes\_valida;
use desarrollo_em3\reportes\sql;
use PHPUnit\Framework\TestCase;
use stdClass;

class _rowsTest extends TestCase
{


    final public function test_format_enteros()
    {
        error::$en_error = false;
        $obj = new _rows();
        $obj = new liberator($obj);


        $row = array('x'=>'1.25');
        $init_key_inexistente = false;
        $indice = 0;
        $rows = array();
        $keys_enteros = array();
        $keys_enteros[] = 'x';

        $result = $obj->format_enteros($indice,$init_key_inexistente,$keys_enteros,$row,$rows);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(1,$result[0]['x']);


        error::$en_error = false;
        $row = array();
        $init_key_inexistente = true;
        $indice = 0;
        $rows = array();
        $keys_enteros = array();
        $keys_enteros[] = 'x';

        $result = $obj->format_enteros($indice,$init_key_inexistente,$keys_enteros,$row,$rows);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(0,$result[0]['x']);
        error::$en_error = false;
    }
    final public function test_format_numbers()
    {
        error::$en_error = false;
        $obj = new _rows();

        $indice = -1;
        $keys_enteros = array();
        $keys_monedas = array();
        $keys_porcentajes = array();
        $row = array();
        $rows = array();
        $result = $obj->format_numbers($indice, false,$keys_enteros,$keys_monedas,$keys_porcentajes,$row,$rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEmpty($result);

        error::$en_error = false;

        $indice = 1;
        $keys_enteros = array();
        $keys_monedas = array();
        $keys_porcentajes = array();
        $row = array();
        $rows = array();

        $keys_enteros[] = 'int';
        $keys_monedas[] = 'moneda';
        $keys_porcentajes[] = 'porc';
        $row['int'] = '-1';
        $row['moneda'] = '-1';
        $row['porc'] = '-1';

        $result = $obj->format_numbers($indice, false,$keys_enteros,$keys_monedas,$keys_porcentajes,$row,$rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('-1',$result[1]['int']);
        $this->assertEquals('-$1.00',$result[1]['moneda']);
        $this->assertEquals('-1.00%',$result[1]['porc']);

        error::$en_error = false;

        $indice = 1;
        $keys_enteros = array();
        $keys_monedas = array();
        $keys_porcentajes = array();
        $row = array();
        $rows = array();

        $keys_enteros[] = 'int';
        $keys_monedas[] = 'moneda';
        $keys_porcentajes[] = 'porc';

        $result = $obj->format_numbers($indice, true,$keys_enteros,$keys_monedas,$keys_porcentajes,$row,$rows);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('0',$result[1]['int']);
        $this->assertEquals('$0.00',$result[1]['moneda']);
        $this->assertEquals('0.00%',$result[1]['porc']);

        error::$en_error = false;
    }

    final public function test_inicializa_row_value()
    {
        error::$en_error = false;
        $obj = new _rows();
        $obj = new liberator($obj);


        $row = array();
        $init_key_inexistente = false;
        $key = 'a';
        $row['a'] = '';

        $result = $obj->inicializa_row_value($init_key_inexistente,$key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('',$result['a']);


        error::$en_error = false;
        $row = array();
        $init_key_inexistente = true;
        $key = 'a';


        $result = $obj->inicializa_row_value($init_key_inexistente,$key,$row);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(0,$result['a']);

        error::$en_error = false;
    }

    final public function test_init_row_key()
    {
        error::$en_error = false;
        $obj = new _rows();
        $obj = new liberator($obj);


        $row = array();
        $init_key_inexistente = true;
        $key = 'a';

        $result = $obj->init_row_key($init_key_inexistente,$key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(0,$result['a']);


        error::$en_error = false;
        $row = array();
        $init_key_inexistente = false;
        $key = 'a';

        $result = $obj->init_row_key($init_key_inexistente,$key,$row);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
        error::$en_error = false;
    }

    final public function test_params_base_sap()
    {
        error::$en_error = false;
        $obj = new _rows();
        //$obj = new liberator($obj);

        $result = $obj->params_base_sap();


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('U_StatusCob',$result->campos_custom->campo_estatus->name);
        $this->assertEquals('ACTIVO',$result->campos_custom->campo_estatus->value);
        $this->assertEquals('DocDate',$result->campos_custom->campo_fecha_valida->name);

        error::$en_error = false;
    }

    final public function test_integra_row_entero()
    {
        error::$en_error = false;
        $obj = new _rows();
        $obj = new liberator($obj);


        $row = array();
        $init_key_inexistente = false;
        $key = 'x';
        $indice = 0;
        $rows = array();
        $row['x'] = '';

        $result = $obj->integra_row_entero($indice,$init_key_inexistente,$key,$row,$rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(0,$result[0]['x']);


        error::$en_error = false;
        $row = array();
        $init_key_inexistente = true;
        $key = 'x';
        $indice = 0;
        $rows = array();


        $result = $obj->integra_row_entero($indice,$init_key_inexistente,$key,$row,$rows);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(0,$result[0]['x']);

        error::$en_error = false;
    }

    final public function test_periodicidades_em3()
    {
        error::$en_error = false;
        $obj = new _rows();
        $obj = new liberator($obj);

        $result = $obj->periodicidades_em3();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(16,$result['q']['val']);
        /**
         * RESULTADO DEFINIDO COMO MES ANTERIOR
         */
        //$this->assertEquals(31,$result['m']['val']);
        $this->assertEquals(31,$result['m']['val']);


        error::$en_error = false;
    }




}
