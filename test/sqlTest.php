<?php
namespace desarrollo_em3\test;
use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\reportes\_no_visitadas;
use desarrollo_em3\reportes\sql;
use PHPUnit\Framework\TestCase;
use stdClass;

class sqlTest extends TestCase
{

    final public function test_asigna_key_campo()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campos = new stdClass();
        $key_campo_asignar = '$key_campo_asignar';
        $name_base = '$name_base';
        $result = $sql->asigna_key_campo($campos,$key_campo_asignar,$name_base);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('$name_base',$result);


        error::$en_error = false;

        $campos = new stdClass();
        $key_campo_asignar = '$key_campo_asignar';
        $name_base = '$name_base';
        $campos->$key_campo_asignar = 'x';
        $result = $sql->asigna_key_campo($campos,$key_campo_asignar,$name_base);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('x',$result);

        error::$en_error = false;

    }
    final public function test_campo_base()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo = '';
        $entidad = '';
        $result = $sql->campo_base($campo,$entidad);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $campo esta vacio',$result['mensaje_limpio']);


        error::$en_error = false;

        $campo = 'a';
        $entidad = '';
        $result = $sql->campo_base($campo,$entidad);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $entidad esta vacia',$result['mensaje_limpio']);

        error::$en_error = false;

        $campo = 'a';
        $entidad = 'b';
        $result = $sql->campo_base($campo,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('b.a',$result);
        error::$en_error = false;

    }

    final public function test_campo_contrato()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo_folio = 'b';
        $campo_serie = 'a';
        $entidad_base = 'c';
        $result = $sql->campo_contrato($campo_folio,$campo_serie,$entidad_base);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('CONCAT( c.a, c.b )',$result);

        error::$en_error = false;


    }

    final public function test_campo_saldo()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo_aportado = 'a';
        $campo_precio = 'c';
        $result = $sql->campo_saldo($campo_aportado,$campo_precio);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('c - a',$result);

        error::$en_error = false;




    }

    final public function test_campos_data_contrato()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo_aportado = 'a';
        $campo_precio = 'c';
        $result = $sql->campos_data_contrato();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('DocDate',$result['campo_fecha_valida']);
        $this->assertEquals('U_StatusCob',$result['campo_status_contrato']);
        $this->assertEquals('Canceled',$result['campo_status_contrato_def']);

        error::$en_error = false;




    }

    final public function test_cartera_actual()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo_cancelado = '$campo_cancelado';
        $campo_status_contrato = '$campo_status_contrato';
        $name_database = '$name_database';
        $value_cancelado = '$value_cancelado';
        $values_status_contrato = '$values_status_contrato';
        $result = $sql->cartera_actual($campo_cancelado,$campo_status_contrato,$name_database,$value_cancelado,
            $values_status_contrato);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsStringIgnoringCase('(SELECT COUNT(*)',$result);
        $this->assertStringContainsStringIgnoringCase('FROM $name_database.contrato AS contrato',$result);
        $this->assertStringContainsStringIgnoringCase('WHERE contrato.$campo_status_contrato IN ( $values_status_contrato )',$result);
        $this->assertStringContainsStringIgnoringCase("AND contrato.$campo_cancelado = '$value_cancelado'",$result);
        $this->assertStringContainsStringIgnoringCase(" AND contrato.plaza_id = meta_gestor.plaza_id) AS 'CARTERA ACTUAL',",$result);

        error::$en_error = false;

        $campo_cancelado = 'esta_cancelado';
        $campo_status_contrato = 'estado_del_contrato';
        $name_database = 'database';
        $value_cancelado = 'Nel';
        $values_status_contrato = '1,2,3';
        $result = $sql->cartera_actual($campo_cancelado,$campo_status_contrato,$name_database,$value_cancelado,
            $values_status_contrato);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsStringIgnoringCase('(SELECT COUNT(*)',$result);
        $this->assertStringContainsStringIgnoringCase('FROM database.contrato AS contrato',$result);
        $this->assertStringContainsStringIgnoringCase('WHERE contrato.estado_del_contrato IN ( 1,2,3 )',$result);
        $this->assertStringContainsStringIgnoringCase(" AND contrato.esta_cancelado = 'Nel'",$result);
        $this->assertStringContainsStringIgnoringCase("AND contrato.plaza_id = meta_gestor.plaza_id) AS 'CARTERA ACTUAL',",$result);

        error::$en_error = false;


    }

    final public function test_clientes_nuevos_by_vendedor()
    {
        error::$en_error = false;
        $sql = new sql();
        //$sql = new liberator($sql);
        $fecha_inicio = '$fecha_inicio';
        $fecha_fin = '$fecha_fin';
        $name_database = '$name_database';
        $entidad_empleado = '$entidad_empleado';
        $result = $sql->clientes_nuevos_by_vendedor(new stdClass(),$entidad_empleado, $fecha_fin,$fecha_inicio,
            '', $name_database,1,'','' );

        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsStringIgnoringCase('SELECT $entidad_empleado.id AS $entidad_empleado_id,',$result);
        $this->assertStringContainsStringIgnoringCase("COUNT(*) AS 'n_contratos',",$result);
        $this->assertStringContainsStringIgnoringCase("SUM(IF(contrato.DocDate BETWEEN '$fecha_inicio' AND '$fecha_fin',1,0)) AS 'n_contratos_nuevos'",$result);
        $this->assertStringContainsStringIgnoringCase("FROM $name_database.contrato AS contrato",$result);
        $this->assertStringContainsStringIgnoringCase("LEFT JOIN $name_database.cliente AS cliente ON contrato.cliente_id = cliente.id",$result);
        $this->assertStringContainsStringIgnoringCase("WHERE",$result);
        $this->assertStringContainsStringIgnoringCase("contrato.U_StatusCob IN ('ACTIVO', 'PROMESA PAGO')",$result);
        $this->assertStringContainsStringIgnoringCase("AND contrato.Canceled = 'N'",$result);
        $this->assertStringContainsStringIgnoringCase('LEFT JOIN $name_database.$entidad_empleado AS $entidad_empleado',$result);
        $this->assertStringContainsStringIgnoringCase('LEFT JOIN $name_database.$entidad_empleado AS $entidad_empleado',$result);
        $this->assertStringContainsStringIgnoringCase('ON cliente.$entidad_empleado_id = $entidad_empleado.id',$result);

        error::$en_error = false;

        $fecha_inicio = '$fecha_inicio';
        $fecha_fin = '$fecha_fin';
        $name_database = '$name_database';
        $entidad_empleado = '$entidad_empleado';
        $key_empleado_gestor_id = '$key_empleado_gestor_id';
        $result = $sql->clientes_nuevos_by_vendedor(new stdClass(),$entidad_empleado, $fecha_fin,$fecha_inicio,
            $key_empleado_gestor_id, $name_database,1,'','' );
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsStringIgnoringCase('SELECT $entidad_empleado.id AS $entidad_empleado_id',$result);
        $this->assertStringContainsStringIgnoringCase("COUNT(*) AS 'n_contratos',",$result);
        $this->assertStringContainsStringIgnoringCase("SUM(IF(contrato.DocDate BETWEEN '$fecha_inicio' AND '$fecha_fin',1,0)) AS 'n_contratos_nuevos'",$result);
        $this->assertStringContainsStringIgnoringCase("FROM $name_database.contrato AS contrato",$result);
        $this->assertStringContainsStringIgnoringCase("LEFT JOIN $name_database.cliente AS cliente ON contrato.cliente_id = cliente.id",$result);
        $this->assertStringContainsStringIgnoringCase("WHERE",$result);
        $this->assertStringContainsStringIgnoringCase("contrato.U_StatusCob IN ('ACTIVO', 'PROMESA PAGO')",$result);
        $this->assertStringContainsStringIgnoringCase("AND contrato.Canceled = 'N'",$result);
        $this->assertStringContainsStringIgnoringCase('LEFT JOIN $name_database.$entidad_empleado AS $entidad_empleado',$result);
        $this->assertStringContainsStringIgnoringCase('LEFT JOIN $name_database.$entidad_empleado AS $entidad_empleado',$result);
        $this->assertStringContainsStringIgnoringCase('ON contrato.$key_empleado_gestor_id = $entidad_empleado.id',$result);


        error::$en_error = false;



        $fecha_inicio = '$fecha_inicio';
        $fecha_fin = '$fecha_fin';
        $name_database = '$name_database';
        $entidad_empleado = '$entidad_empleado';
        $key_empleado_gestor_id = '$key_empleado_gestor_id';
        $status_values = '$status_values';
        $result = $sql->clientes_nuevos_by_vendedor(new stdClass(),$entidad_empleado, $fecha_fin,$fecha_inicio,
            $key_empleado_gestor_id, $name_database,1,$status_values,'' );


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsStringIgnoringCase('SELECT $entidad_empleado.id AS $entidad_empleado_id',$result);
        $this->assertStringContainsStringIgnoringCase("COUNT(*) AS 'n_contratos',",$result);
        $this->assertStringContainsStringIgnoringCase("SUM(IF(contrato.DocDate BETWEEN '$fecha_inicio' AND '$fecha_fin',1,0)) AS 'n_contratos_nuevos'",$result);
        $this->assertStringContainsStringIgnoringCase("FROM $name_database.contrato AS contrato",$result);
        $this->assertStringContainsStringIgnoringCase("LEFT JOIN $name_database.cliente AS cliente ON contrato.cliente_id = cliente.id",$result);
        $this->assertStringContainsStringIgnoringCase("WHERE",$result);
        $this->assertStringContainsStringIgnoringCase("AND contrato.Canceled = 'N'",$result);
        $this->assertStringContainsStringIgnoringCase('LEFT JOIN $name_database.$entidad_empleado AS $entidad_empleado',$result);
        $this->assertStringContainsStringIgnoringCase('LEFT JOIN $name_database.$entidad_empleado AS $entidad_empleado',$result);
        $this->assertStringContainsStringIgnoringCase('ON contrato.$key_empleado_gestor_id = $entidad_empleado.id',$result);
        $this->assertStringContainsStringIgnoringCase('GROUP BY $entidad_empleado.id',$result);
        $this->assertStringContainsStringIgnoringCase('contrato.U_StatusCob IN ($status_values)',$result);



        error::$en_error = false;

    }

    final public function test_cobrado_total()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $fecha_inicio = 'a';
        $fecha_fin = 'b';
        $name_database = 'c';
        $campo_fecha_pago = 'd';
        $campo_monto_pago = 'e';
        $campo_movimiento = 'f';
        $result = $sql->cobrado_total($campo_fecha_pago,$campo_monto_pago,$campo_movimiento,$fecha_fin,$fecha_inicio,
            $name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("(SELECT SUM(pago.e) FROM c.pago AS pago WHERE pago.d BETWEEN 'a' AND 'b' AND pago.f = 'Abono' AND pago.`status` = 'activo' AND pago.plaza_id = plaza.id) AS 'COBRADO TOTAL',",$result);

        error::$en_error = false;

    }

    final public function test_contratos_activos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $name_database = 'xxx';


        $campos_custom = (new _no_visitadas())->campo_base_sap();

        $result = $sql->contratos_activos($name_database, $campos_custom);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("(SELECT COUNT(*) FROM xxx.contrato  WHERE xxx.contrato.U_StatusCob = 'ACTIVO'  AND xxx.contrato.plaza_id = TABLA.PLAZA_ID AND  xxx.contrato.DocDate IS NOT NULL AND  xxx.contrato.U_SeCont IS NOT NULL AND  xxx.contrato.U_SeCont != '' AND  xxx.contrato.PaidToDate < xxx.contrato.DocTotal AND  xxx.contrato.U_FolioCont IS NOT NULL) AS contratos_activos",$result);

        error::$en_error = false;


    }

    final public function test_contratos_con_restos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $name_database = 'ff';

        $result = $sql->contratos_con_restos($name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT ff.contrato_comision.contrato_id, 1 AS x FROM ff.contrato_comision INNER JOIN ff.contrato ON ff.contrato.id = ff.contrato_comision.contrato_id WHERE ff.contrato_comision.monto_resto > 0 AND ff.contrato_comision.comision_pagada = 'inactivo' GROUP BY ff.contrato_comision.contrato_id",$result);

        error::$en_error = false;


    }

    final public function test_contratos_nuevos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);


        $campo_cancelado = 'cc';
        $campo_fecha_valida = 'cfv';
        $campo_status_contrato = 'csc';
        $fecha_fin = 'ff';
        $fecha_inicio = 'fi';
        $name_database = 'nd';
        $value_cancelado = 'vc';
        $value_stauts_contrato = 'vsc';

        $result = $sql->contratos_nuevos($campo_cancelado,$campo_fecha_valida,$campo_status_contrato,$fecha_fin,
            $fecha_inicio,$name_database,$value_cancelado,$value_stauts_contrato);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("(SELECT COUNT(*) FROM nd.contrato AS contrato WHERE contrato.cfv BETWEEN 'fi' AND 'ff' AND contrato.csc IN ( vsc ) AND contrato.cc = 'vc' AND meta_gestor.plaza_id = contrato.plaza_id ) AS 'CONTRATOS NUEVOS',",$result);

        error::$en_error = false;


    }

    final public function test_dias_ultima_visita()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $name_database = 'ds';

        $result = $sql->dias_ultima_visita($name_database);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("@dias := DATEDIFF(CURDATE(),IFNULL(ds.contrato.fecha_ultima_visita_cliente,ds.contrato.fecha_ultima_visita)) AS 'DIAS TRANS ULT MOV CLIENTE'",$result);

        error::$en_error = false;


    }

    final public function test_entidades_base()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $entidad_cliente = '';
        $entidad_base = 'entidad_base';
        $entidad_status = '';
        $entidad_tipo_morosidad = '';
        $result = $sql->entidades_base($entidad_base,$entidad_cliente,$entidad_status,$entidad_tipo_morosidad);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('entidad_base',$result->entidad_status);
        $this->assertEquals('entidad_base',$result->entidad_tipo_morosidad);
        $this->assertEquals('entidad_base',$result->entidad_cliente);

        error::$en_error = false;

        $entidad_cliente = 'entidad_cliente';
        $entidad_base = 'entidad_base';
        $entidad_status = '';
        $entidad_tipo_morosidad = '';
        $result = $sql->entidades_base($entidad_base,$entidad_cliente,$entidad_status,$entidad_tipo_morosidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('entidad_base',$result->entidad_status);
        $this->assertEquals('entidad_base',$result->entidad_tipo_morosidad);
        $this->assertEquals('entidad_cliente',$result->entidad_cliente);

        error::$en_error = false;



        $entidad_cliente = 'entidad_cliente';
        $entidad_base = 'entidad_base';
        $entidad_status = 'entidad_status';
        $entidad_tipo_morosidad = '';
        $result = $sql->entidades_base($entidad_base,$entidad_cliente,$entidad_status,$entidad_tipo_morosidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('entidad_status',$result->entidad_status);
        $this->assertEquals('entidad_base',$result->entidad_tipo_morosidad);
        $this->assertEquals('entidad_cliente',$result->entidad_cliente);

        error::$en_error = false;


    }

    final public function test_entidad_cliente()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $entidad_cliente = '';
        $entidad_base = 'x';
        $result = $sql->entidad_cliente($entidad_base,$entidad_cliente);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('x',$result);

        error::$en_error = false;

        $entidad_cliente = 'd';
        $entidad_base = 'x';
        $result = $sql->entidad_cliente($entidad_base,$entidad_cliente);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('d',$result);
        error::$en_error = false;


    }

    final public function test_entidad_status()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $entidad_base = 'a';
        $entidad_status = '';
        $result = $sql->entidad_status($entidad_base,$entidad_status);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a',$result);


        error::$en_error = false;

        $entidad_base = 'a';
        $entidad_status = 'z';
        $result = $sql->entidad_status($entidad_base,$entidad_status);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('z',$result);
        error::$en_error = false;

    }

    final public function test_entidad_tipo_morosidad()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $entidad_base = 'a';
        $entidad_tipo_morosidad = '';
        $result = $sql->entidad_tipo_morosidad($entidad_base,$entidad_tipo_morosidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a',$result);


        error::$en_error = false;

        $entidad_base = 'a';
        $entidad_tipo_morosidad = 'z';
        $result = $sql->entidad_tipo_morosidad($entidad_base,$entidad_tipo_morosidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('z',$result);
        error::$en_error = false;

    }

    final public function test_genera_campo_contrato()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campo_condicion_pago = 'ccp';
        $name_database = 'ndb';
        $result = $sql->genera_campo_contrato($campo_condicion_pago,$name_database);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('ndb.contrato.ccp',$result);

        error::$en_error = false;

    }

    final public function test_genera_sql_no_visit()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campo_condicion_pago = 'ccp';
        $name_database = 'ndb';
        $periodicidades = array();
        $campos_custom = new stdClass();
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'a';
        $campos_custom->campo_estatus->value = 'va';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'b';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'c';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'd';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'e';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'f';
        $periodicidades[0]['key'] = 'k1';
        $periodicidades[0]['val'] = 'v1';

        $result = $sql->genera_sql_no_visit($campo_condicion_pago,$campos_custom,$name_database,$periodicidades);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT TABLA.PLAZA_ID, ndb.plaza.descripcion AS plaza, (SELECT COUNT(*) FROM ndb.contrato  WHERE ndb.contrato.a = 'va'  AND ndb.contrato.plaza_id = TABLA.PLAZA_ID AND  ndb.contrato.b IS NOT NULL AND  ndb.contrato.c IS NOT NULL AND  ndb.contrato.c != '' AND  ndb.contrato.d < ndb.contrato.e AND  ndb.contrato.f IS NOT NULL) AS contratos_activos, COUNT(*) AS no_visitado, SUM(CASE WHEN TABLA.RESTOS > 0 THEN 1 ELSE 0 END) AS con_restos, SUM(CASE WHEN TABLA.RESTOS = 0 THEN 1 ELSE 0 END) AS sin_restos FROM (SELECT ndb.contrato.plaza_id AS 'PLAZA_ID', @dias := DATEDIFF(CURDATE(),IFNULL(ndb.contrato.fecha_ultima_visita_cliente,ndb.contrato.fecha_ultima_visita)) AS 'DIAS TRANS ULT MOV CLIENTE', CASE  WHEN ndb.contrato.ccp = 'k1' AND @dias > v1 THEN 'NO'  END AS 'VISITADA', IFNULL(restos.x,0) AS 'RESTOS' FROM ndb.contrato LEFT JOIN (SELECT ndb.contrato_comision.contrato_id, 1 AS x FROM ndb.contrato_comision INNER JOIN ndb.contrato ON ndb.contrato.id = ndb.contrato_comision.contrato_id WHERE ndb.contrato_comision.monto_resto > 0 AND ndb.contrato_comision.comision_pagada = 'inactivo' GROUP BY ndb.contrato_comision.contrato_id) AS restos ON restos.contrato_id = ndb.contrato.id WHERE ndb.contrato.b IS NOT NULL AND  ndb.contrato.c IS NOT NULL AND  ndb.contrato.c != '' AND  ndb.contrato.d < ndb.contrato.e AND  ndb.contrato.f IS NOT NULL AND  ndb.contrato.a = 'va') AS TABLA INNER JOIN ndb.plaza ON ndb.plaza.id = TABLA.PLAZA_ID WHERE TABLA.VISITADA != 'SI' GROUP BY TABLA.PLAZA_ID",$result);


        error::$en_error = false;



    }

    final public function test_genera_tabla_contratos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campos_custom = new stdClass();
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'a';
        $campos_custom->campo_estatus->value = 'va';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'b';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'c';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'd';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'e';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'f';
        $campo_condicion_pago = 'g';
        $name_database = 'ndb';
        $periodicidades[0]['key'] = 'k1';
        $periodicidades[0]['val'] = 'v1';
        $result = $sql->genera_tabla_contratos($campos_custom,$campo_condicion_pago,$name_database, $periodicidades);
       // print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT ndb.contrato.plaza_id AS 'PLAZA_ID', @dias := DATEDIFF(CURDATE(),IFNULL(ndb.contrato.fecha_ultima_visita_cliente,ndb.contrato.fecha_ultima_visita)) AS 'DIAS TRANS ULT MOV CLIENTE', CASE  WHEN ndb.contrato.g = 'k1' AND @dias > v1 THEN 'NO'  END AS 'VISITADA', IFNULL(restos.x,0) AS 'RESTOS' FROM ndb.contrato LEFT JOIN (SELECT ndb.contrato_comision.contrato_id, 1 AS x FROM ndb.contrato_comision INNER JOIN ndb.contrato ON ndb.contrato.id = ndb.contrato_comision.contrato_id WHERE ndb.contrato_comision.monto_resto > 0 AND ndb.contrato_comision.comision_pagada = 'inactivo' GROUP BY ndb.contrato_comision.contrato_id) AS restos ON restos.contrato_id = ndb.contrato.id WHERE ndb.contrato.b IS NOT NULL AND  ndb.contrato.c IS NOT NULL AND  ndb.contrato.c != '' AND  ndb.contrato.d < ndb.contrato.e AND  ndb.contrato.f IS NOT NULL AND  ndb.contrato.a = 'va'",$result);


        error::$en_error = false;



    }

    final public function test_genera_when_periodicidad()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $periodicidades =array();
        $when_ccp = 'when_ccp';
        $periodicidades[0]['key'] = 'key';
        $periodicidades[0]['val'] = 'val';

        $periodicidades[1]['key'] = 'key1';
        $periodicidades[1]['val'] = 'val1';
        $result = $sql->genera_when_periodicidad($periodicidades, $when_ccp);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("WHEN when_ccp = 'key' AND @dias > val THEN 'NO' WHEN when_ccp = 'key1' AND @dias > val1 THEN 'NO' ",$result);


        error::$en_error = false;



    }

    final public function test_init_campos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campos_custom = new stdClass();
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'campo_estatus';
        $campos_custom->campo_estatus->value = 'campo_estatus.val';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'campo_fecha_valida';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'campo_serie';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'campo_monto_pagado';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'campo_monto_total';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'campo_folio';
        $result = $sql->init_campos($campos_custom);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('campo_estatus',$result->estatus);
        $this->assertEquals('campo_estatus.val',$result->estatus_val);
        $this->assertEquals('campo_fecha_valida',$result->fecha);
        $this->assertEquals('campo_serie',$result->serie);
        $this->assertEquals('campo_monto_pagado',$result->monto_p);
        $this->assertEquals('campo_monto_total',$result->monto_t);
        $this->assertEquals('campo_folio',$result->folio);


        error::$en_error = false;



    }

    final public function test_left_join_cp_cobranza()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campos = new stdClass();
        $result = $sql->left_join_cp_cobranza($campos);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertStringContainsString('Error al validar campos',$result['mensaje']);


        error::$en_error = false;

        $campos = new stdClass();
        $campos->entidad_cp = '';
        $result = $sql->left_join_cp_cobranza($campos);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertStringContainsString('Error al validar campos',$result['mensaje']);
        error::$en_error = false;



        $campos = new stdClass();
        $campos->entidad_cp = 'entidad_cp';
        $campos->entidad_localidad = 'entidad_localidad';
        $result = $sql->left_join_cp_cobranza($campos);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('',$result);
        error::$en_error = false;

        $campos = new stdClass();
        $campos->entidad_cp = 'cp_cobranza';
        $campos->entidad_localidad = 'entidad_localidad';
        $result = $sql->left_join_cp_cobranza($campos);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsStringIgnoringCase('LEFT JOIN cp AS cp_cobranza',$result);
        $this->assertStringContainsStringIgnoringCase('ON cp_cobranza.id = entidad_localidad.cp_id',$result);

        error::$en_error = false;

    }

    final public function test_left_join_empleado_base()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $entidad_empleado = 'ohem';
        $key_empleado_gestor_id = '';
        $key_empleado_id = 'ohem_id';
        $name_database = 'concentradora_bajio';
        $result = $sql->left_join_empleado_base($entidad_empleado,$key_empleado_gestor_id,$key_empleado_id,$name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('LEFT JOIN concentradora_bajio.ohem AS ohem ON cliente.ohem_id = ohem.id',$result);


        error::$en_error = false;

        $entidad_empleado = 'empleado';
        $key_empleado_gestor_id = 'empleado_gestor_id';
        $key_empleado_id = 'ohem_id';
        $name_database = 'concentradora_bajio';

        $result = $sql->left_join_empleado_base($entidad_empleado,$key_empleado_gestor_id,$key_empleado_id,$name_database);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('LEFT JOIN concentradora_bajio.empleado AS empleado ON contrato.empleado_gestor_id = empleado.id',$result);
        error::$en_error = false;


    }

    final public function test_left_join_status_contrato()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $entidad_base = 'a';
        $entidades = new stdClass();
        $entidades->entidad_status = 'b';
        $result = $sql->left_join_status_contrato($entidad_base,$entidades);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN b AS b ON b.id = a.b_id",$result);

        error::$en_error = false;


    }

    final public function test_metas()
    {
        error::$en_error = false;
        $sql = new sql();
        //$sql = new liberator($sql);
        $entidad_empleado = 'ohem';
        $fecha_fin = '2020-01-01';
        $fecha_inicio = '2020-01-01';
        $meta_gestor_id = '1';

        $result = $sql->metas($entidad_empleado,$fecha_fin,$fecha_inicio,$meta_gestor_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("SELECT plaza.descripcion AS 'PLAZA', '2020-01-01' AS 'FECHA INICIO', '2020-01-01' AS 'FECHA FIN', ohem.id AS  'GESTOR ID',  ohem.nombre_completo as 'GESTOR', IFNULL(meta_gestor_concentrado.n_contratos,'NO TIENE') AS 'CONTRATOS CONGELADOS', IFNULL(ROUND(meta_gestor_concentrado.monto_total,2),'NO TIENE') AS 'META', IFNULL(ROUND(meta_gestor_concentrado.meta_proceso,2),0) AS 'COBRADO META', IFNULL(ROUND(meta_gestor_concentrado.monto_de_mas,2),0) AS 'COBRADO META DE MAS', IFNULL(ROUND(meta_gestor_concentrado.monto_nuevo,2),0) AS 'COBRADO CONTRATOS NUEVOS', IFNULL(ROUND(meta_gestor_concentrado.monto_contratos_extra,2),0) AS 'COBRADO CONTRATOS EXTRA' FROM meta_gestor_concentrado LEFT JOIN ohem ON ohem.id = meta_gestor_concentrado.ohem_id LEFT JOIN plaza ON ohem.plaza_id = plaza.id LEFT JOIN meta_gestor ON meta_gestor_concentrado.meta_gestor_id = meta_gestor.id WHERE meta_gestor.id = 1",$result);

        error::$en_error = false;


    }

    final public function test_obj_campos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campos = new stdClass();
        $result = $sql->obj_campos($campos);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("DocDate",$result->campo_fecha_valida);
        $this->assertEquals("U_StatusCob",$result->campo_status_contrato);
        $this->assertEquals("Canceled",$result->campo_status_contrato_def);

        error::$en_error = false;


    }

    final public function test_pagos_determinados_por_periodo()
    {
        error::$en_error = false;
        $sql = new sql();
        //$sql = new liberator($sql);

        $campo_fecha_valida = 'DocDate';
        $campo_pago_total = 'DocTotal';
        $campo_movimiento = 'U_Movto';
        $entidad_empleado = 'ohem';
        $fecha_fin = '2020-01-11';
        $fecha_inicio = '2020-01-01';
        $name_database = 'concentradora_bajio';
        $plaza_id = '1';
        $result = $sql->pagos_determinados_por_periodo($campo_fecha_valida,$campo_pago_total,$campo_movimiento,
            $entidad_empleado,$fecha_fin,$fecha_inicio,$name_database,$plaza_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT pago.ohem_id, SUM(pago.DocTotal) as COBRADO_GENERAL, ohem.nombre_completo AS 'GESTOR' FROM concentradora_bajio.pago AS pago LEFT JOIN concentradora_bajio.ohem AS ohem ON pago.ohem_id = ohem.id WHERE pago.DocDate BETWEEN '2020-01-01' AND '2020-01-11' AND pago.U_Movto = 'Abono' AND pago.`status` = 'activo' AND pago.plaza_id = 1 GROUP BY pago.ohem_id",$result);

        error::$en_error = false;


    }

    final public function test_params_sql_contratos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campos_custom = new stdClass();
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'ce';
        $campos_custom->campo_estatus->value = 'vce';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'cvf';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'cs';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'cmp';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'cmt';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'cf';

        $name_database = 'nd';
        $result = $sql->params_sql_contratos($campos_custom,$name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("nd.contrato.cvf IS NOT NULL AND  nd.contrato.cs IS NOT NULL AND  nd.contrato.cs != '' AND  nd.contrato.cmp < nd.contrato.cmt AND  nd.contrato.cf IS NOT NULL AND  nd.contrato.ce = 'vce' ",$result->where_sql);
        $this->assertEquals("@dias := DATEDIFF(CURDATE(),IFNULL(nd.contrato.fecha_ultima_visita_cliente,nd.contrato.fecha_ultima_visita)) AS 'DIAS TRANS ULT MOV CLIENTE'",$result->dias_ultima_visita);
        $this->assertEquals("SELECT nd.contrato_comision.contrato_id, 1 AS x FROM nd.contrato_comision INNER JOIN nd.contrato ON nd.contrato.id = nd.contrato_comision.contrato_id WHERE nd.contrato_comision.monto_resto > 0 AND nd.contrato_comision.comision_pagada = 'inactivo' GROUP BY nd.contrato_comision.contrato_id",$result->restos_sql);


        error::$en_error = false;


    }

    final public function test_rpt_no_visitadas()
    {
        error::$en_error = false;
        $sql = new sql();
        //$sql = new liberator($sql);
        $campos = new stdClass();
        $entidad_base = 'entidad_base';
        $entidad_empleado = 'entidad_empleado';
        $filtro_plaza = '';
        $plaza_id = '';
        $campos->campo_afiliado = 'campo_afiliado';
        $campos->campo_aportado = 'campo_aportado';
        $campos->campo_cp = 'campo_cp';
        $campos->campo_codigo_gestor = 'campo_codigo_gestor';
        $campos->campo_domicilio_cobranza = 'campo_codigo_gestor';
        $campos->campo_fecha_contrato = 'campo_fecha_contrato';
        $campos->campo_folio = 'campo_folio';
        $campos->campo_gestor = 'campo_gestor';
        $campos->campo_importe_pago = 'campo_importe_pago';
        $campos->campo_localidad = 'campo_localidad';
        $campos->campo_municipio = 'campo_municipio';
        $campos->campo_periodicidad_pago = 'campo_periodicidad_pago';
        $campos->campo_precio = 'campo_precio';
        $campos->campo_serie = 'campo_serie';
        $campos->campo_status_contrato = 'campo_status_contrato';
        $campos->campo_tipo_morosidad = 'campo_tipo_morosidad';
        $campos->entidad_cp = 'entidad_cp';
        $campos->entidad_localidad = 'entidad_localidad';
        $campos->entidad_municipio = 'entidad_municipio';
        $campos->value_pp_m = 'value_pp_m';
        $campos->value_pp_m_rs = 'value_pp_m_rs';
        $campos->value_pp_q = 'value_pp_q';
        $campos->value_pp_q_rs = 'value_pp_q_rs';
        $campos->value_pp_s = 'value_pp_s';
        $campos->value_pp_s_rs = 'value_pp_s_rs';

        $result = $sql->rpt_no_visitadas($campos,$entidad_base,$entidad_empleado,$filtro_plaza,$plaza_id,'','','');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsString("SELECT * FROM",$result);
        $this->assertStringContainsString("(",$result);
        $this->assertStringContainsString("SELECT",$result);
        $this->assertStringContainsString("entidad_base.id AS 'CONTRATO_ID',",$result);
        $this->assertStringContainsString("CONCAT( entidad_base.campo_serie, entidad_base.campo_folio ) AS 'CONTRATO',",$result);
        $this->assertStringContainsString("plaza.id AS 'plaza_id',",$result);
        $this->assertStringContainsString("entidad_base.campo_importe_pago AS 'IMPORTE PAGO',",$result);
        $this->assertStringContainsString("@dias := DATEDIFF(CURDATE(),IFNULL(entidad_base.fecha_ultima_visita_cliente,entidad_base.fecha_ultima_visita)) AS 'DIAS TRANS ULT MOV CLIENTE',",$result);
        $this->assertStringContainsString("WHEN entidad_base.campo_periodicidad_pago = 'value_pp_m' AND @dias BETWEEN 0 AND 31 THEN ",$result);
        $this->assertStringContainsString("entidad_base.plaza_id =  GROUP BY contrato_id) AS restos ON restos.contrato_id = entidad_base.id",$result);
        $this->assertStringContainsString("entidad_base.campo_serie != '' AND",$result);
        $this->assertStringContainsString("entidad_base.campo_status_contrato IN ('ACTIVO','SUSPENSION TEMPORAL','OBSERVACION','POR FIRMAR','PRE-CANCELADO','PROMESA PAGO')",$result);

        error::$en_error = false;
        $result = $sql->rpt_no_visitadas($campos,$entidad_base,$entidad_empleado,$filtro_plaza,$plaza_id,'entidad_cliente','','');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsString("entidad_base.campo_fecha_contrato AS 'FECHA CONTRATO',",$result);
        $this->assertStringContainsString("entidad_cliente.campo_afiliado AS 'AFILIADO',",$result);


        error::$en_error = false;
        $result = $sql->rpt_no_visitadas($campos,$entidad_base,$entidad_empleado,$filtro_plaza,$plaza_id,'entidad_cliente','entidad_status','');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsString("LEFT JOIN entidad_status AS entidad_status",$result);
        $this->assertStringContainsString("ON entidad_status.id = entidad_base.entidad_status_id",$result);
        $this->assertStringContainsString("entidad_status.campo_status_contrato IN ('ACTIVO','SUSPENSION TEMPORAL','OBSERVACION','POR FIRMAR','PRE-CANCELADO','PROMESA PAGO')",$result);

        error::$en_error = false;
        $result = $sql->rpt_no_visitadas($campos,$entidad_base,$entidad_empleado,$filtro_plaza,$plaza_id,'entidad_cliente','entidad_status','entidad_tipo_morosidad');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsString("entidad_status.campo_status_contrato IN ('ACTIVO','SUSPENSION TEMPORAL','OBSERVACION','POR FIRMAR','PRE-CANCELADO','PROMESA PAGO')",$result);
        $this->assertStringContainsString("entidad_status.campo_status_contrato AS 'STATUS DEL CONTRATO',",$result);
        $this->assertStringContainsString("entidad_cliente.campo_afiliado AS 'AFILIADO',",$result);

        error::$en_error = false;


    }

    final public function test_rpt_meta_gestor_concentrado()
    {
        error::$en_error = false;
        $sql = new sql();
        //$sql = new liberator($sql);

        $result = $sql->rpt_meta_gestor_concentrado('DocDate','2024-06-01','2024-06-30',
            '39,40,41,42,43,44','zzz');
        //print_r($result);exit;


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertStringContainsString("SELECT",$result);
        $this->assertStringContainsString("plaza.descripcion AS PLAZA,",$result);
        $this->assertStringContainsString("meta_gestor.fecha_inicio AS 'FECHA INICIO',",$result);
        $this->assertStringContainsString("meta_gestor.fecha_fin AS 'FECHA FIN',",$result);
        $this->assertStringContainsString("SUM( IFNULL(meta_gestor_concentrado.n_contratos,0) ) AS 'CONTRATOS CONGELADOS'",$result);
        $this->assertStringContainsString("(SELECT COUNT(*)",$result);
        $this->assertStringContainsString("FROM zzz.contrato AS contrato",$result);
        $this->assertStringContainsString("WHERE contrato.U_StatusCob IN ( 'ACTIVO', 'PROMESA PAGO' )",$result);
        $this->assertStringContainsString("AND contrato.Canceled = 'N'",$result);
        $this->assertStringContainsString("AND contrato.plaza_id = meta_gestor.plaza_id) AS 'CARTERA ACTUAL',",$result);
        $this->assertStringContainsString("(SELECT COUNT(*) ",$result);
        $this->assertStringContainsString("FROM zzz.contrato AS contrato",$result);
        $this->assertStringContainsString("WHERE contrato.DocDate BETWEEN '2024-06-30' AND '2024-06-01'",$result);
        $this->assertStringContainsString("AND contrato.U_StatusCob IN ( 'ACTIVO', 'PROMESA PAGO' )",$result);
        $this->assertStringContainsString("AND contrato.Canceled = 'N'",$result);
        $this->assertStringContainsString("AND meta_gestor.plaza_id = contrato.plaza_id ) AS 'CONTRATOS NUEVOS',",$result);
        $this->assertStringContainsString("AND meta_gestor.plaza_id = contrato.plaza_id ) AS 'CONTRATOS NUEVOS',",$result);
        $this->assertStringContainsString("AND pago.U_Movto = 'Abono' AND pago.`status` = 'activo",$result);
        $this->assertStringContainsString("'COBRADO TOTAL'",$result);
        $this->assertStringContainsString("AND pago.plaza_id = plaza.id) AS 'COBRADO TOTAL'",$result);
        $this->assertEquals("SELECT plaza.descripcion AS PLAZA, meta_gestor.fecha_inicio AS 'FECHA INICIO', meta_gestor.fecha_fin AS 'FECHA FIN',  SUM( IFNULL(meta_gestor_concentrado.n_contratos,0) ) AS 'CONTRATOS CONGELADOS',  (SELECT COUNT(*) FROM zzz.contrato AS contrato WHERE contrato.U_StatusCob IN ( 'ACTIVO', 'PROMESA PAGO' ) AND contrato.Canceled = 'N' AND contrato.plaza_id = meta_gestor.plaza_id) AS 'CARTERA ACTUAL', (SELECT COUNT(*) FROM zzz.contrato AS contrato WHERE contrato.DocDate BETWEEN '2024-06-30' AND '2024-06-01' AND contrato.U_StatusCob IN ( 'ACTIVO', 'PROMESA PAGO' ) AND contrato.Canceled = 'N' AND meta_gestor.plaza_id = contrato.plaza_id ) AS 'CONTRATOS NUEVOS', SUM( IFNULL(meta_gestor_concentrado.monto_total,0) ) AS META, SUM( IFNULL(meta_gestor_concentrado.meta_proceso,0) ) AS 'COBRADO META', (SELECT SUM(pago.DocTotal) FROM zzz.pago AS pago WHERE pago.DocDate BETWEEN '2024-06-30' AND '2024-06-01' AND pago.U_Movto = 'Abono' AND pago.`status` = 'activo' AND pago.plaza_id = plaza.id) AS 'COBRADO TOTAL', meta_gestor.id AS meta_gestor_id, plaza.id AS 'PLAZA ID', SUM( IFNULL(meta_gestor_concentrado.monto_de_mas,0) ) AS 'COBRADO META DE MAS', SUM( IFNULL(meta_gestor_concentrado.monto_nuevo,0)) AS 'COBRADO CONTRATOS NUEVOS', SUM( IFNULL(meta_gestor_concentrado.monto_contratos_extra,0) ) AS 'COBRADO CONTRATOS EXTRA' FROM zzz.meta_gestor_concentrado AS meta_gestor_concentrado LEFT JOIN zzz.meta_gestor AS meta_gestor ON meta_gestor.id = meta_gestor_concentrado.meta_gestor_id LEFT JOIN zzz.plaza AS plaza ON plaza.id = meta_gestor.plaza_id  WHERE meta_gestor.id IN ( 39,40,41,42,43,44 ) GROUP BY meta_gestor.plaza_id",$result);


        error::$en_error = false;

        $result = $sql->rpt_meta_gestor_concentrado('DocDueDate','2024-06-01','2024-06-30',
            '39,40,41,42,43,44','zzz');

        $this->assertStringContainsString("(SELECT SUM(pago.DocTotal) FROM zzz.pago AS pago WHERE pago.DocDueDate BETWEEN '2024-06-30' AND '2024-06-01' AND pago.U_Movto = 'Abono' AND pago.`status` = 'activo' AND pago.plaza_id = plaza.id) AS 'COBRADO TOTAL'",$result);
        error::$en_error = false;

        $result = $sql->rpt_meta_gestor_concentrado('DocDueDate','2024-06-01','2024-06-30',
            '39,40,41,42,43,44','concentradora_bajio');
        $this->assertEquals("SELECT plaza.descripcion AS PLAZA, meta_gestor.fecha_inicio AS 'FECHA INICIO', meta_gestor.fecha_fin AS 'FECHA FIN',  SUM( IFNULL(meta_gestor_concentrado.n_contratos,0) ) AS 'CONTRATOS CONGELADOS',  (SELECT COUNT(*) FROM concentradora_bajio.contrato AS contrato WHERE contrato.U_StatusCob IN ( 'ACTIVO', 'PROMESA PAGO' ) AND contrato.Canceled = 'N' AND contrato.plaza_id = meta_gestor.plaza_id) AS 'CARTERA ACTUAL', (SELECT COUNT(*) FROM concentradora_bajio.contrato AS contrato WHERE contrato.DocDate BETWEEN '2024-06-30' AND '2024-06-01' AND contrato.U_StatusCob IN ( 'ACTIVO', 'PROMESA PAGO' ) AND contrato.Canceled = 'N' AND meta_gestor.plaza_id = contrato.plaza_id ) AS 'CONTRATOS NUEVOS', SUM( IFNULL(meta_gestor_concentrado.monto_total,0) ) AS META, SUM( IFNULL(meta_gestor_concentrado.meta_proceso,0) ) AS 'COBRADO META', (SELECT SUM(pago.DocTotal) FROM concentradora_bajio.pago AS pago WHERE pago.DocDueDate BETWEEN '2024-06-30' AND '2024-06-01' AND pago.U_Movto = 'Abono' AND pago.`status` = 'activo' AND pago.plaza_id = plaza.id) AS 'COBRADO TOTAL', meta_gestor.id AS meta_gestor_id, plaza.id AS 'PLAZA ID', SUM( IFNULL(meta_gestor_concentrado.monto_de_mas,0) ) AS 'COBRADO META DE MAS', SUM( IFNULL(meta_gestor_concentrado.monto_nuevo,0)) AS 'COBRADO CONTRATOS NUEVOS', SUM( IFNULL(meta_gestor_concentrado.monto_contratos_extra,0) ) AS 'COBRADO CONTRATOS EXTRA' FROM concentradora_bajio.meta_gestor_concentrado AS meta_gestor_concentrado LEFT JOIN concentradora_bajio.meta_gestor AS meta_gestor ON meta_gestor.id = meta_gestor_concentrado.meta_gestor_id LEFT JOIN concentradora_bajio.plaza AS plaza ON plaza.id = meta_gestor.plaza_id  WHERE meta_gestor.id IN ( 39,40,41,42,43,44 ) GROUP BY meta_gestor.plaza_id",$result);
        error::$en_error = false;


    }

    final public function test_sql_no_visit()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $contratos_activos_sql = 'contratos_activos_sql';
        $contratos_no_visit = 'contratos_no_visit';
        $name_database = 'name_database';

        $result = $sql->sql_no_visit($contratos_activos_sql,$contratos_no_visit,$name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT TABLA.PLAZA_ID, name_database.plaza.descripcion AS plaza, contratos_activos_sql, COUNT(*) AS no_visitado, SUM(CASE WHEN TABLA.RESTOS > 0 THEN 1 ELSE 0 END) AS con_restos, SUM(CASE WHEN TABLA.RESTOS = 0 THEN 1 ELSE 0 END) AS sin_restos FROM (contratos_no_visit) AS TABLA INNER JOIN name_database.plaza ON name_database.plaza.id = TABLA.PLAZA_ID WHERE TABLA.VISITADA != 'SI' GROUP BY TABLA.PLAZA_ID",$result);

        error::$en_error = false;


    }

    final public function test_sql_no_visit_sap()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $name_database = 'x';

        $result = $sql->sql_no_visit_sap($name_database);
       // print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT TABLA.PLAZA_ID, x.plaza.descripcion AS plaza, (SELECT COUNT(*) FROM x.contrato  WHERE x.contrato.U_StatusCob = 'ACTIVO'  AND x.contrato.plaza_id = TABLA.PLAZA_ID AND  x.contrato.DocDate IS NOT NULL AND  x.contrato.U_SeCont IS NOT NULL AND  x.contrato.U_SeCont != '' AND  x.contrato.PaidToDate < x.contrato.DocTotal AND  x.contrato.U_FolioCont IS NOT NULL) AS contratos_activos, COUNT(*) AS no_visitado, SUM(CASE WHEN TABLA.RESTOS > 0 THEN 1 ELSE 0 END) AS con_restos, SUM(CASE WHEN TABLA.RESTOS = 0 THEN 1 ELSE 0 END) AS sin_restos FROM (SELECT x.contrato.plaza_id AS 'PLAZA_ID', @dias := DATEDIFF(CURDATE(),IFNULL(x.contrato.fecha_ultima_visita_cliente,x.contrato.fecha_ultima_visita)) AS 'DIAS TRANS ULT MOV CLIENTE', CASE WHEN x.contrato.U_CondPago = 'S' AND @dias > 7 THEN 'NO' WHEN x.contrato.U_CondPago = 'Q' AND @dias > 16 THEN 'NO' WHEN x.contrato.U_CondPago = 'M' AND @dias > 31 THEN 'NO'  END AS 'VISITADA', IFNULL(restos.x,0) AS 'RESTOS' FROM x.contrato LEFT JOIN (SELECT x.contrato_comision.contrato_id, 1 AS x FROM x.contrato_comision INNER JOIN x.contrato ON x.contrato.id = x.contrato_comision.contrato_id WHERE x.contrato_comision.monto_resto > 0 AND x.contrato_comision.comision_pagada = 'inactivo' GROUP BY x.contrato_comision.contrato_id) AS restos ON restos.contrato_id = x.contrato.id WHERE x.contrato.DocDate IS NOT NULL AND  x.contrato.U_SeCont IS NOT NULL AND  x.contrato.U_SeCont != '' AND  x.contrato.PaidToDate < x.contrato.DocTotal AND  x.contrato.U_FolioCont IS NOT NULL AND  x.contrato.U_StatusCob = 'ACTIVO') AS TABLA INNER JOIN x.plaza ON x.plaza.id = TABLA.PLAZA_ID WHERE TABLA.VISITADA != 'SI' GROUP BY TABLA.PLAZA_ID",$result);

        error::$en_error = false;


    }

    final public function test_sql_when_periodicidad()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo_condicion_pago = 'ccp';
        $name_database = 'ndb';
        $periodicidades = array();
        $periodicidades[0]['key'] = 'key0';
        $periodicidades[0]['val'] = 'val0';

        $periodicidades[1]['key'] = 'key1';
        $periodicidades[1]['val'] = 'val1';

        $result = $sql->sql_when_periodicidad($campo_condicion_pago,$name_database, $periodicidades);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("WHEN ndb.contrato.ccp = 'key0' AND @dias > val0 THEN 'NO' WHEN ndb.contrato.ccp = 'key1' AND @dias > val1 THEN 'NO' ",$result);

        error::$en_error = false;


    }

    final public function test_status_def()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $values_status_def = '';

        $result = $sql->status_def($values_status_def);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("N",$result);

        $values_status_def = 'x';

        $result = $sql->status_def($values_status_def);
        $this->assertEquals("x",$result);

        error::$en_error = false;


    }

    final public function test_tabla_contratos()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo_condicion_pago = 'ccp';
        $dias_ultima_visita = 'duv';
        $name_database = 'nd';
        $periodicidades = array();
        $restos_sql = 'rs';
        $where_sql = 'ws';
        $periodicidades[0]['key'] = 'key';
        $periodicidades[0]['val'] = 'val';

        $result = $sql->tabla_contratos($campo_condicion_pago,$dias_ultima_visita,$name_database,$periodicidades,$restos_sql,$where_sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT nd.contrato.plaza_id AS 'PLAZA_ID', duv, CASE  WHEN nd.contrato.ccp = 'key' AND @dias > val THEN 'NO'  END AS 'VISITADA', IFNULL(restos.x,0) AS 'RESTOS' FROM nd.contrato LEFT JOIN (rs) AS restos ON restos.contrato_id = nd.contrato.id WHERE ws",$result);

        error::$en_error = false;


    }

    final public function test_values_status()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $status_values = '';
        $result = $sql->values_status($status_values);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("'ACTIVO', 'PROMESA PAGO'",$result);

        error::$en_error = false;

        $status_values = 'x';
        $result = $sql->values_status($status_values);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x",$result);
        error::$en_error = false;

    }

    final public function test_when()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo = 'campo';
        $operador = 'operador';
        $value_compare = 'value_compare';
        $value_result = 'value_result';
        $result = $sql->when($campo,$operador,$value_compare,$value_result);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("WHEN campo operador 'value_compare' THEN 'value_result' ",$result);

        error::$en_error = false;

    }

    final public function test_when_case()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);
        $campo = 'a';
        $operador = 'ss';
        $value = 'xxx';
        $result = $sql->when_case($campo,$operador,$value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a ss 'xxx'",$result);

        error::$en_error = false;

    }

    final public function test_when_periodicidad()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $periodicidad = array();
        $w_per = 'xxx';
        $when_ccp = 'when_ccp';
        $periodicidad['key'] = 'key';
        $periodicidad['val'] = 'val';

        $result = $sql->when_periodicidad($periodicidad,$w_per,$when_ccp);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("xxx WHEN when_ccp = 'key' AND @dias > val THEN 'NO' ",$result);

        error::$en_error = false;



    }

    final public function test_where_contrato()
    {
        error::$en_error = false;
        $sql = new sql();
        $sql = new liberator($sql);

        $campos_custom = new stdClass();
        $name_database = 'zzz';
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'status_contrato_id';
        $campos_custom->campo_estatus->value = '1';

        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'fecha_valida_informacion';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'campo_serie';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'monto_pagado';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'monto_total';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'folio';

        $result = $sql->where_contrato($campos_custom,$name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("zzz.contrato.fecha_valida_informacion IS NOT NULL AND  zzz.contrato.campo_serie IS NOT NULL AND  zzz.contrato.campo_serie != '' AND  zzz.contrato.monto_pagado < zzz.contrato.monto_total AND  zzz.contrato.folio IS NOT NULL AND  zzz.contrato.status_contrato_id = '1' ",$result);

        error::$en_error = false;


        $name_database = 'rrrr';

        $campos_custom = (new _no_visitadas())->campo_base_sap();

        $result = $sql->where_contrato($campos_custom,$name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("rrrr.contrato.DocDate IS NOT NULL AND  rrrr.contrato.U_SeCont IS NOT NULL AND  rrrr.contrato.U_SeCont != '' AND  rrrr.contrato.PaidToDate < rrrr.contrato.DocTotal AND  rrrr.contrato.U_FolioCont IS NOT NULL AND  rrrr.contrato.U_StatusCob = 'ACTIVO' ",$result);


        error::$en_error = false;

    }

}
