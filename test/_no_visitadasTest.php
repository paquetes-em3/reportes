<?php
namespace desarrollo_em3\test;
use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\reportes\_format_value;
use desarrollo_em3\reportes\_no_visitadas;
use desarrollo_em3\reportes\_valida;
use desarrollo_em3\reportes\sql;
use PHPUnit\Framework\TestCase;
use stdClass;

class _no_visitadasTest extends TestCase
{
    final public function test_campo_base_sap()
    {
        error::$en_error = false;
        $obj = new _no_visitadas();
        $obj = new liberator($obj);

        $result = $obj->campo_base_sap();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('U_StatusCob',$result->campo_estatus->name);
        $this->assertEquals('ACTIVO',$result->campo_estatus->value);
        $this->assertEquals('DocDate',$result->campo_fecha_valida->name);
        $this->assertEquals('U_SeCont',$result->campo_serie->name);
        $this->assertEquals('PaidToDate',$result->campo_monto_pagado->name);
        $this->assertEquals('DocTotal',$result->campo_monto_total->name);
        $this->assertEquals('U_FolioCont',$result->campo_folio->name);

        error::$en_error = false;
    }

    final public function test_n_dias_quincena()
    {
        error::$en_error = false;
        $obj = new _no_visitadas();
        //$obj = new liberator($obj);

        $result = $obj->n_dias_quincena();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsint($result);
        $this->assertEquals(15,$result);


        error::$en_error = false;
    }

    final public function test_n_dias_mes_actual()
    {
        error::$en_error = false;
        $obj = new _no_visitadas();
        //$obj = new liberator($obj);

        $result = $obj->n_dias_mes_actual();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsint($result);
        /**
         * RESULT ORIGINAL DEFINIDO MES ANTERIOR
         */
        //$this->assertEquals(30,$result);
        $this->assertEquals(31,$result);


        error::$en_error = false;
    }

    final public function test_n_dias_mes_anterior()
    {
        error::$en_error = false;
        $obj = new _no_visitadas();
        //$obj = new liberator($obj);

        $result = $obj->n_dias_mes_anterior();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsint($result);
        $this->assertEquals(31,$result);

        error::$en_error = false;
    }

    final public function test_periodicidades_sap()
    {
        error::$en_error = false;
        $obj = new _no_visitadas();
        //$obj = new liberator($obj);

        $result = $obj->periodicidades_sap();
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('S',$result['s']['key']);
        $this->assertEquals('7',$result['s']['val']);

        $this->assertEquals('Q',$result['q']['key']);
        $this->assertEquals('16',$result['q']['val']);

        $this->assertEquals('M',$result['m']['key']);
        $this->assertEquals('31',$result['m']['val']);


        error::$en_error = false;
    }



}
