<?php
namespace desarrollo_em3\test;
use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\reportes\_valida;
use desarrollo_em3\reportes\sql;
use PHPUnit\Framework\TestCase;
use stdClass;

class _validaTest extends TestCase
{

    final public function test_campo_name()
    {
        error::$en_error = false;
        $val = new _valida();
        $val = new liberator($val);

        $campos_custom = new stdClass();
        $key_name = 'x';
        $campos_custom->x = new stdClass();
        $campos_custom->x->name = 'z';

        $result = $val->campo_name($campos_custom,$key_name);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);


        error::$en_error = false;

    }

    final public function test_campos_name()
    {
        error::$en_error = false;
        $val = new _valida();
        $val = new liberator($val);

        $campos_custom = new stdClass();
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'name';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'fecha_valida';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'serie';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'mp';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'mt';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'f';

        $result = $val->campos_name($campos_custom);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);


        error::$en_error = false;

    }

    final public function test_valida_base_campos()
    {
        error::$en_error = false;
        $val = new _valida();
        //$val = new liberator($val);

        $campos_custom = new stdClass();
        $name_database = 'a';
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'a';
        $campos_custom->campo_estatus->value = 'c';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'b';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'c';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'd';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'e';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'f';


        $result = $val->valida_base_campos($campos_custom,$name_database);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);


        error::$en_error = false;

    }

    final public function test_valida_campos_name()
    {
        error::$en_error = false;
        $val = new _valida();
        //$val = new liberator($val);

        $campos_custom = new stdClass();
        $name_database = 'a';
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'a';
        $campos_custom->campo_estatus->value = 'c';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'b';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'c';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'd';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'e';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'f';


        $result = $val->valida_campos_name($campos_custom);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);


        error::$en_error = false;

    }

    final public function test_valida_input_base()
    {
        error::$en_error = false;
        $val = new _valida();
        //$sql = new liberator($sql);
        $fecha_inicio = '';
        $fecha_fin = '';
        $name_database = '';
        $result = $val->valida_input_base($fecha_fin,$fecha_inicio,$name_database);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $name_database esta vacia',$result['mensaje_limpio']);


        error::$en_error = false;

        $fecha_inicio = '';
        $fecha_fin = '';
        $name_database = 'a';
        $result = $val->valida_input_base($fecha_fin,$fecha_inicio,$name_database);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $fecha_fin esta vacia',$result['mensaje_limpio']);
        error::$en_error = false;


        $fecha_inicio = '';
        $fecha_fin = 'x';
        $name_database = 'a';
        $result = $val->valida_input_base($fecha_fin,$fecha_inicio,$name_database);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $fecha_inicio esta vacia',$result['mensaje_limpio']);
        error::$en_error = false;

        $fecha_inicio = 'z';
        $fecha_fin = 'x';
        $name_database = 'a';
        $result = $val->valida_input_base($fecha_fin,$fecha_inicio,$name_database);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;
        error::$en_error = false;


    }

    final public function test_valida_no_visit()
    {
        error::$en_error = false;
        $val = new _valida();
        //$sql = new liberator($sql);
        $campo_condicion_pago = 'a';
        $name_database = 'b';
        $periodicidades = array();
        $campos_custom = new stdClass();
        $campos_custom->campo_estatus = new stdClass();
        $campos_custom->campo_estatus->name = 'name';
        $campos_custom->campo_estatus->value = 'name';
        $campos_custom->campo_fecha_valida = new stdClass();
        $campos_custom->campo_fecha_valida->name = 'fecha_valida';
        $campos_custom->campo_serie = new stdClass();
        $campos_custom->campo_serie->name = 'serie';
        $campos_custom->campo_monto_pagado = new stdClass();
        $campos_custom->campo_monto_pagado->name = 'mp';
        $campos_custom->campo_monto_total = new stdClass();
        $campos_custom->campo_monto_total->name = 'mt';
        $campos_custom->campo_folio = new stdClass();
        $campos_custom->campo_folio->name = 'f';
        $periodicidades[0]['key'] = 'key';
        $periodicidades[0]['val'] = 'val';
        $result = $val->valida_no_visit($campo_condicion_pago,$campos_custom,$name_database,$periodicidades);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;



    }



}
